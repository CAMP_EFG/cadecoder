﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace LMU_Terminal
{
    public struct ComPortEvent
    {
        public enum ComPortEventType
        {
            ComPortOpen,
            ComPortClose,
            WaitTime,
            SendCommand,
            AwaitReply,
            SendQuery
        }

        public int SequenceNumber { get; set; }
        public ComPortEventType EventType { get; set; }
        public String CommandToSend { get; set; }
        public String MustContain { get; set; }
        public int TimeOut { get; set; }
        public int Retries { get; set; }
    }

    public class ComQueueEventArgs : EventArgs
    {
        public enum ComQueueReturnState
        {
            Success,
            UnableToOpenComPort,
            NoResponseToQuery,
            UnitNotResponding
        }

        public ComQueueEventArgs(int SequenceNumber, ComPortEvent.ComPortEventType EventType, ComQueueReturnState Status, string ReturnString)
        {
            this.SequenceNumber = SequenceNumber;
            this.EventType = EventType;
            this.Status = Status;
            this.ReturnString = ReturnString;
        }

        public int SequenceNumber;
        public ComPortEvent.ComPortEventType EventType;
        public ComQueueReturnState Status;
        public string ReturnString;
    }

    public class ComPortEventQueue
    {
        private enum ComPortEventState
        {
            Idle,
            LookingForNextEvent,
            AwaitingReply,
            Found,
            TimedOut,
            AwaitingTimeOut,
            CallingBack,
            ClearingEvent,
            ShuttingDown
        }

        public ComPort comPort { get; set; }
        private List<ComPortEvent> Queue { get; set; }
        private ComPortEventState State;
        private int CurrentRetries;
        private bool DataFound;
        private Timer ComTimer;
        private Timer ReadTimer;
        private Timer NextTimer;

        public ComPortEventQueue()
        {
            comPort = new ComPort();
            Queue = new List<ComPortEvent>();
            State = ComPortEventState.Idle;
            DataFound = false;
            CurrentRetries = 0;
            ComTimer = new Timer();
            ComTimer.Enabled = false;
            ComTimer.Interval = 100;
            ComTimer.Tick += new EventHandler(ComTimer_Tick);
            ReadTimer = new Timer();
            ReadTimer.Enabled = true;
            ReadTimer.Interval = 100;
            ReadTimer.Tick += new EventHandler(ReadTimer_Tick);
            NextTimer = new Timer();
            NextTimer.Enabled = false;
            NextTimer.Interval = 100;
            NextTimer.Tick += new EventHandler(NextTimer_Tick);
            comPort.TextLineReceived += new EventHandler(comPort_TextLineReceived);
        }

        public void Abort()
        {
            State = ComPortEventState.ShuttingDown;
            ReadTimer.Enabled = false;
            ComTimer.Enabled = false;
            NextTimer.Enabled = false;
            comPort.Close();
            Queue = new List<ComPortEvent>();
            DataFound = false;
            CurrentRetries = 0;
            ReadTimer.Enabled = true;
            State = ComPortEventState.Idle;
        }

        //================================================================
        // Class Events
        //================================================================

        public event EventHandler ActionCompleted;

        protected virtual void OnActionCompleted(EventArgs e)
        {
            EventHandler handler = ActionCompleted;
            if (handler != null)
                handler(this, e);
        }

        private void DoCallBack(ComQueueEventArgs.ComQueueReturnState Status, string ReturnString)
        {
            ComQueueEventArgs Args = new ComQueueEventArgs(Queue[0].SequenceNumber, Queue[0].EventType, Status, ReturnString);

            Queue.RemoveAt(0);
            OnActionCompleted(Args);
            NextTimer.Enabled = true;
        }

        //================================================================
        // Com Port Events
        //================================================================

        private void comPort_TextLineReceived(object sender, EventArgs e)
        {

        }

        //================================================================
        // Timer Events
        //================================================================

        private void NextTimer_Tick(object sender, EventArgs e)
        {
            NextTimer.Enabled = false;
            State = ComPortEventState.Idle;
            CheckNext();
        }

        private void ReadTimer_Tick(object sender, EventArgs e)
        {
            string Line;

            ReadTimer.Enabled = false;

            while (comPort.TextLineCount > 0)
            {
                DataFound = true;

                Line = comPort.ReadTextLine();

                if (State == ComPortEventState.AwaitingReply)
                {
                    if (Line.Contains(Queue[0].MustContain))
                    {
                        State = ComPortEventState.Found;
                        Debug.WriteLine(Line);
                        ComTimer.Enabled = false;
                        DoCallBack(ComQueueEventArgs.ComQueueReturnState.Success, Line);
                    }
                }
            }

            ReadTimer.Enabled = true;
        }

        private void ComTimer_Tick(object sender, EventArgs e)
        {
            ComTimer.Enabled = false;

            Debug.WriteLine("Tick");
            if (State == ComPortEventState.AwaitingTimeOut)
            {
                DoCallBack(ComQueueEventArgs.ComQueueReturnState.Success, "");
            }
            else if ((State == ComPortEventState.AwaitingReply) && (Queue[0].EventType == ComPortEvent.ComPortEventType.SendQuery))
            {
                if (CurrentRetries > 0)
                {
                    Debug.WriteLine("Time Out: " + CurrentRetries.ToString() + " Retries");
                    CurrentRetries--;
                   // comPort.WriteTextLine(Queue[0].CommandToSend, true, true);
                    ComTimer.Enabled = true;
                }
                else
                {
                    State = ComPortEventState.TimedOut;
                    if (DataFound)
                    {
                        DoCallBack(ComQueueEventArgs.ComQueueReturnState.NoResponseToQuery, "");
                    }
                    else
                    {
                        DoCallBack(ComQueueEventArgs.ComQueueReturnState.UnitNotResponding, "");
                    }
                }
            }
        }

        private void CheckNext()
        {
            if (Queue.Count == 0)
            {
                State = ComPortEventState.Idle;
            }
            else
            {
                State = ComPortEventState.LookingForNextEvent;
                //NextEvent();
            }
        }

        //private void OpenComPort()
        //{
        //    if (!comPort.IsOpen)
        //    {
        //        comPort.Open();
        //    }
        //}

        //private void NextEvent()
        //{
        //    switch (Queue[0].EventType)
        //    {
        //        case ComPortEvent.ComPortEventType.AwaitReply:
        //            OpenComPort();
        //            if (comPort.IsOpen)
        //            {
        //                State = ComPortEventState.AwaitingReply;
        //                ComTimer.Interval = Queue[0].TimeOut;
        //                DataFound = false;
        //                ComTimer.Enabled = true;
        //            }
        //            else
        //            {
        //                DoCallBack(ComQueueEventArgs.ComQueueReturnState.UnableToOpenComPort, "");
        //            }
        //            break;

        //        case ComPortEvent.ComPortEventType.ComPortClose:
        //            comPort.Close();
        //            if (comPort.IsOpen)
        //            {
        //                DoCallBack(ComQueueEventArgs.ComQueueReturnState.UnableToOpenComPort, "");
        //            }
        //            else
        //            {
        //                DoCallBack(ComQueueEventArgs.ComQueueReturnState.Success, "");
        //            }
        //            break;

        //        case ComPortEvent.ComPortEventType.ComPortOpen:
        //            OpenComPort();
        //            if (comPort.IsOpen)
        //            {
        //                DoCallBack(ComQueueEventArgs.ComQueueReturnState.Success, "");
        //            }
        //            else
        //            {
        //                DoCallBack(ComQueueEventArgs.ComQueueReturnState.UnableToOpenComPort, "");
        //            }
        //            break;

        //        case ComPortEvent.ComPortEventType.SendCommand:
        //            OpenComPort();
        //            if (comPort.IsOpen)
        //                comPort.WriteTextLine(Queue[0].CommandToSend, true, true);

        //            if (comPort.IsOpen)
        //            {
        //                DoCallBack(ComQueueEventArgs.ComQueueReturnState.Success, "");
        //            }
        //            else
        //            {
        //                DoCallBack(ComQueueEventArgs.ComQueueReturnState.UnableToOpenComPort, "");
        //            }
        //            break;

        //        case ComPortEvent.ComPortEventType.SendQuery:
        //            OpenComPort();
        //            if (comPort.IsOpen)
        //            {
        //                State = ComPortEventState.AwaitingReply;
        //                CurrentRetries = Queue[0].Retries;
        //                ComTimer.Interval = Queue[0].TimeOut;
        //                comPort.WriteTextLine(Queue[0].CommandToSend, true, true);
        //                DataFound = false;
        //                ComTimer.Enabled = true;
        //            }
        //            else
        //            {
        //                DoCallBack(ComQueueEventArgs.ComQueueReturnState.UnableToOpenComPort, "");
        //            }
        //            break;

        //        case ComPortEvent.ComPortEventType.WaitTime:
        //            State = ComPortEventState.AwaitingTimeOut;
        //            ComTimer.Interval = Queue[0].TimeOut;
        //            ComTimer.Enabled = true;
        //            break;
        //    }
        //}

        private void Enqueue(ComPortEvent Event)
        {
            Queue.Add(Event);

           //
        }

        public void OpenPort(int SequenceNumber)
        {
            ComPortEvent Event = new ComPortEvent();

            Event.EventType = ComPortEvent.ComPortEventType.ComPortOpen;
            Event.SequenceNumber = SequenceNumber;

            Enqueue(Event);
        }

        public void ClosePort(int SequenceNumber)
        {
            ComPortEvent Event = new ComPortEvent();

            Event.EventType = ComPortEvent.ComPortEventType.ComPortClose;
            Event.SequenceNumber = SequenceNumber;

            Enqueue(Event);
        }

        public void SendPortCommand(int SequenceNumber, string Command)
        {
            ComPortEvent Event = new ComPortEvent();

            Event.EventType = ComPortEvent.ComPortEventType.SendCommand;
            Event.CommandToSend = Command;
            Event.SequenceNumber = SequenceNumber;

            Enqueue(Event);
        }

        public void AwaitPortString(int SequenceNumber, string MustContain, int TimeOut)
        {
            ComPortEvent Event = new ComPortEvent();

            Event.EventType = ComPortEvent.ComPortEventType.AwaitReply;
            Event.MustContain = MustContain;
            Event.TimeOut = TimeOut;
            Event.SequenceNumber = SequenceNumber;

            Enqueue(Event);
        }

        public void SendPortQuery(int SequenceNumber, string Query, string MustContain, int TimeOut, int Retries)
        {
            ComPortEvent Event = new ComPortEvent();

            Event.EventType = ComPortEvent.ComPortEventType.SendQuery;
            Event.CommandToSend = Query;
            Event.MustContain = MustContain;
            Event.TimeOut = TimeOut;
            Event.Retries = Retries;
            Event.SequenceNumber = SequenceNumber;

            Enqueue(Event);
        }

        public void WaitPortTime(int SequenceNumber, int TimeOut)
        {
            ComPortEvent Event = new ComPortEvent();

            Event.EventType = ComPortEvent.ComPortEventType.WaitTime;
            Event.TimeOut = TimeOut;
            Event.SequenceNumber = SequenceNumber;

            Enqueue(Event);
        }

    }
}

