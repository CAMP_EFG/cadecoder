﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

//using CommStudio.Barcodes;
//using CommStudio.Connections;
//using CommStudio.Transfers;

namespace LMU_Terminal
{
    //=================================================================================================
    // BASED OFF OF SAX COMM STUDIO COMM CONNECTION
    //=================================================================================================

    public enum ComPortBaudRate
    {
        baud_110 = 110,
        baud_300 = 300,
        baud_1200 = 1200,
        baud_2400 = 2400,
        baud_4800 = 4800,
        baud_9600 = 9600,
        baud_19200 = 19200,
        baud_38400 = 38400,
        baud_57600 = 57600,
        baud_115200 = 115200,
        baud_230400 = 230400,
        //baud_460800 = 460800,
        //baud_921600 = 921600
    }

    public enum ComPortDataBits
    {
        Five = 5,
        Six = 6,
        Seven = 7,
        Eight = 8
    }
    
    public enum ComPortParity
    {
        None = 0,
        Odd = 1,
        Even = 2,
        Mark = 3,
        Space = 4
    }

    public enum ComPortStopBits
    {
        One = 0,
        OnePointFive = 1,
        Two = 2
    }

    public class ComSettings
    {
        private bool suppressSettingExceptions = true;

        private static readonly string[] ParityNames = {"None", "Odd", "Even", "Mark", "Space"};
        private static readonly string[] StopBitNames = { "1", "1.5", "2" };

        private int portNumber;
        private ComPortBaudRate baudRate;
        private ComPortDataBits dataBits;
        private ComPortParity parity;
        private ComPortStopBits stopBits;

        public ComSettings() //Constructor
        {
            portNumber = 1;
            baudRate = ComPortBaudRate.baud_115200;
            dataBits = ComPortDataBits.Eight;
            parity = ComPortParity.None;
            stopBits = ComPortStopBits.One;
        }

        public ComSettings(ComSettings value) //Constructor
        {
            portNumber = value.portNumber;
            baudRate = value.baudRate;
            dataBits = value.dataBits;
            parity = value.parity;
            stopBits = value.stopBits;
        }

        public int PortNumber
        {
            get
            {
                return portNumber;
            }
            set
            {
                if ((value >= 1) && (value <= 99))
                    portNumber = value;
                else if (!suppressSettingExceptions)
                    throw new Exception("Invalid PortNumber");
            }
        }

        public ComPortBaudRate BaudRate
        {
            get
            {
                return baudRate;
            }
            set
            {
                switch (value)
                {
                    case ComPortBaudRate.baud_110:
                    case ComPortBaudRate.baud_300:
                    case ComPortBaudRate.baud_1200:
                    case ComPortBaudRate.baud_2400:
                    case ComPortBaudRate.baud_4800:
                    case ComPortBaudRate.baud_9600:
                    case ComPortBaudRate.baud_19200:
                    case ComPortBaudRate.baud_38400:
                    case ComPortBaudRate.baud_57600:
                    case ComPortBaudRate.baud_115200:
                    case ComPortBaudRate.baud_230400:
                    //case ComPortBaudRate.baud_460800:
                    //case ComPortBaudRate.baud_921600:
                        baudRate = value;
                        break;

                    default:
                        if (!suppressSettingExceptions)
                            throw new Exception("Invalid BaudRate");
                        break;
                }
            }
        }

        public ComPortDataBits DataBits // Five, Six, Seven, Eight
        {
            get
            {
                return dataBits;
            }
            set
            {
                if ((value >= ComPortDataBits.Five) && (value <= ComPortDataBits.Eight))
                {
                    dataBits = value;
                }
                else if (!suppressSettingExceptions)
                    throw new Exception("Invalid DataBits");
            }
        }

        public ComPortParity Parity // None, Odd, Even, Mark, Space
        {
            get
            {
                return parity;
            }
            set
            {
                if ((value >= ComPortParity.None) && (value <= ComPortParity.Space))
                {
                    parity = value;
                }
                else if (!suppressSettingExceptions)
                    throw new Exception("Invalid Parity");
            }
        }

        public ComPortStopBits StopBits // One, OnePointFive, Two
        {
            get
            {
                return stopBits;
            }
            set
            {
                if ((value >= ComPortStopBits.One) && (value <= ComPortStopBits.Two))
                {
                    stopBits = value;
                }
                else if (!suppressSettingExceptions)
                    throw new Exception("Invalid StopBits");
            }
        }

        public override string ToString()
        {
            return "COM" + portNumber.ToString() + ", " + 
                   ((int)baudRate).ToString() + ", " + 
                   ((int)DataBits).ToString() + ", " + 
                   ParityNames[(int)parity][0] + ", " + 
                   StopBitNames[(int)stopBits];
        }

        public override bool Equals(System.Object obj)
        {
            if (obj == null)                            // If parameter is null return false.
                return false;

            ComSettings p = obj as ComSettings;         // If parameter cannot be cast to Point return false.
            if ((System.Object)p == null)
                return false;

            return (portNumber == p.portNumber) &&
                   (baudRate == p.baudRate) &&
                   (dataBits == p.dataBits) &&
                   (parity == p.parity) &&
                   (stopBits == p.stopBits);
        }

        public bool Equals(ComSettings p)
        {
            if (p == null)                            // If parameter is null return false.
                return false;

            return (portNumber == p.portNumber) &&
                   (baudRate == p.baudRate) &&
                   (dataBits == p.dataBits) &&
                   (parity == p.parity) &&
                   (stopBits == p.stopBits);
        }

        public override int GetHashCode()
        {
            return ((portNumber << 24) | (int)baudRate) ^ (((int)dataBits << 8) | ((int)parity << 4) | (int)stopBits);
        }
    }

    public class ComPort
    {
        // local copies of Sax Com classes
       // private SerialConnection serialConnection;

        // local copies of properties
        private ComSettings comSettings;
        private bool dataSetReady;
        private bool clearToSend;
        private bool ringIndicator;
        private bool breakReceived;
        private bool receiveTextLines;

        // local storage for serial data
        private List<String> textLines;
//        private List<String> syncEscLines;

        //=================================================================================================
        // SAX COMM STUDIO COMM CONNECTION
        //=================================================================================================

        //================================================================
        // Constructors and Destructors
        //================================================================

        public ComPort() //Constructor
        {
            comSettings = new ComSettings();
            //RefreshComPort();
        }

        ~ComPort() //Destructor
        {
            Close();
        }

        //================================================================
        // Class Events
        //================================================================

        public event EventHandler TextLineReceived;
        public event EventHandler Opened;
        public event EventHandler Closed;
        public event EventHandler DsrChanged;
        public event EventHandler CtsChanged;
        public event EventHandler RingDetected;
        public event EventHandler BreakReceived;

        protected virtual void OnTextLineReceived(EventArgs e)
        {
            EventHandler handler = TextLineReceived;
            if (handler != null)
                handler(this, e);
        }

        protected virtual void OnOpened(EventArgs e)
        {
            EventHandler handler = Opened;
            if (handler != null)
                handler(this, e);
        }

        protected virtual void OnClosed(EventArgs e)
        {
            EventHandler handler = Closed;
            if (handler != null)
                handler(this, e);
        }

        protected virtual void OnDsrChanged(EventArgs e)
        {
            EventHandler handler = DsrChanged;
            if (handler != null)
                handler(this, e);
        }

        protected virtual void OnCtsChanged(EventArgs e)
        {
            EventHandler handler = CtsChanged;
            if (handler != null)
                handler(this, e);
        }

        protected virtual void OnRingDetected(EventArgs e)
        {
            EventHandler handler = RingDetected;
            if (handler != null)
                handler(this, e);
        }

        protected virtual void OnBreakReceived(EventArgs e)
        {
            EventHandler handler = BreakReceived;
            if (handler != null)
                handler(this, e);
        }

        //================================================================
        // Class Properties
        //================================================================

        //public bool IsOpen
        //{
        //    get
        //    {
        //        //return isOpen();
        //    }
        //}

        //public bool Dsr
        //{
        //    get
        //    {
        //        dataSetReady = serialConnection.Dsr;
        //        return dataSetReady;
        //    }
        //}

        //public bool Cts
        //{
        //    get
        //    {
        //        clearToSend = serialConnection.Cts;
        //        return clearToSend;
        //    }
        //}

        //public bool Ring
        //{
        //    get
        //    {
        //        ringIndicator = serialConnection.Ring;
        //        return ringIndicator;
        //    }
        //}

        //public bool Break
        //{
        //    get
        //    {
        //        breakReceived = serialConnection.Break;
        //        return breakReceived;
        //    }
        //}

        public int TextLineCount
        {
            get
            {
                int retVal = -1;

                if (receiveTextLines && (textLines != null) && (textLines.Count >= 0))
                    retVal = textLines.Count;

                return retVal;
            }
        }

        public bool ReceiveTextLines
        {
            get
            {
                return receiveTextLines;
            }
            set
            {
                if (receiveTextLines != value)
                {
                    if(receiveTextLines)
                    {
                        textLines = new List<String>();
                    }
                    else
                    {
                        textLines = null;
                    }
                }

                receiveTextLines = value;
            }
        }

        public ComSettings Settings
        {
            get
            {
                return new ComSettings(comSettings);
            }
        }

        //================================================================
        // Class Methods
        //================================================================

        public void SetSettings(ComSettings value)
        {
            comSettings = new ComSettings(value);

            //RefreshComPort();
        }

        //public void ShowPropertiesDialog(System.Windows.Forms.IWin32Window owner)
        //{
        //    if (serialConnection != null)
        //    {
        //        Close();
        //        if (HasComPorts())
        //        {
        //            serialConnection.ShowPropertiesDialog(owner);
        //            comSettings = CopySettings(serialConnection.Options);
        //        }
        //        else
        //        {
        //            System.Windows.Forms.MessageBox.Show("No Com Ports found!");
        //        }
        //    }
        //}

        //public void ShowPropertiesDialog()
        //{
        //    if (serialConnection != null)
        //    {
        //        Close();
        //        if (HasComPorts())
        //        {
        //            serialConnection.ShowPropertiesDialog();
        //            comSettings = CopySettings(serialConnection.Options);
        //        }
        //        else
        //        {
        //            System.Windows.Forms.MessageBox.Show("No Com Ports found!");
        //        }
        //    }
        //}

        //public void WriteTextLine(String text, bool appendCr, bool appendLf)
        //{
        //    String textToSend = text;

        //    if (appendCr) 
        //        textToSend += "\r";

        //    if (appendLf)
        //        textToSend += "\n";

        //    if ((serialConnection != null) && serialConnection.IsOpen)
        //    {
        //        serialConnection.Write(textToSend);
        //    }
        //}

        public String ReadTextLine()
        {
            String retVal = null;
            String test = "";
            if (receiveTextLines && (textLines != null) && (textLines.Count > 0))
            {
                if (textLines[0] != null)
                {
                    retVal = textLines[0].ToString();
                }
                textLines.RemoveAt(0);
            }

            return retVal;
        }

        //public void Open()
        //{
        //    if (!serialConnection.IsOpen)
        //        try
        //        {
        //            RefreshComPort();

        //            serialConnection.Open();
        //        }
        //        catch (Exception error)
        //        {
        //            if (error is IOException)
        //            {
        //                if (serialConnection.IsOpen)
        //                    serialConnection.Close();
        //                Console.WriteLine("OpenComPort: " + error.Message);
        //            }
        //        }
        //}

        public void Close()
        {

        }

        //public override string ToString()
        //{
        //    string retVal;

        //    //if (isOpen())
        //    //    retVal = "OPENED: ";
        //    //else
        //    //    retVal = "CLOSED: ";
            
        //    //return retVal + comSettings.ToString();
        //}

        public override bool Equals(System.Object obj)
        {
            if (obj == null)                                                // If parameter is null return false.
                return false;

            ComPort p = obj as ComPort;                                     // If parameter cannot be cast to Point return false.
            if ((System.Object)p == null)
                return false;

            if ((comSettings == null) || (p.comSettings == null))           // If Settings are not available
                return false;

            return (comSettings.PortNumber == p.comSettings.PortNumber);    // Consider it the same ComPort if it has the same PortNumber
        }

        public bool Equals(ComPort p)
        {
            if (p == null)                                                  // If parameter is null return false.
                return false;

            if ((comSettings == null) || (p.comSettings == null))           // If Settings are not available
                return false;

            return (comSettings.PortNumber == p.comSettings.PortNumber);    // Consider it the same ComPort if it has the same PortNumber
        }

        public override int GetHashCode()
        {
            if (comSettings == null)
                return 0;            

            return comSettings.GetHashCode();
        }

        //================================================================
        // Serial Port Events
        //================================================================

        private void serialConnection_DataAvailable(object sender, EventArgs e)
        {
            String TextLine = "";

            if (receiveTextLines)
            {
                if (textLines == null)
                    textLines = new List<String>();

                //try
                //{
                //    while ((serialConnection.Available > 0) && (TextLine != null))
                //    {
                //        TextLine = serialConnection.ReadLine();

                //        if (TextLine != null)
                //        {
                //            textLines.Add(TextLine);
                //            OnTextLineReceived(new EventArgs());
                //        }
                //    }
                //}
                //catch (Exception exception)
                //{
                //    Console.WriteLine("serialConnection_DataAvailable " + exception.Message);
                //}
            }
        }

        private void serialConnection_Opened(object sender, EventArgs e)
        {
            OnOpened(new EventArgs());
        }

        private void serialConnection_Closed(object sender, EventArgs e)
        {
            OnClosed(new EventArgs());
        }

        //private void serialConnection_DsrChanged(object sender, EventArgs e)
        //{
        //    dataSetReady = serialConnection.Dsr;
        //    OnDsrChanged(new EventArgs());
        //}

        //private void serialConnection_CtsChanged(object sender, EventArgs e)
        //{
        //    clearToSend = serialConnection.Cts;
        //    OnCtsChanged(new EventArgs());
        //}

        //private void serialConnection_RingDetected(object sender, EventArgs e)
        //{
        //    ringIndicator = serialConnection.Ring;
        //    OnRingDetected(new EventArgs());
        //}

        //private void serialConnection_BreakReceived(object sender, EventArgs e)
        //{
        //    breakReceived = serialConnection.Break;
        //    OnBreakReceived(new EventArgs());
        //}

        //================================================================
        // Internal Functions
        //================================================================

        private bool HasComPorts()
        {
            string[] ports = System.IO.Ports.SerialPort.GetPortNames();

            return (ports.Count() != 0);
        }

        //private bool isOpen()
        //{
        //    if (serialConnection == null)
        //        return false;
        //    else
        //        return serialConnection.IsOpen;
        //}

        //private SerialOptions CopyOptions(ComSettings comSettings)
        //{
        //    SerialOptions options = new SerialOptions();

        //    options.PortName = "COM" + comSettings.PortNumber.ToString();
        //    options.BaudRate = (int)comSettings.BaudRate;
        //    options.DataBits = (int)comSettings.DataBits;
        //    options.Parity = (Parity)comSettings.Parity;
        //    options.StopBits = (CommStopBits)comSettings.StopBits;
        //    options.FlowControlDsrDtr = false;
        //    options.FlowControlCtsRts = false;
        //    options.FlowControlRs485 = false;
        //    options.FlowControlXonXoff = false;
        //    options.EnableDtr = true;
        //    options.EnableRts = true;

        //    return options;
        //}

        //private ComSettings CopySettings(SerialOptions options)
        //{
        //    ComSettings settings = new ComSettings();

        //    if (serialConnection != null)
        //    {
        //        settings.PortNumber = int.Parse(options.PortName.Substring(3));
        //        settings.BaudRate = (ComPortBaudRate)options.BaudRate;
        //        settings.DataBits = (ComPortDataBits)options.DataBits;
        //        settings.Parity = (ComPortParity)options.Parity;
        //        settings.StopBits = (ComPortStopBits)options.StopBits;
        //    }
        //    return settings;
        //}

        //private void RefreshComPort()
        //{
        //    Boolean wasOpen = ((serialConnection != null) && (serialConnection.IsOpen));

        //    if (wasOpen)
        //        Close();

        //    serialConnection = new SerialConnection();
        //    serialConnection.DataAvailableThreshold = 1;
        //    serialConnection.DataAvailable += new Connection.DataAvailableEventHandler(serialConnection_DataAvailable);
        //    serialConnection.Opened += new EventHandler(serialConnection_Opened);
        //    serialConnection.Closed += new EventHandler(serialConnection_Closed);
        //    serialConnection.DsrChanged += new SerialConnection.DsrEventHandler(serialConnection_DsrChanged);
        //    serialConnection.CtsChanged += new SerialConnection.CtsEventHandler(serialConnection_CtsChanged);
        //    serialConnection.RingDetected += new SerialConnection.RingEventHandler(serialConnection_RingDetected);
        //    serialConnection.BreakReceived += new SerialConnection.BreakEventHandler(serialConnection_BreakReceived);
        //    serialConnection.SetOptions(CopyOptions(comSettings));

        //    if (wasOpen)
        //        Open();
        //}








        //================================================================
        // Static Functions
        //================================================================


    }
}
