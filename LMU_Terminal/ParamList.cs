﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LMU_Terminal
{
    public struct CommentEntryType
    {
        public int CommentIndex;
        public string Comment;

        public CommentEntryType(int commentIndex, string comment)
        {
            CommentIndex = commentIndex;
            Comment = comment;
        }
    }

    public struct MsgEntryType
    {
        public String Param;
        public String Value;
        public String Units;
        public String Hex;
        public String Comment;
        public String Revision;
    }

    public enum ParamType
    {
        Hex,
        Binary,
        Sint,
        Uint,
        Alpha,
        IpAddress,
        UnixTime,
        PackedBcd
    }

    public enum Endian
    {
        Big,
        Little
    }

    public class Comment
    {
        private bool BitMode;
        private CommentEntryType[] Comments;

        private void CopyComment(string comment, bool bitMode)
        {
            Comments = new CommentEntryType[1];
            Comments[0] = new CommentEntryType(0, comment);
            BitMode = bitMode;
        }

        private void CopyComment(string[] comments, bool bitMode)
        {
            int i;
            Comments = new CommentEntryType[comments.Length];

            for (i = 0; i < comments.Length; i++)
            {
                Comments[i].CommentIndex = i;
                Comments[i].Comment = comments[i];
            }
            BitMode = bitMode;
        }

        private void CopyComment(CommentEntryType[] comments, bool bitMode)
        {
            int i;
            Comments = new CommentEntryType[comments.Length];

            for (i = 0; i < comments.Length; i++)
            {
                Comments[i].CommentIndex = comments[i].CommentIndex;
                Comments[i].Comment = comments[i].Comment;
            }
            BitMode = bitMode;
        }

        public Comment(string comment)
        {
            CopyComment(comment, false);
        }

        public Comment(string comment, bool bitMode)
        {
            CopyComment(comment, bitMode);
        }

        public Comment(string[] comments)
        {
            CopyComment(comments, false);
        }

        public Comment(string[] comments, bool bitMode)
        {
            CopyComment(comments, bitMode);
        }

        public Comment(CommentEntryType[] comments)
        {
            CopyComment(comments, false);
        }

        public Comment(CommentEntryType[] comments, bool bitMode)
        {
            CopyComment(comments, bitMode);
        }

        public string GetComment(int Index)
        {
            int i;
            string retVal = "";

            if (BitMode)
            {
                for (i = 0; i < Comments.Length; i++)
                {
                    if ((Comments[i].CommentIndex <= 31) &&
                        (Index & (1 << Comments[i].CommentIndex)) != 0)
                    {
                        if (retVal != "")
                            retVal += "; ";
                        retVal += "bit" + i.ToString() + "=" + Comments[i].Comment;
                    }
                }
            }
            else
            {
                if (Comments.Length == 1)
                {
                    retVal = Comments[0].Comment;
                }
                else if (Comments.Length > 1)
                {
                    retVal = "Invalid";
                    for (i = 0; i < Comments.Length; i++)
                    {
                        if (Index == Comments[i].CommentIndex)
                        {
                            retVal = Comments[i].Comment;
                            break;
                        }
                    }
                }
            }
            return retVal;
        }
    }

    class ParamList
    {
        bool Add0x = false;
        MsgEntryType Header;
        List<MsgEntryType> Entries;

        public ParamList(String param, String value, String units, String hex, String comment, String revision)
        {
            Entries = new List<MsgEntryType>();
            Header = new MsgEntryType();

            Header.Param = param;
            Header.Value = value;
            Header.Units = units;
            Header.Hex = hex;
            Header.Comment = comment;
            Header.Revision = revision;
        }

        public void FillDataGrid(DataGridView dgv)
        {
            int paramCol = 0;
            int valCol = 1;
            int unitCol = 2;
            int hexCol = 3;
            int comCol = 4;
            int revCol = 5;
            int columns = 4;
            int i;

            bool addUnits = (Header.Units != "");
            bool addRevision = (Header.Revision != "");

            if (addUnits) columns++;
            if (addRevision) columns++;

            dgv.ColumnCount = columns;
            dgv.RowCount = Entries.Count;

            if (!addUnits)
            {
                hexCol--;
                comCol--;
                revCol--;
            }

            dgv.Columns[paramCol].HeaderText = Header.Param;
            dgv.Columns[valCol].HeaderText = Header.Value;
            if (addUnits) dgv.Columns[unitCol].HeaderText = Header.Units;
            dgv.Columns[hexCol].HeaderText = Header.Hex;
            dgv.Columns[comCol].HeaderText = Header.Comment;
            if (addRevision) dgv.Columns[revCol].HeaderText = Header.Revision;
            dgv.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;

            for (i = 0; i < Entries.Count; i++)
            {
                dgv[paramCol, i].Value = Entries[i].Param;
                dgv[valCol, i].Value = Entries[i].Value;
                if (addUnits) dgv[unitCol, i].Value = Entries[i].Units;
                dgv[hexCol, i].Value = Entries[i].Hex;
                dgv[comCol, i].Value = Entries[i].Comment;
                if (addRevision) dgv[revCol, i].Value = Entries[i].Revision;
            }
        }

        public byte AddDecodedBitParam(byte data, byte startbit, byte stopbit, String paramTitle, String units, String comment)
        {
            return AddDecodedBitParam(data, startbit, stopbit, paramTitle, units, new Comment(comment), "");
        }

        public byte AddDecodedBitParam(byte data, byte startbit, byte stopbit, String paramTitle, String units, String comment, String revision)
        {
            return AddDecodedBitParam(data, startbit, stopbit, paramTitle, units, new Comment(comment), revision);
        }

        public byte AddDecodedBitParam(byte data, byte startbit, byte stopbit, String paramTitle, String units, Comment comment)
        {
            return AddDecodedBitParam(data, startbit, stopbit, paramTitle, units, comment, "");
        }

        public byte AddDecodedBitParam(byte data, byte startbit, byte stopbit, String paramTitle, String units, Comment comment, String revision)
        {
            MsgEntryType Entry = new MsgEntryType();
            int i;
            byte Value = 0;
            ushort Len = 1;

            if ((stopbit - startbit) > 3)
                Len = 2;

            if ((stopbit <= 7) && (stopbit >= startbit))
            {
                for (i = stopbit; i >= startbit; i--)
                {
                    Value <<= 1;
                    if ((data & (1 << i)) != 0)
                    {
                        Value |= 1;
                    }
                }
            }

            Entry.Param = paramTitle;
            Entry.Value = Value.ToString();
            Entry.Units = units;
            Entry.Hex = Utility.Dec2Hex(Value, Len, Add0x);
            Entry.Comment = comment.GetComment(Value);
            Entry.Revision = revision;
            Entries.Add(Entry);

            return Value;
        }

        public double AddDecodedParam(byte[] data, ref int startbyte, int numbytes, ParamType pt, Endian endian, String paramTitle, String units, String comment)
        {
            return AddDecodedParam(data, ref startbyte, numbytes, pt, endian, paramTitle, units, new Comment(comment), "");
        }

        public double AddDecodedParam(byte[] data, ref int startbyte, int numbytes, ParamType pt, Endian endian, String paramTitle, String units, String comment, String revision)
        {
            return AddDecodedParam(data, ref startbyte, numbytes, pt, endian, paramTitle, units, new Comment(comment), revision);
        }

        public double AddDecodedParam(byte[] data, ref int startbyte, int numbytes, ParamType pt, Endian endian, String paramTitle, String units, Comment comment)
        {
            return AddDecodedParam(data, ref startbyte, numbytes, pt, endian, paramTitle, units, comment, "");
        }

        public double AddDecodedParam(byte[] data, ref int startbyte, int numBytes, ParamType pt, Endian endian, String paramTitle, String units, Comment comment, String revision)
        {
            MsgEntryType Entry = new MsgEntryType();
            DateTime BaseDate = new DateTime(1970, 1, 1);
            DateTime TmpDate;
            int stopbyte = startbyte + numBytes - 1;
            int start = startbyte;
            int stop = stopbyte + 1;
            int inc = 1;
            int i;
            double Adder = 0;
            double Value = 0;
            String ValString = "";
            String HexString = "";


            if (endian == Endian.Little)
            {
                start = stopbyte;
                stop = startbyte - 1;
                inc = -1;
            }

            if ((numBytes > 0) && (stopbyte < data.Length))
            {
                i = start;

                if ((data[i] & 0x80) != 0)
                    Adder = -1;

                do
                {
                    HexString += Utility.Dec2Hex(data[i], 2, Add0x);

                    Adder *= 256;
                    Value *= 256;
                    Value += data[i];

                    switch (pt)
                    {
                        case ParamType.Alpha:
                            ValString += (char)data[i];
                            break;

                        case ParamType.Binary:
                            ValString += Utility.Dec2Bin(data[i], Add0x);
                            break;

                        case ParamType.IpAddress:
                            if (ValString != "")
                                ValString += ".";
                            ValString += data[i].ToString();
                            break;

                        default:
                            break;
                    }
                    i += inc;
                }
                while (i != stop);
            }

            switch (pt)
            {
                case ParamType.Uint:
                    ValString = ((UInt64)Value).ToString();
                    break;

                case ParamType.Sint:
                    Value += Adder;
                    ValString = ((Int64)Value).ToString();
                    break;

                case ParamType.UnixTime:
                    TmpDate = new DateTime((Int64)(Value * 10000000) + BaseDate.Ticks);
                    ValString = TmpDate.ToShortDateString() + " " + TmpDate.ToLongTimeString();
                    break;

                case ParamType.Hex:
                    ValString = HexString;
                    break;

                case ParamType.PackedBcd:
                    ValString = HexString.Replace("F","");
                    break;

                default:
                    break;
            }

            Entry.Param = paramTitle;
            Entry.Value = ValString;
            Entry.Units = units;
            Entry.Hex = HexString;
            Entry.Comment = comment.GetComment((int)Value);
            Entry.Revision = revision;
            Entries.Add(Entry);

            //Increment the position
            startbyte += numBytes;

            return Value;
        }

        public double AddScaledParam(byte[] data, ref int startbyte, int numBytes, double slope, double intercept, bool signed, Endian endian, String paramTitle, String units, String comment)
        {
            return AddScaledParam(data, ref startbyte, numBytes, slope, intercept, signed, endian, paramTitle, units, new Comment(comment), "");
        }

        public double AddScaledParam(byte[] data, ref int startbyte, int numBytes, double slope, double intercept, bool signed, Endian endian, String paramTitle, String units, String comment, String revision)
        {
            return AddScaledParam(data, ref startbyte, numBytes, slope, intercept, signed, endian, paramTitle, units, new Comment(comment), revision);
        }

        public double AddScaledParam(byte[] data, ref int startbyte, int numBytes, double slope, double intercept, bool signed, Endian endian, String paramTitle, String units, Comment comment)
        {
            return AddScaledParam(data, ref startbyte, numBytes, slope, intercept, signed, endian, paramTitle, units, comment, "");
        }

        public double AddScaledParam(byte[] data, ref int startbyte, int numBytes, double slope, double intercept, bool signed, Endian endian, String paramTitle, String units, Comment comment, String revision)
        {
            MsgEntryType Entry = new MsgEntryType();
            int stopbyte = startbyte + numBytes - 1;
            int start = startbyte;
            int stop = stopbyte + 1;
            int inc = 1;
            int i;
            double Adder = 0;
            double Value = 0;
            String HexString = "";
            String ASCII_value = "";

            if (endian == Endian.Little)
            {
                start = stopbyte;
                stop = startbyte - 1;
                inc = -1;
            }

            if ((numBytes > 0) && (stopbyte < data.Length))
            {
                i = start;

                if ((data[i] & 0x80) != 0)
                    Adder = -1;
                if (units.Equals("ASCII"))
                {
                    do
                    {
                        HexString += Utility.Dec2Hex(data[i], 2, Add0x);
                        i += inc;
                    }
                    while (i != stop);
                    ASCII_value = Utility.AssembleHexBytesToString(HexString, "");
                }



            
                else
                {
                    do
                    {
                        HexString += Utility.Dec2Hex(data[i], 2, Add0x);

                        Adder *= 256;
                        Value *= 256;
                        Value += data[i];
                        i += inc;
                    }
                    while (i != stop);
                }
            }

            if (signed)
                Value += Adder;

            Value *= slope;
            Value += intercept;

            Entry.Param = paramTitle;
            if (units.Equals("ASCII"))
                { Entry.Value = ASCII_value; }
            else
                { Entry.Value = Value.ToString(); }
                       
            Entry.Units = units;
            Entry.Hex = HexString;
            Entry.Comment = comment.GetComment((int)Value);
            Entry.Revision = revision;
            Entries.Add(Entry);

            //Increment the position
            startbyte += numBytes;

            return Value;
        }

        public double AddNullTerminatedString(byte[] data, ref int startbyte, String comment, String revision)
        {
            return AddNullTerminatedString(data, ref startbyte, new Comment(comment), revision);
        }

        public double AddNullTerminatedString(byte[] data, ref int startbyte, Comment comment, String revision)
        {
            MsgEntryType Entry = new MsgEntryType();
            String paramTitle = "";
            String HexString = "";
            String curString = "";
            double Value = 0;

            while ((startbyte < data.Length) && (data[startbyte] != 0))
            {
                HexString += Utility.Dec2Hex(data[startbyte], 2, Add0x);
                if (data[startbyte] == ':')
                {
                    paramTitle = curString;
                    curString = "";
                }
                else if ((data[startbyte] != ' ') || (curString != ""))
                {
                    curString += (char)data[startbyte];
                }
                startbyte++;
            }
            HexString += Utility.Dec2Hex(0, 2, Add0x);

            if ((paramTitle != "") || (curString != ""))
            {
                Entry.Param = paramTitle;
                Entry.Value = curString;
                Entry.Units = "";
                Entry.Hex = HexString;
                Entry.Comment = comment.GetComment(0);
                Entry.Revision = revision;
                Entries.Add(Entry);
                Value = 1;
            }

            return Value;
        }

        // Voltage parameters that are 2 bytes, unsigned, and scaled by 0.05
        private double AddJbusVoltageParam(byte[] data, ref int startbyte, Endian endian, String paramTitle, String comment, String revision)
        {
            return AddScaledParam(data, ref startbyte, 2, 0.05, 0, false, endian, paramTitle, "V", comment, revision);
        }

        // J1708 parameters that are 1 byte, unsigned, and scaled by 0.5
        private double AddJ1708PercentParam(byte[] data, ref int startbyte, Endian endian, String paramTitle, String comment, String revision)
        {
            return AddScaledParam(data, ref startbyte, 1, 0.5, 0, false, endian, paramTitle, "%", comment, revision);
        }

        // J1939 parameters that are 1 byte, unsigned, and scaled by 0.4
        private double AddJ1939PercentParam(byte[] data, ref int startbyte, Endian endian, String paramTitle, String comment, String revision)
        {
            return AddScaledParam(data, ref startbyte, 1, 0.4, 0, false, endian, paramTitle, "%", comment, revision);
        }

        public double AddJbusParam(byte[] data, ref int startbyte, JbusParam MsgType, Endian endian, String paramTitle, String revision)
        {
            string[] Spn1856Str = { "Not Buckled - SPN 1856 Format",
                                    "Buckled - SPN 1856 Format",
                                    "Error - SPN 1856 Format",
                                    "Not Available - SPN 1856 Format"
                                  };

            string[] CsfJ1708Str = { "Active", "Inactive" };
            string[] DctJ1708Str = { "Expansion", "Standard" };
            string[] LciJ1708Str = { "PID", "SID" };

            MsgEntryType Entry;
            int NumDTCs;
            int OCI = 0;
            int temp = 0;
            double retVal = 0;
            switch (MsgType)
            {
                case JbusParam.Pid84:
                    retVal = AddScaledParam(data, ref startbyte, 1, 0.5, 0, false, endian, paramTitle, "mph", "PID 84 Format", revision);
                    break;
                case JbusParam.Pid110:
                    retVal = AddScaledParam(data, ref startbyte, 1, 1, 0, true, endian, paramTitle, "°F", "PID 110 Format", revision);
                    break;
                case JbusParam.Pid158:
                    retVal = AddJbusVoltageParam(data, ref startbyte, endian, paramTitle, "PID 158 Format", revision);
                    break;
                case JbusParam.Pid168:
                    retVal = AddJbusVoltageParam(data, ref startbyte, endian, paramTitle, "PID 168 Format", revision);
                    break;
                case JbusParam.Pid175:
                    retVal = AddScaledParam(data, ref startbyte, 2, 0.25, 0, true, endian, paramTitle, "°F", "PID 175 Format", revision);
                    break;
                case JbusParam.Pid190:
                    retVal = AddScaledParam(data, ref startbyte, 2, 0.25, 0, false, endian, paramTitle, "RPM", "PID 190 Format", revision);
                    break;
                case JbusParam.Pid235:
                    retVal = AddScaledParam(data, ref startbyte, 4, 0.05, 0, false, endian, paramTitle, "hours", "PID 235 Format", revision);
                    break;
                case JbusParam.Pid236:
                    retVal = AddScaledParam(data, ref startbyte, 4, 0.125, 0, false, endian, paramTitle, "gal", "PID 236 Format", revision);
                    break;
                case JbusParam.Pid237:
                case JbusParam.Spn237:
                    retVal = AddDecodedParam(data, ref startbyte, 17, ParamType.Alpha, Endian.Big, paramTitle, "", "PID/SPN 237 Format", revision);
                    break;
                case JbusParam.Pid245:
                    retVal = AddScaledParam(data, ref startbyte, 4, 0.1, 0, false, endian, paramTitle, "miles", "PID 245 Format", revision);
                    break;
                case JbusParam.Pid247:
                    retVal = AddScaledParam(data, ref startbyte, 4, 0.05, 0, false, endian, paramTitle, "hours", "PID 247 Format", revision);
                    break;
                case JbusParam.Pid250:
                    retVal = AddScaledParam(data, ref startbyte, 4, 0.125, 0, false, endian, paramTitle, "gal", "PID 250 Format", revision);
                    break;

                case JbusParam.Spn38:
                    retVal = AddJ1939PercentParam(data, ref startbyte, endian, paramTitle, "SPN 38 Format", revision);
                    break;
                case JbusParam.Spn84:
                    retVal = AddScaledParam(data, ref startbyte, 2, (1.0 / 256), 0, false, endian, paramTitle, "kph", "SPN 84 Format", revision);
                    break;
                case JbusParam.Spn91:
                    retVal = AddJ1939PercentParam(data, ref startbyte, endian, paramTitle, "SPN 91 Format", revision);
                    break;
                case JbusParam.Spn96:
                    retVal = AddJ1939PercentParam(data, ref startbyte, endian, paramTitle, "SPN 96 Format", revision);
                    break;
                case JbusParam.Spn98:
                    retVal = AddJ1939PercentParam(data, ref startbyte, endian, paramTitle, "SPN 98 Format", revision);
                    break;
                case JbusParam.Spn100:
                    retVal = AddScaledParam(data, ref startbyte, 1, 4.0, 0, false, endian, paramTitle, "kPa", "SPN 100 Format", revision);
                    break;
                case JbusParam.Spn101:
                    retVal = AddScaledParam(data, ref startbyte, 2, 0.0078125, -250, false, endian, paramTitle, "kPa", "SPN 101 Format", revision);
                    break;
                case JbusParam.Spn109:
                    retVal = AddScaledParam(data, ref startbyte, 1, 2.0, 0, false, endian, paramTitle, "kPa", "SPN 109 Format", revision);
                    break;
                case JbusParam.Spn110:
                    retVal = AddScaledParam(data, ref startbyte, 1, 1, -40, false, endian, paramTitle, "°C", "SPN 110 Format", revision);
                    break;
                case JbusParam.Spn111:
                    retVal = AddJ1939PercentParam(data, ref startbyte, endian, paramTitle, "SPN 111 Format", revision);
                    break;
                case JbusParam.Spn157:
                    retVal = AddScaledParam(data, ref startbyte, 2, (1.0 / 256), 0, false, endian, paramTitle, "MPa", "SPN 157 Format", revision);
                    break;
                case JbusParam.Spn158:
                    retVal = AddJbusVoltageParam(data, ref startbyte, endian, paramTitle, "SPN 158 Format", revision);
                    break;
                case JbusParam.Spn168:
                    retVal = AddJbusVoltageParam(data, ref startbyte, endian, paramTitle, "SPN 168 Format", revision);
                    break;
                case JbusParam.Spn171:
                    retVal = AddScaledParam(data, ref startbyte, 2, 0.03125, -273, false, endian, paramTitle, "°C", "SPN 171 Format", revision);
                    break;
                case JbusParam.Spn172:
                    retVal = AddScaledParam(data, ref startbyte, 1, 1.0, -40, false, endian, paramTitle, "°C", "SPN 172 Format", revision);
                    break;
                case JbusParam.Spn174:
                    retVal = AddScaledParam(data, ref startbyte, 1, 1.0, -40, false, endian, paramTitle, "°C", "SPN 174 Format", revision);
                    break;
                case JbusParam.Spn175:
                    retVal = AddScaledParam(data, ref startbyte, 2, 0.03125, -273, false, endian, paramTitle, "°C", "SPN 175 Format", revision);
                    break;
                case JbusParam.Spn177:
                    retVal = AddScaledParam(data, ref startbyte, 2, 0.03125, -273, false, endian, paramTitle, "°C", "SPN 177 Format", revision);
                    break;
                case JbusParam.Spn183:
                    retVal = AddScaledParam(data, ref startbyte, 2, 0.05, 0, false, endian, paramTitle, "L/h", "SPN 183 Format", revision);
                    break;
                case JbusParam.Spn185:
                    retVal = AddScaledParam(data, ref startbyte, 2, 0.001953125, 0, false, endian, paramTitle, "km/L", "SPN 185 Format", revision);
                    break;
                case JbusParam.Spn190:
                    retVal = AddScaledParam(data, ref startbyte, 2, 0.125, 0, false, endian, paramTitle, "RPM", "SPN 190 Format", revision);
                    break;
                case JbusParam.Spn235:
                    retVal = AddScaledParam(data, ref startbyte, 4, 0.05, 0, false, endian, paramTitle, "hours", "SPN 235 Format", revision);
                    break;
                case JbusParam.Spn236:
                    retVal = AddScaledParam(data, ref startbyte, 4, 0.5, 0, false, endian, paramTitle, "L", "SPN 236 Format", revision);
                    break;
                case JbusParam.Spn245:
                    retVal = AddScaledParam(data, ref startbyte, 4, 0.125, 0, false, endian, paramTitle, "km", "SPN 245 Format", revision);
                    break;
                case JbusParam.Spn247:
                    retVal = AddScaledParam(data, ref startbyte, 4, 0.05, 0, false, endian, paramTitle, "hours", "SPN 247 Format", revision);
                    break;
                case JbusParam.Spn250:
                    retVal = AddScaledParam(data, ref startbyte, 4, 0.5, 0, false, endian, paramTitle, "L", "SPN 250 Format", revision);
                    break;
                case JbusParam.Spn441:
                    retVal = AddScaledParam(data, ref startbyte, 1, 1.0, -40, false, endian, paramTitle, "°C", "SPN 441 Format", revision);
                    break;
                case JbusParam.Spn513:
                    retVal = AddScaledParam(data, ref startbyte, 1, 1.0, -125, false, endian, paramTitle, "%", "SPN 513 Format", revision);
                    break;
                case JbusParam.Spn916:
                    retVal = AddScaledParam(data, ref startbyte, 2, 1.0, -32127, false, endian, paramTitle, "hours", "SPN 916 Format", revision);
                    break;
                case JbusParam.Spn917:
                    retVal = AddScaledParam(data, ref startbyte, 4, 5.0, 0, false, endian, paramTitle, "m", "SPN 917 Format", revision);
                    break;
                case JbusParam.Spn1761:
                    retVal = AddJ1939PercentParam(data, ref startbyte, endian, paramTitle, "SPN 1761 Format", revision);
                    break;
                case JbusParam.Spn1856:
                    retVal = AddDecodedBitParam(data[startbyte], 0, 1, paramTitle, "", new Comment(Spn1856Str, true), revision);
                    startbyte++;
                    break;
                case JbusParam.Spn3515:
                    retVal = AddScaledParam(data, ref startbyte, 1, 1.0, -40, false, endian, paramTitle, "°C", "SPN 3515 Format", revision);
                    break;
                case JbusParam.Spn3516:
                    retVal = AddScaledParam(data, ref startbyte, 1, 0.25, 0, false, endian, paramTitle, "%", "SPN 3516 Format", revision);
                    break;

                case JbusParam.DtcJ1708:
                    AddDecodedParam(data, ref startbyte, 1, ParamType.Uint, endian, "Protocol", "", "", revision);
                    AddDecodedParam(data, ref startbyte, 1, ParamType.Uint, endian, "Message Identifier (MID)", "", "", revision);
                    AddDecodedParam(data, ref startbyte, 1, ParamType.Uint, endian, "Data Length", "", "", revision);
                    NumDTCs = (int)AddDecodedParam(data, ref startbyte, 1, ParamType.Uint, endian, "Number of DTCs", "", "", revision);
                    retVal = NumDTCs;
                    AddDecodedParam(data, ref startbyte, 1, ParamType.Uint, endian, "Modified Count", "", "", revision);

                    while (NumDTCs-- > 0)
                    {
                        if (data.Length - startbyte >= 2)
                        {
                            Entry = new MsgEntryType();

                            temp = 0;
                            while (data[startbyte] == 0xFF)
                            {
                                startbyte++;
                                temp += 256;
                            }
                            temp += data[startbyte];
                            Entry.Value = temp.ToString();

                            if ((data[startbyte + 1] & 0x10) > 0)
                            {
                                Entry.Param = "Subsystem Identifier (SID)";
                            }
                            else
                            {
                                Entry.Param = "Parameter Identifier (PID)";
                            }
                            /*
                            if ((data[startbyte + 1] & 0x20) > 0)
                            {
                                temp = data[startbyte];
                            }
                            else
                            {
                                temp = data[startbyte] + 256;
                            }
                            */

                            Entry.Hex = Utility.Dec2Hex((ulong)temp, 3, Add0x);
                            Entry.Units = "";
                            Entry.Comment = "";
                            Entry.Revision = "-";
                            Entries.Add(Entry);

                            OCI = AddDecodedBitParam(data[startbyte + 1], 7, 7, "Occurrence Count Included (OCI)", "", "", revision);
                            AddDecodedBitParam(data[startbyte + 1], 6, 6, "Current Status of Fault (CSF)", "", new Comment(CsfJ1708Str, false), revision);
                            AddDecodedBitParam(data[startbyte + 1], 5, 5, "Diagnostic Code Type (DCT)", "", new Comment(DctJ1708Str, false), revision);
                            AddDecodedBitParam(data[startbyte + 1], 4, 4, "Low Character Identifier (LCI)", "", new Comment(LciJ1708Str, false), revision);
                            AddDecodedBitParam(data[startbyte + 1], 0, 3, "Failure Mode Indicator (FMI)", "", "", revision);
                            startbyte += 2;
                        }

                        if (OCI > 0) //OCI Included
                        {
                            AddDecodedParam(data, ref startbyte, 1, ParamType.Uint, endian, "Occurrence Count (OC)", "", "", revision);
                        }
                    }
                    break;

                case JbusParam.DtcJ1939:
                    AddDecodedParam(data, ref startbyte, 1, ParamType.Uint, endian, "Protocol", "", "", revision);
                    AddDecodedParam(data, ref startbyte, 1, ParamType.Uint, endian, "Source Address (SA)", "", "", revision);
                    AddDecodedParam(data, ref startbyte, 1, ParamType.Uint, endian, "Lamp Status", "", "", revision);
                    NumDTCs = (int)AddDecodedParam(data, ref startbyte, 1, ParamType.Uint, endian, "Number of DTCs", "", "", revision);
                    retVal = NumDTCs;
                    AddDecodedParam(data, ref startbyte, 1, ParamType.Uint, endian, "Modified Count", "", "", revision);

                    while (NumDTCs-- > 0)
                    {
                        if (data.Length - startbyte >= 3)
                        {
                            Entry = new MsgEntryType();
                            temp = ((((int)data[startbyte + 2] & 0xE0) << 11) | ((int)data[startbyte + 1] << 8) | (int)data[startbyte]);
                            Entry.Param = "Suspect Parameter Number (SPN)";
                            Entry.Hex = Utility.Dec2Hex((ulong)temp, 5, Add0x);
                            Entry.Value = temp.ToString();
                            Entry.Units = "";
                            Entry.Comment = "";
                            Entry.Revision = "-";
                            Entries.Add(Entry);

                            AddDecodedBitParam(data[startbyte + 2], 0, 4, "Failure Mode Indicator (FMI)", "", "", revision);
                            startbyte += 3;
                        }

                        if (data.Length - startbyte >= 1)
                        {
                            AddDecodedBitParam(data[startbyte], 7, 7, "Conversion Method (CM)", "", "", revision);
                            AddDecodedBitParam(data[startbyte], 0, 5, "Occurrence Count (OC)", "", "", revision);
                            startbyte++;
                        }
                    }
                    break;

                case JbusParam.Dtc138:
                    int typeNumber;
                    typeNumber = (int)AddDecodedParam(data, ref startbyte, 1, ParamType.Uint, endian, "DTC Block I Type", "", "", revision);
                    AddDecodedParam(data, ref startbyte, 1, ParamType.Uint, endian, "DTC Block I Length", "", "", revision);

                    switch (typeNumber)
                    {
                        case 0:
                            //AddDecodedParam(data, ref startbyte, 2, ParamType.Uint, endian, "Lamp Status", "", "", revision);
                            AddDecodedParam(data, ref startbyte, 2, ParamType.Uint, Endian.Big, "Lamp Status", "", "", revision);


                            if (data.Length - startbyte >= 3)
                            {
                                Entry = new MsgEntryType();
                                temp = ((((int)data[startbyte + 2] & 0xE0) << 11) | ((int)data[startbyte + 1] << 8) | (int)data[startbyte]);
                                Entry.Param = "Suspect Parameter Number (SPN)";
                                Entry.Hex = Utility.Dec2Hex((ulong)temp, 5, Add0x);
                                Entry.Value = temp.ToString();
                                Entry.Units = "";
                                Entry.Comment = "";
                                Entry.Revision = "-";
                                Entries.Add(Entry);

                                AddDecodedBitParam(data[startbyte + 2], 0, 4, "Failure Mode Indicator (FMI)", "", "", revision);
                                startbyte += 3;
                            }

                            if (data.Length - startbyte >= 1)
                            {
                                AddDecodedBitParam(data[startbyte], 7, 7, "Conversion Method (CM)", "", "", revision);
                                AddDecodedBitParam(data[startbyte], 0, 6, "Occurrence Count (OC)", "", "", revision);


                                startbyte++;
                            }

                            break;

                        case 16:
                            AddDecodedParam(data, ref startbyte, 2, ParamType.Uint, Endian.Big, "Lamp Status", "", "", revision);
                            //AddDecodedParam(data, ref startbyte, 1, ParamType.Uint, endian, "Source Address", "", "", revision);
                            //NumDTCs = 2;
                            

                            while (data.Length - startbyte >= 3)
                            {
                                
                                
                                    AddDecodedParam(data, ref startbyte, 1, ParamType.Uint, endian, "Source Address", "", "", revision);
                                    Entry = new MsgEntryType();
                                    temp = ((((int)data[startbyte + 2] & 0xE0) << 11) | ((int)data[startbyte + 1] << 8) | (int)data[startbyte]);
                                    Entry.Param = "Suspect Parameter Number (SPN)";
                                    Entry.Hex = Utility.Dec2Hex((ulong)temp, 5, Add0x);
                                    Entry.Value = temp.ToString();
                                    Entry.Units = "";
                                    Entry.Comment = "";
                                    Entry.Revision = "-";
                                    Entries.Add(Entry);

                                    AddDecodedBitParam(data[startbyte + 2], 0, 4, "Failure Mode Indicator (FMI)", "", "", revision);
                                    //AddDecodedParam(data, ref startbyte, 1, ParamType.Uint, endian, "Source Address", "", "", revision);
                                    startbyte += 3;
                                    if (data.Length - startbyte >= 1)
                                    {
                                        AddDecodedBitParam(data[startbyte], 7, 7, "Conversion Method (CM)", "", "", revision);
                                        //startbyte++;
                                        AddDecodedBitParam(data[startbyte], 0, 6, "Occurrence Count (OC)", "", "", revision);
                                    }
                                                                    

                                //if (data.Length - startbyte >= 1)
                                //{
                                //    AddDecodedBitParam(data[startbyte], 7, 7, "Conversion Method (CM)", "", "", revision);
                                //    AddDecodedBitParam(data[startbyte], 0, 5, "Occurrence Count (OC)", "", "", revision);
                                //    startbyte++;
                                //    if (data.Length - startbyte > 1)
                                //    {
                                //        AddDecodedParam(data, ref startbyte, 1, ParamType.Uint, endian, "Source Address", "", "", revision);
                                //    }
                                //}


                            }

                            break;
                    }
                    break;

                case JbusParam.Bobcat_CAN21_HydraulicChargePressure:
                    retVal = AddScaledParam(data, ref startbyte, 2, 0.5, 0, false, endian, paramTitle, "kPa", "Bobcat CAN21 Hydraulic Charge Pressure", revision);
                    break;
                case JbusParam.Bobcat_CAN21_HydraulicOilTemperature:
                    retVal = AddScaledParam(data, ref startbyte, 1, 1.0, -40, false, endian, paramTitle, "°C", "Bobcat CAN21 Hydraulic Oil Temperature", revision);
                    break;

                case JbusParam.Bobcat_CAN21_Devicebatteryvoltage:
                    retVal = AddScaledParam(data, ref startbyte, 4, 0.001, 0, false, endian, paramTitle, "V", "Voltage", revision);
                    break;

                case JbusParam.Bobcat_CAN21_Enginehours:
                    retVal = AddScaledParam(data, ref startbyte, 4, 0.05, 0, false, endian, paramTitle, "hours", "Engine Hours", revision);
                    break;

                case JbusParam.Bobcat_CAN21_Enginespeed:
                    retVal = AddScaledParam(data, ref startbyte, 4, 0.125, 0, false, endian, paramTitle, "RPM", "Engine Speed(RPM)", revision);
                    break;

                case JbusParam.Bobcat_CAN21_Fuellevel:
                    retVal = AddScaledParam(data, ref startbyte, 4, 0.4, 0, false, endian, paramTitle, "%", "Fuel Level", revision);
                    break;

                case JbusParam.Bobcat_CAN21_Deflevel:
                    retVal = AddScaledParam(data, ref startbyte, 4, 0.4, 0, false, endian, paramTitle, "%", "DEF Level", revision);
                    break;

                case JbusParam.Bobcat_CAN21_Serviceclockhours:
                    retVal = AddScaledParam(data, ref startbyte, 4, 1.0, -32127, false, endian, paramTitle, "hours", "Service Clock Hours", revision);
                    break;

                case JbusParam.Bobcat_CAN21_Cumulativefuelusage:
                    retVal = AddScaledParam(data, ref startbyte, 4, 0.5, 0, false, endian, paramTitle, "liters", "Cumulative Fuel Usage", revision);
                    break;

                case JbusParam.Bobcat_CAN21_Dtccount:
                    retVal = AddScaledParam(data, ref startbyte, 4, 1.0, 0, false, endian, paramTitle, " ", "DTC Count", revision);
                    break;
                case JbusParam.Bobcat_CAN21_Bobcatfaultcount:
                    retVal = AddScaledParam(data, ref startbyte, 4, 1.0, 0, false, endian, paramTitle, " ", "Bobcat Fault Count", revision);
                    break;

                default:
                    break;
            }

            return retVal;
        }

    }

    public enum JbusParam
    {
        Pid84,    //J1708 Road Speed
        Pid110,   //J1708 Engine Coolant Temperature
        Pid158,   //J1708 Switched Battery Voltage
        Pid168,   //J1708 Battery Voltage
        Pid175,   //J1708 Engine Oil Temperature
        Pid190,   //J1708 Engine Speed
        Pid235,   //J1708 Total Idle Hours
        Pid236,   //J1708 Total Idle Fuel
        Pid237,   //J1708 Vehicle Identification Number (VIN)
        Pid245,   //J1708 Odometer
        Pid247,   //J1708 Total Engine Hours
        Pid250,   //J1708 Total Fuel

        Spn38,    //J1939 Fuel Tank Level 2
        Spn84,    //J1939 Road Speed
        Spn91,    //J1939 Throttle Position <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        Spn96,    //J1939 Fuel Tank Level 1
        Spn98,    //J1939 Engine Oil Level
        Spn100,   //J1939 Engine Oil Pressure
        Spn101,   //J1939 Engine Crankcase Pressure
        Spn109,   //J1939 Engine Coolant Pressure
        Spn110,   //J1939 Engine Coolant Temperature
        Spn111,   //J1939 Engine Coolant Level
        Spn157,   //J1939 Fuel Rail Pressure <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        Spn158,   //J1939 Switched Battery Voltage
        Spn168,   //J1939 Battery Voltage
        Spn171,   //J1939 Ambient Air Temperature
        Spn172,   //J1939 Engine Air Intake Temperature <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        Spn174,   //J1939 Engine Fuel Temperature
        Spn175,   //J1939 Engine Oil Temperature
        Spn177,   //J1939 Transmission Oil Temperature
        Spn183,   //J1939 Engine Avg Fuel Rate
        Spn185,   //J1939 Avg Fuel Economy
        Spn190,   //J1939 Engine Speed
        Spn235,   //J1939 Total Idle Hours
        Spn236,   //J1939 Total Idle Fuel
        Spn237,   //J1939 Vehicle Identification Number (VIN)
        Spn245,   //J1939 Odometer
        Spn247,   //J1939 Total Engine Hours
        Spn250,   //J1939 Total Fuel
        Spn441,   //J1939 Axiliary Temperature 1
        Spn513,   //J1939 Actual Engine - Percent Torque
        Spn916,   //J1939 Service Clock Hours <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        Spn917,   //J1939 Hi-Rez Odometer
        Spn1761,  //J1939 DEF Level/NOx Tank Level
        Spn1856,  //J1939 Seatbelt
        Spn3515,  //J1939 DEF Temperature
        Spn3516,  //J1939 DEF Concentration

        DtcJ1708,
        DtcJ1939,
        Dtc138,
        Bobcat_CAN21_HydraulicChargePressure,
        Bobcat_CAN21_HydraulicOilTemperature,
        Bobcat_CAN21_Devicebatteryvoltage,
        Bobcat_CAN21_Enginehours,
        Bobcat_CAN21_Enginespeed,
        Bobcat_CAN21_Fuellevel,
        Bobcat_CAN21_Deflevel,
        Bobcat_CAN21_Serviceclockhours,
        Bobcat_CAN21_Cumulativefuelusage,
        Bobcat_CAN21_Dtccount,
        Bobcat_CAN21_Bobcatfaultcount,

    }
}
