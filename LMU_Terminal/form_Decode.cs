﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LMU_Terminal
{
    public enum EntryTypeType
    {
        NoConversion = 0,
        SharedNybble = 0
    }

    public struct EntryType
    {
        String KeyWord;
        String Units;
        String Comment;
        int Length;
        EntryTypeType Type;
    }

    public partial class form_Decode : Form
    {
        private byte[] Data;
        private UInt16 DataLen;
        private LmuDevice lmuDevice;

        private OtaMessage Message = new OtaMessage();

        public form_Decode()
        {
            InitializeComponent();
            this.lmuDevice = new LmuDevice();
            UpdateTitle();
            this.Show();
        }

        public form_Decode(string HexString)
        {
            InitializeComponent();
            this.textBox_Hex.Text = HexString;
            this.lmuDevice = new LmuDevice();
            UpdateTitle();
            this.Show();
            Decode();
        }

        public form_Decode(string HexString, LmuDevice lmu)
        {
            InitializeComponent();
            this.textBox_Hex.Text = HexString;
            this.lmuDevice = lmu;
            UpdateTitle();
            this.Show();
            Decode();
        }

        private void button_Clear_Click(object sender, EventArgs e)
        {
            this.textBox_Hex.Text = "";
            dataGridViewUdp.ColumnCount = 0;
            dataGridViewVbus.ColumnCount = 0;
        }

        private void form_Decode_Load(object sender, EventArgs e)
        {
            form_Decode_Resize(sender, e);
        }

        private void form_Decode_Resize(object sender, EventArgs e)
        {
            tab_Grid.Width = this.Width - 3 * tab_Grid.Left;
            tab_Grid.Height = this.Height - tab_Grid.Top - 4 * tab_Grid.Left;
        }

        private void Decode()
        {
            Message = new OtaMessage(this.textBox_Hex.Text, lmuDevice);
            Message.FillDataGrid(dataGridViewUdp, dataGridViewVbus);
        }

        private void button_Decode_Click(object sender, EventArgs e)
        {
            Decode();
        }

        private void UpdateTitle()
        {
            string Title = "Hex Message Decoder";
            string fileName = lmuDevice.GetFileName();

            if ((fileName == null) || (fileName == ""))
            {
                this.Text = Title;
            }
            else
            {
                this.Text = Title + " - " + fileName;
            }
        }

        private void timerUpdate_Tick(object sender, EventArgs e)
        {
            UpdateTitle();
        }
    }
}
