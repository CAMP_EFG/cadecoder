﻿namespace LMU_Terminal
{
    partial class FormTerminal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormTerminal));
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelDsr = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelCts = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelRi = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelBr = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBarTerminalLength = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripStatusLabelLoadCapture = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonConfigure = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonOpen = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonClose = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLoadCaptureFile = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonCopyGrid = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonClear = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLoadLmuDeviceFile = new System.Windows.Forms.ToolStripButton();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.dgvTerminal = new System.Windows.Forms.DataGridView();
            this.timerUpdate = new System.Windows.Forms.Timer(this.components);
            this.checkBoxAutoScroll = new System.Windows.Forms.CheckBox();
            this.textBoxTxData = new System.Windows.Forms.TextBox();
            this.buttonSendData = new System.Windows.Forms.Button();
            this.checkedListBoxDisplay = new System.Windows.Forms.CheckedListBox();
            this.labelInclude = new System.Windows.Forms.Label();
            this.buttonDecode = new System.Windows.Forms.Button();
            this.checkReadLiveLog = new System.Windows.Forms.CheckBox();
            this.statusStrip.SuspendLayout();
            this.toolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTerminal)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip
            // 
            this.statusStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip.Location = new System.Drawing.Point(0, 576);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.statusStrip.Size = new System.Drawing.Size(1283, 26);
            this.statusStrip.TabIndex = 0;
            this.statusStrip.Text = "statusStrip";
            // 
            // toolStripStatusLabelDsr
            // 
            this.toolStripStatusLabelDsr.Enabled = false;
            this.toolStripStatusLabelDsr.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripStatusLabelDsr.Name = "toolStripStatusLabelDsr";
            this.toolStripStatusLabelDsr.Size = new System.Drawing.Size(38, 21);
            this.toolStripStatusLabelDsr.Text = "DSR";
            // 
            // toolStripStatusLabelCts
            // 
            this.toolStripStatusLabelCts.Enabled = false;
            this.toolStripStatusLabelCts.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripStatusLabelCts.Name = "toolStripStatusLabelCts";
            this.toolStripStatusLabelCts.Size = new System.Drawing.Size(35, 21);
            this.toolStripStatusLabelCts.Text = "CTS";
            // 
            // toolStripStatusLabelRi
            // 
            this.toolStripStatusLabelRi.Enabled = false;
            this.toolStripStatusLabelRi.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripStatusLabelRi.Name = "toolStripStatusLabelRi";
            this.toolStripStatusLabelRi.Size = new System.Drawing.Size(24, 21);
            this.toolStripStatusLabelRi.Text = "RI";
            // 
            // toolStripStatusLabelBr
            // 
            this.toolStripStatusLabelBr.Enabled = false;
            this.toolStripStatusLabelBr.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripStatusLabelBr.Name = "toolStripStatusLabelBr";
            this.toolStripStatusLabelBr.Size = new System.Drawing.Size(39, 21);
            this.toolStripStatusLabelBr.Text = "BRK";
            // 
            // toolStripProgressBarTerminalLength
            // 
            this.toolStripProgressBarTerminalLength.Name = "toolStripProgressBarTerminalLength";
            this.toolStripProgressBarTerminalLength.Size = new System.Drawing.Size(133, 20);
            // 
            // toolStripStatusLabelLoadCapture
            // 
            this.toolStripStatusLabelLoadCapture.Name = "toolStripStatusLabelLoadCapture";
            this.toolStripStatusLabelLoadCapture.Size = new System.Drawing.Size(0, 21);
            // 
            // toolStrip
            // 
            this.toolStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonConfigure,
            this.toolStripSeparator1,
            this.toolStripButtonOpen,
            this.toolStripButtonClose,
            this.toolStripSeparator2,
            this.toolStripLoadCaptureFile,
            this.toolStripButtonCopyGrid,
            this.toolStripButtonClear,
            this.toolStripSeparator3,
            this.toolStripLoadLmuDeviceFile});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(1283, 27);
            this.toolStrip.TabIndex = 1;
            this.toolStrip.Text = "toolStrip";
            // 
            // toolStripButtonConfigure
            // 
            this.toolStripButtonConfigure.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonConfigure.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonConfigure.Image")));
            this.toolStripButtonConfigure.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonConfigure.Name = "toolStripButtonConfigure";
            this.toolStripButtonConfigure.Size = new System.Drawing.Size(24, 24);
            this.toolStripButtonConfigure.Text = "Configure Com Port";
            this.toolStripButtonConfigure.ToolTipText = "Configure Com Port";
            this.toolStripButtonConfigure.Visible = false;
            this.toolStripButtonConfigure.Click += new System.EventHandler(this.toolStripButtonConfigure_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 27);
            // 
            // toolStripButtonOpen
            // 
            this.toolStripButtonOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonOpen.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonOpen.Image")));
            this.toolStripButtonOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonOpen.Name = "toolStripButtonOpen";
            this.toolStripButtonOpen.Size = new System.Drawing.Size(24, 24);
            this.toolStripButtonOpen.Text = "Open Com Port";
            this.toolStripButtonOpen.Visible = false;
            this.toolStripButtonOpen.Click += new System.EventHandler(this.toolStripButtonOpen_Click);
            // 
            // toolStripButtonClose
            // 
            this.toolStripButtonClose.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonClose.Enabled = false;
            this.toolStripButtonClose.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonClose.Image")));
            this.toolStripButtonClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonClose.Name = "toolStripButtonClose";
            this.toolStripButtonClose.Size = new System.Drawing.Size(24, 24);
            this.toolStripButtonClose.Text = "Close Com Port";
            this.toolStripButtonClose.Visible = false;
            this.toolStripButtonClose.Click += new System.EventHandler(this.toolStripButtonClose_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 27);
            // 
            // toolStripLoadCaptureFile
            // 
            this.toolStripLoadCaptureFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripLoadCaptureFile.Image = ((System.Drawing.Image)(resources.GetObject("toolStripLoadCaptureFile.Image")));
            this.toolStripLoadCaptureFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripLoadCaptureFile.Name = "toolStripLoadCaptureFile";
            this.toolStripLoadCaptureFile.Size = new System.Drawing.Size(24, 24);
            this.toolStripLoadCaptureFile.Text = "Load Text Capture File";
            this.toolStripLoadCaptureFile.Click += new System.EventHandler(this.toolStripLoadCaptureFile_Click);
            // 
            // toolStripButtonCopyGrid
            // 
            this.toolStripButtonCopyGrid.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonCopyGrid.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonCopyGrid.Image")));
            this.toolStripButtonCopyGrid.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCopyGrid.Name = "toolStripButtonCopyGrid";
            this.toolStripButtonCopyGrid.Size = new System.Drawing.Size(24, 24);
            this.toolStripButtonCopyGrid.Text = "Copy Grid To Clipboard";
            this.toolStripButtonCopyGrid.Click += new System.EventHandler(this.toolStripButtonCopyGrid_Click);
            // 
            // toolStripButtonClear
            // 
            this.toolStripButtonClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonClear.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonClear.Image")));
            this.toolStripButtonClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonClear.Name = "toolStripButtonClear";
            this.toolStripButtonClear.Size = new System.Drawing.Size(24, 24);
            this.toolStripButtonClear.Text = "Clear Terminal";
            this.toolStripButtonClear.Click += new System.EventHandler(this.toolStripButtonClear_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 27);
            // 
            // toolStripLoadLmuDeviceFile
            // 
            this.toolStripLoadLmuDeviceFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripLoadLmuDeviceFile.Image = ((System.Drawing.Image)(resources.GetObject("toolStripLoadLmuDeviceFile.Image")));
            this.toolStripLoadLmuDeviceFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripLoadLmuDeviceFile.Name = "toolStripLoadLmuDeviceFile";
            this.toolStripLoadLmuDeviceFile.Size = new System.Drawing.Size(24, 24);
            this.toolStripLoadLmuDeviceFile.Text = "Load LMU Device File";
            this.toolStripLoadLmuDeviceFile.Click += new System.EventHandler(this.toolStripLoadLmuDeviceFile_Click);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "SetUp.bmp");
            // 
            // dgvTerminal
            // 
            this.dgvTerminal.AllowUserToAddRows = false;
            this.dgvTerminal.AllowUserToDeleteRows = false;
            this.dgvTerminal.AllowUserToResizeRows = false;
            this.dgvTerminal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTerminal.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvTerminal.Location = new System.Drawing.Point(178, 70);
            this.dgvTerminal.Margin = new System.Windows.Forms.Padding(4);
            this.dgvTerminal.Name = "dgvTerminal";
            this.dgvTerminal.RowHeadersVisible = false;
            this.dgvTerminal.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvTerminal.Size = new System.Drawing.Size(1087, 497);
            this.dgvTerminal.TabIndex = 2;
            this.dgvTerminal.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTerminal_CellContentDoubleClick);
            this.dgvTerminal.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dgvTerminal_MouseClick);
            // 
            // timerUpdate
            // 
            this.timerUpdate.Enabled = true;
            this.timerUpdate.Interval = 1;
            this.timerUpdate.Tick += new System.EventHandler(this.timerUpdate_Tick);
            // 
            // checkBoxAutoScroll
            // 
            this.checkBoxAutoScroll.AutoSize = true;
            this.checkBoxAutoScroll.Checked = true;
            this.checkBoxAutoScroll.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxAutoScroll.Location = new System.Drawing.Point(257, 9);
            this.checkBoxAutoScroll.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxAutoScroll.Name = "checkBoxAutoScroll";
            this.checkBoxAutoScroll.Size = new System.Drawing.Size(94, 21);
            this.checkBoxAutoScroll.TabIndex = 3;
            this.checkBoxAutoScroll.Text = "AutoScroll";
            this.checkBoxAutoScroll.UseVisualStyleBackColor = true;
            this.checkBoxAutoScroll.CheckedChanged += new System.EventHandler(this.checkBoxAutoScroll_CheckedChanged);
            this.checkBoxAutoScroll.CheckStateChanged += new System.EventHandler(this.checkBoxAutoScroll_CheckStateChanged);
            // 
            // textBoxTxData
            // 
            this.textBoxTxData.Location = new System.Drawing.Point(178, 38);
            this.textBoxTxData.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxTxData.Name = "textBoxTxData";
            this.textBoxTxData.Size = new System.Drawing.Size(1086, 22);
            this.textBoxTxData.TabIndex = 4;
            this.textBoxTxData.Visible = false;
            this.textBoxTxData.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxTxData_KeyPress);
            // 
            // buttonSendData
            // 
            this.buttonSendData.Location = new System.Drawing.Point(13, 38);
            this.buttonSendData.Margin = new System.Windows.Forms.Padding(4);
            this.buttonSendData.Name = "buttonSendData";
            this.buttonSendData.Size = new System.Drawing.Size(157, 25);
            this.buttonSendData.TabIndex = 5;
            this.buttonSendData.Text = "Send Data";
            this.buttonSendData.UseVisualStyleBackColor = true;
            this.buttonSendData.UseWaitCursor = true;
            this.buttonSendData.Visible = false;
            this.buttonSendData.Click += new System.EventHandler(this.buttonSendData_Click);
            // 
            // checkedListBoxDisplay
            // 
            this.checkedListBoxDisplay.BackColor = System.Drawing.SystemColors.Control;
            this.checkedListBoxDisplay.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.checkedListBoxDisplay.CheckOnClick = true;
            this.checkedListBoxDisplay.Items.AddRange(new object[] {
            "NMEA",
            "Cell",
            "Triggers",
            "Events",
            "HEX",
            "VBUS",
            "JPod2",
            "HA",
            "Everything Else"});
            this.checkedListBoxDisplay.Location = new System.Drawing.Point(19, 90);
            this.checkedListBoxDisplay.Margin = new System.Windows.Forms.Padding(4);
            this.checkedListBoxDisplay.Name = "checkedListBoxDisplay";
            this.checkedListBoxDisplay.Size = new System.Drawing.Size(138, 170);
            this.checkedListBoxDisplay.TabIndex = 6;
            this.checkedListBoxDisplay.SelectedIndexChanged += new System.EventHandler(this.checkedListBoxDisplay_SelectedIndexChanged);
            this.checkedListBoxDisplay.SelectedValueChanged += new System.EventHandler(this.checkedListBoxDisplay_SelectedValueChanged);
            // 
            // labelInclude
            // 
            this.labelInclude.AutoSize = true;
            this.labelInclude.Location = new System.Drawing.Point(19, 70);
            this.labelInclude.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelInclude.Name = "labelInclude";
            this.labelInclude.Size = new System.Drawing.Size(57, 17);
            this.labelInclude.TabIndex = 7;
            this.labelInclude.Text = "Include:";
            // 
            // buttonDecode
            // 
            this.buttonDecode.Location = new System.Drawing.Point(526, 5);
            this.buttonDecode.Margin = new System.Windows.Forms.Padding(4);
            this.buttonDecode.Name = "buttonDecode";
            this.buttonDecode.Size = new System.Drawing.Size(109, 27);
            this.buttonDecode.TabIndex = 8;
            this.buttonDecode.Text = "Decode";
            this.buttonDecode.UseVisualStyleBackColor = true;
            this.buttonDecode.Click += new System.EventHandler(this.buttonDecode_Click);
            // 
            // checkReadLiveLog
            // 
            this.checkReadLiveLog.AutoSize = true;
            this.checkReadLiveLog.Location = new System.Drawing.Point(384, 9);
            this.checkReadLiveLog.Margin = new System.Windows.Forms.Padding(4);
            this.checkReadLiveLog.Name = "checkReadLiveLog";
            this.checkReadLiveLog.Size = new System.Drawing.Size(122, 21);
            this.checkReadLiveLog.TabIndex = 9;
            this.checkReadLiveLog.Text = "Read Live Log";
            this.checkReadLiveLog.UseVisualStyleBackColor = true;
            this.checkReadLiveLog.CheckedChanged += new System.EventHandler(this.checkReadLiveLog_CheckedChanged);
            // 
            // FormTerminal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1283, 602);
            this.Controls.Add(this.checkReadLiveLog);
            this.Controls.Add(this.buttonDecode);
            this.Controls.Add(this.labelInclude);
            this.Controls.Add(this.checkedListBoxDisplay);
            this.Controls.Add(this.buttonSendData);
            this.Controls.Add(this.textBoxTxData);
            this.Controls.Add(this.checkBoxAutoScroll);
            this.Controls.Add(this.dgvTerminal);
            this.Controls.Add(this.toolStrip);
            this.Controls.Add(this.statusStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(661, 358);
            this.Name = "FormTerminal";
            this.Text = "LMU Terminal";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormTerminal_FormClosing);
            this.Load += new System.EventHandler(this.FormTerminal_Load);
            this.Resize += new System.EventHandler(this.FormTerminal_Resize);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTerminal)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton toolStripButtonConfigure;
        private System.Windows.Forms.ToolStripButton toolStripButtonOpen;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.ToolStripButton toolStripButtonClose;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.DataGridView dgvTerminal;
        private System.Windows.Forms.Timer timerUpdate;
        private System.Windows.Forms.CheckBox checkBoxAutoScroll;
        private System.Windows.Forms.TextBox textBoxTxData;
        private System.Windows.Forms.Button buttonSendData;
        private System.Windows.Forms.ToolStripButton toolStripButtonClear;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelDsr;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelCts;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelRi;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelBr;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBarTerminalLength;
        private System.Windows.Forms.CheckedListBox checkedListBoxDisplay;
        private System.Windows.Forms.Label labelInclude;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripLoadLmuDeviceFile;
        private System.Windows.Forms.ToolStripButton toolStripButtonCopyGrid;
        private System.Windows.Forms.ToolStripButton toolStripLoadCaptureFile;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelLoadCapture;
        private System.Windows.Forms.Button buttonDecode;
        private System.Windows.Forms.CheckBox checkReadLiveLog;
    }
}

