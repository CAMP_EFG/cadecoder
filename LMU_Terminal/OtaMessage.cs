﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LMU_Terminal
{
    class OtaMessage
    {
        private static readonly String[] Option1BitTypeStr = {"MobileID", 
                                                              "MobileIDType",
                                                              "AuthWord",
                                                              "Routing",
                                                              "Forwarding",
                                                              "RespRedirect",
                                                              "Unused",
                                                              "AlwaysSet"};

        private static readonly string[] MobileIdTypeStr = {"OFF", 
                                                            "ESN", 
                                                            "IMEI or EID", 
                                                            "IMSI of SIM",
                                                            "User Defined",
                                                            "Phone Number",
                                                            "IP of LMU"};

        private static readonly String[] ServiceTypeStr = {"Unacknowledged Request", 
                                                           "Acknowledged Request", 
                                                           "Response To An Acknowledged Request"};

        private static readonly String[] MessageTypeStr = {"Null Message", 
                                                           "Acknowledge Message",
                                                           "Event Report Message",
                                                           "ID Report Message",
                                                           "User Message",
                                                           "Application Message",
                                                           "Parameter Message",
                                                           "Unit Request Message",
                                                           "Locate Report Message",
                                                           "User Message With Accumulators",
                                                           "Mini Event Report Message",
                                                           "Mini User Message",
                                                           "Mini App Message",
                                                           "Device Version Report Message",
                                                           "App Message With Accumulators"};

        private static readonly String[] AckTypeStr = {"Success", 
                                                       "FailUnknown",
                                                       "BadMsgType",
                                                       "BadOperation",
                                                       "BadSerialPort",
                                                       "BadAuthentify",
                                                       "BadIdLookup",
                                                       "BadSequence"};

        private static readonly String[] GpsFixStatusTypeStr = {"Predicted", 
                                                                "DiffCorrected",
                                                                "LastKnown",
                                                                "InvalidFix",
                                                                "2dFix",
                                                                "Historic",
                                                                "InvalidTime",
                                                                "Unused"};

        private static readonly String[] CommStateTypeStr = {"Available", 
                                                             "NetworkService",
                                                             "DataService",
                                                             "ConnectedPPP",
                                                             "VoiceCallActive",
                                                             "Roaming",
                                                             "3gNetwork",
                                                             "Unused"};

        private static readonly String[] InputsTypeStr = {"Ignition", 
                                                          "Input1",
                                                          "Input2",
                                                          "Input3",
                                                          "Input4",
                                                          "Input5",
                                                          "Input6",
                                                          "Input7"};

        private static readonly String[] UnitStatusTypeStr = {"UpdateStatus", 
                                                              "GpsAntStatus",
                                                              "GpsRxSelfTest",
                                                              "GpsRxTrack",
                                                              "Reserved",
                                                              "Reserved",
                                                              "Reserved",
                                                              "Unused"};

        private static readonly String[] ModemTypeStr = {"(0) ?", "(1) ?", "(2) ?", 
                                                         "(3) iDEN LMU 19200, ATDT 0, PPP, Motorola iDEN Status – TC990569", 
                                                         "(4) ?",
                                                         "(5) GPRS LMU 57600,ATD*99***1#, WaveCom", 
                                                         "(6) ?", "(7) ?", "(8) ?", "(9) ?", "(10) ?",
                                                         "(11) CDMA LMU CDMA 1xRTT Modem – Kyocera M200",
                                                         "(12) ?", "(13) ?",
                                                         "(14) iDEN LMU 19200, ATDT 0, PPP, CMUX Status – TC990599",
                                                         "(15) GPRS LMU 115200, ATD*99***1#, PPP, Siemens",
                                                         "(16) ?",
                                                         "(17) CDMA LMU CDMA 1xRTT Modem – WaveCom Q2438",
                                                         "(18) GPRS LMU – ATD*99***1#, PPP, Siemens/Cinterion MC55i",
                                                         "(19) ?",
                                                         "(20) ConnectOne WiFi modem",
                                                         "(21) UBlox LEON G100 GSM/GPRS 2G",
                                                         "(22) Cinterion BGS3 GSM/GPRS 2G",
                                                         "(23) Motorola C24 CDMA modem",
                                                         "(24) Motorola H24 HSPA modem",
                                                         "(25) Delta Mobile DM300 WiFi modem",
                                                         "(26) ?", "(27) ?",
                                                         "(28) Quake 9612 Iridium Satellite modem",
                                                         "(29) UBlox LISA U200 HSPA 3G",
                                                         "(30) UBlox FW75 CDMA 2G",
                                                         "(31) UBlox LISA C200 CDMA 2G",
                                                         "(32) Cinterion BGS2 GSM/GPRS 2G",
                                                         "(33) Sierra LTE ( MC7700 )",
                                                         "(34) Sierra Quad-band HSPA",
                                                         "(35) UBlox SARA GSM/GPRS 2G",
                                                         "(36) Telit CE910-DUAL CDMA modem",
                                                         "(37) Telit UE910 HSPA Dualband modem",
                                                         "(38) Cinterion/Gemalto EHS5 HSPA modem",
                                                         "(39) Cinterion/Gemalto Industrial PLS8 LTE Modem",
                                                         "(40) Cinterion/Gemalto Automotive ALS3 LTE Modem",
                                                         "(41) Cinterion/Gemalto PXS8 HSPA/CDMA Modem",
                                                         "(42) ATEL VIA CDMA",
                                                         "(43) Cinterion/Gemalto PHS8 HSPA modem",
                                                         "(44) Remote Radio via Radio Access Interface (via BT, BLE, serial)",
                                                         "(45) ATEL Mediatek MT6221 chipset GPRS radio",
                                                         "(46) Telit LE910 LTE modem",
                                                         "(47) Telit HE910 HSPA Pentaband modem",
                                                         "(48) UBlox Toby L2xx LTE modem",
                                                         "(49) Ublox ODIN WiFi/Bluetooth",
                                                         "(50) Cinterion/Gemalto EHS6 HSPA modem"};

        public enum OptionsType
        {
            MobileId = 1,
            MobileIdType = 2,
            AuthWord = 4,
            Routing = 8,
            Forwarding = 16,
            RespRedirect = 32,
            OptExt = 64,
            AlwaysSet = 128
        }

        public enum OptionExtType
        {
            Esn = 1,
            Vin = 2,
            EncryptSvc = 4,
            LmdCompress = 8,
            LmdRouting = 16,
            NotUsed5 = 32,
            NotUsed6 = 64,
            NotUsed7 = 128
        }

        public enum ServiceType
        {
            UnacknowledgedRequest = 0,
            AcknowledgedRequest = 1,
            ResponseToAcknowledgedRequest = 2
        }

        public enum MessageType
        {
            NullMessage = 0,
            AckNakMessage = 1,
            EventReportMessage = 2,
            IdReportMessage = 3,
            UserDataMessage = 4,
            ApplicationDataMessage = 5,
            ConfigurationParameterMessage = 6,
            UnitRequestMessage = 7,
            LocateReportMessage = 8,
            UserDataWithAccumulatorsMessage = 9,
            MiniEventReportMessage = 10,
            MiniUserDataMessage = 11,
            MiniApplicationMessage = 12,
            DeviceVersionMessageMdt = 13,
            ApplicationMessageWithAccumulators = 14,
            PulsFirmwareDownload = 206,
            PulsFirmwareAcknowledgement = 210,
        }

        public enum ModemType
        {
            Motorola_iDEN_TC990569 = 3,
            GPRS_WaveCom = 5,
            CDMA_Kyocera_M200 = 11,
            CMUX_iDEN_TC990599 = 14,
            GPRS_Siemens = 15,
            CDMA_WaveCom_Q2438 = 17,
            GPSR_Siemens_Cinterion_MC55i = 18,
            ConnectOne_WiFi_modem = 20,
            UBlox_LEON_G100_GSM_GPRS_2G = 21,
            Cinterion_BGS3_GSM_GPRS_2G = 22,
            Motorola_C24_CDMA_modem = 23,
            Motorola_H24_HSPA_modem = 24,
            DeltaMobile_DM300_WiFi_modem = 25,
            Quake_9612_Iridium_Satellite_modem = 28,
            UBlox_LISA_U200_HSPA_3G = 29,
            UBlox_FW75_CDMA_2G = 30,
            UBlox_LISA_C200_CDMA_2G = 31,
            Cinterion_BGS2_GSM_GPRS_2G = 32,
            Sierra_LTE_MC7700 = 33,
            Sierra_Quad_band_HSPA = 34,
            UBlox_SARA_GSM_GPRS_2G = 35,
            Telit_CE910_DUAL_CDMA_modem = 36,
            Telit_UE910_HSPA_Dualband_modem = 37,
            Cinterion_Gemalto_EHS5_HSPA_modem = 38,
            Cinterion_Gemalto_Industrial_PLS8_LTE_Modem = 39,
            Cinterion_Gemalto_Automotive_ALS3_LTE_Modem = 40,
            Cinterion_Gemalto_PXS8_HSPA_CDMA_Modem = 41,
            ATEL_VIA_CDMA = 42,
            Cinterion_Gemalto_PHS8_HSPA_modem = 43,
            Remote_Radio_via_Radio_Access_Interface = 44,
            ATEL_Mediatek_MT6221_chipset_GPRS_radio = 45,
            Telit_LE910_LTE_modem = 46,
            Telit_HE910_HSPA_Pentaband_modem = 47,
            UBlox_Toby_L2xx_LTE_modem = 48,
            Ublox_ODIN_WiFi_Bluetooth = 49,
            Cinterion_Gemalto_EHS6_HSPA_modem = 50
        }

        private ParamList paramList;
        private AppMessage appMessage;
        public Boolean valid;
        private OptionsType Options;
        private OptionExtType OptionExt;
        private ServiceType Service;
        private MessageType Message;
        private string idString;
        private int SequenceNumber;
        int MsgType;
        int MsgLength;

        int BinLen;
        byte[] BinMsg;

        public override string ToString()
        {
            return idString;
        }

        public OtaMessage()
        {
            SequenceNumber = -1;
            valid = false;
        }

        public OtaMessage(byte[] Data, LmuDevice lmuDevice)
        {
            SequenceNumber = -1;
            idString = "Unknown";
            CreateMessage(Data, lmuDevice);
        }
        
        public OtaMessage(String CurText, LmuDevice lmuDevice)
        {
            byte[] FinalData;

            SequenceNumber = -1;
            Boolean Valid;
            byte[] data;
            int dataLen = 0;
            int Length = CurText.Length;
            int i = 0;
            int nybble = 0;
            int CurVal = 0;

            idString = "Unknown";

            if (Length > 0)
            {
                data = new byte[Length];

                while (i < Length)
                {
                    if (nybble == 0) CurVal = 0;

                    Valid = false;

                    if ((CurText[i] >= '0') && (CurText[i] <= '9'))
                    {
                        CurVal |= CurText[i] - '0';
                        Valid = true;
                    }
                    else if ((CurText[i] >= 'a') && (CurText[i] <= 'f'))
                    {
                        CurVal |= CurText[i] - 'a' + 10;
                        Valid = true;
                    }
                    else if ((CurText[i] >= 'A') && (CurText[i] <= 'F'))
                    {
                        CurVal |= CurText[i] - 'A' + 10;
                        Valid = true;
                    }

                    if (Valid)
                    {
                        if (nybble >= 1)
                        {
                            data[dataLen++] = (byte) CurVal;
                            CurVal = 0;
                            nybble = 0;
                        }
                        else
                        {
                            nybble++;
                            CurVal <<= 4;
                        }
                    }
                    else
                    {
                        CurVal = 0;
                        nybble = 0;
                    }

                    i++;
                }

                FinalData = new byte[dataLen];

                for (i = 0; i < dataLen; i++)
                    FinalData[i] = data[i];

                CreateMessage(FinalData, lmuDevice);
            }
            else
            {
                paramList = new ParamList("Parameter", "Value", "Units", "Hex", "Comment", "");
            }
        }

        private bool HasIpHeader(byte[] data, int startbyte)
        {
            bool retVal = false;
            int numBytes = 20;  //20 bytes in an IP Header
            int stopbyte = startbyte + numBytes - 1;
            int i;
            UInt32 total = 0;
            UInt32 curWord;
            UInt32 checkSum;

            if (data.Length > stopbyte)
            {
                if (data[startbyte] == 0x45)    //This is a 5 length (32 byte each) IPv4 header
                {
                    for (i = 0; i < numBytes; i += 2)
                    {
                        curWord = data[startbyte + i];
                        curWord <<= 8;
                        curWord += data[startbyte + i + 1];
                        total += curWord;
                    }
                    checkSum = total >> 16;
                    checkSum += (total & 0xFFFF);
                    total &= 0xFFFF;
                    retVal = (checkSum == 0xFFFF);
                    if (!retVal)
                    {
                        

                    }
                    //retVal = true;
                }
            }

            return retVal;
        }

        private bool HasOptionsHeader(byte[] data, int startbyte)
        {
            bool retVal = false;

            if (data.Length > startbyte)
            {
                retVal = ((data[startbyte] & 0x80) != 0);
            }

            return retVal;
        }

        private string GetSeqNumHdr()
        {
            string retVal = "";

            if (SequenceNumber >= 0)
            {
                retVal = "[" + SequenceNumber.ToString() + "]: ";
            }

            return retVal;
        }

        private void CreateMessage(byte[] Data, LmuDevice lmuDevice)
        {
            int pos = 0;
            int temp;
            valid = false;
            BinLen = Data.Length;
            int msgLen;
            string tmpString;

            if (BinLen > 0)
            {
                paramList = new ParamList("Parameter", "Value", "Units", "Hex", "Comment", "");
                idString = "Invalid";
                SequenceNumber = -1;

                BinMsg = new byte[BinLen];
                Data.CopyTo(BinMsg, 0);

                if (HasIpHeader(BinMsg, pos))
                {
                    //IP Header
                    paramList.AddDecodedBitParam(BinMsg[pos], 4, 7, "Version", "", "", "");
                    paramList.AddDecodedBitParam(BinMsg[pos], 0, 3, "Header Length", "", "", "");
                    pos++;

                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Hex, Endian.Big, "Type of Service (TOS)", "", "", "");
                    paramList.AddDecodedParam(BinMsg, ref pos, 2, ParamType.Uint, Endian.Big, "Total Length (in bytes)", "", "");
                    paramList.AddDecodedParam(BinMsg, ref pos, 2, ParamType.Hex, Endian.Big, "Identification", "", "");
                    paramList.AddDecodedParam(BinMsg, ref pos, 2, ParamType.Hex, Endian.Big, "Flags & Fragment Offset", "", "");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Big, "Time to Live (TTL)", "", "");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Big, "Protocol", "", "");
                    paramList.AddDecodedParam(BinMsg, ref pos, 2, ParamType.Uint, Endian.Big, "Header Checksum", "", "");
                    paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.IpAddress, Endian.Big, "Source IP", "", "");
                    paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.IpAddress, Endian.Big, "Destination IP", "", "");

                    //UDP Header
                    paramList.AddDecodedParam(BinMsg, ref pos, 2, ParamType.Uint, Endian.Big, "Source Port", "", "");
                    paramList.AddDecodedParam(BinMsg, ref pos, 2, ParamType.Uint, Endian.Big, "Destination Port", "", "");
                    paramList.AddDecodedParam(BinMsg, ref pos, 2, ParamType.Uint, Endian.Big, "UDP Packet Length", "", "");
                    paramList.AddDecodedParam(BinMsg, ref pos, 2, ParamType.Uint, Endian.Big, "UDP Checksum", "", "");
                }

                //LM Direct Header
                if (HasOptionsHeader(BinMsg, pos))
                {
                    //Options Header
                    Options = (OptionsType)paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Binary, Endian.Big, "Options Byte", "", new Comment(Option1BitTypeStr, true));
                    if ((Options & OptionsType.MobileId) > 0)
                    {
                        temp = (int)paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Big, "Mobile ID Length", "", "");
                        paramList.AddDecodedParam(BinMsg, ref pos, temp, ParamType.Hex, Endian.Big, "Mobile ID", "", "See Type Below");
                    }
                    if ((Options & OptionsType.MobileIdType) > 0)
                    {
                        temp = (int)paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Big, "Mobile ID Type Length", "", "");
                        temp = (int)paramList.AddDecodedParam(BinMsg, ref pos, temp, ParamType.Uint, Endian.Big, "Mobile ID Type", "", new Comment(MobileIdTypeStr, false));
                    }
                    if ((Options & OptionsType.AuthWord) > 0)
                    {
                        temp = (int)paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Big, "Authentification Length", "", "");
                        paramList.AddDecodedParam(BinMsg, ref pos, temp, ParamType.Hex, Endian.Big, "Authentification", "", "");
                    }
                    if ((Options & OptionsType.Routing) > 0)
                    {
                        temp = (int)paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Big, "Routing Length", "", "");
                        paramList.AddDecodedParam(BinMsg, ref pos, temp, ParamType.Hex, Endian.Big, "Routing", "", "");
                    }
                    if ((Options & OptionsType.Forwarding) > 0)
                    {
                        temp = (int)paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Big, "Forwarding Length", "", "");
                        paramList.AddDecodedParam(BinMsg, ref pos, temp, ParamType.Hex, Endian.Big, "Forwarding", "", "");
                    }
                    if ((Options & OptionsType.RespRedirect) > 0)
                    {
                 
                        temp = (int)paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Big, "Response Redirection Length", "", "");
                        paramList.AddDecodedParam(BinMsg, ref pos, temp, ParamType.Hex, Endian.Big, "Response Redirection", "", "");
                    }
                    if ((Options & OptionsType.OptExt) > 0)
                    {
                        temp = (int)paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Big, "Option Extension Length", "", "");
                        OptionExt = (OptionExtType)paramList.AddDecodedParam(BinMsg, ref pos, temp, ParamType.Binary, Endian.Big, "Option Extension", "", "");
                    }
                    //Option Extensions
                    if ((OptionExt & OptionExtType.Esn) > 0)
                    {
                        temp = (int)paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Big, "ESN Length", "", "");
                        paramList.AddDecodedParam(BinMsg, ref pos, temp, ParamType.Hex, Endian.Big, "ESN", "", "");
                    }
                    if ((OptionExt & OptionExtType.Vin) > 0)
                    {
                        temp = (int)paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Big, "VIN Length", "", "");
                        paramList.AddDecodedParam(BinMsg, ref pos, temp, ParamType.Alpha, Endian.Big, "VIN", "", "");
                    }
                    if ((OptionExt & OptionExtType.EncryptSvc) > 0)
                    {
                        temp = (int)paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Big, "Encryption Service Length", "", "");
                        paramList.AddDecodedParam(BinMsg, ref pos, temp, ParamType.Hex, Endian.Big, "Encryption Service", "", "");
                    }
                    if ((OptionExt & OptionExtType.LmdRouting) > 0)
                    {
                        temp = (int)paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Big, "LMDirect Routing Length", "", "");
                        paramList.AddDecodedParam(BinMsg, ref pos, temp, ParamType.Hex, Endian.Big, "LMDirect Routing", "", "");
                    }
                }

                //Message Header
                Service = (ServiceType)paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Big, "LM Direct Service Type", "", new Comment(ServiceTypeStr, false));
                Message = (MessageType)paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Big, "LM Direct Message Type", "", new Comment(MessageTypeStr, false));
                SequenceNumber = (int)paramList.AddDecodedParam(BinMsg, ref pos, 2, ParamType.Uint, Endian.Big, "Sequence Number", "", "");

                //The rest depends on Message Type
                if (Message == MessageType.NullMessage)                             // DONE Type 0 ==============================
                {
                    //Nothing to do here                    
                    idString = GetSeqNumHdr() + "Null Message";
                }
                else if (Message == MessageType.AckNakMessage)                      // DONE Type 1==============================
                {
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Big, "Type", "", "");
                    temp = (int)paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Binary, Endian.Big, "Ack", "", new Comment(AckTypeStr, true));
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Big, "Spare", "", "");
                    paramList.AddDecodedParam(BinMsg, ref pos, 3, ParamType.Alpha, Endian.Big, "App Version", "", "");
                    if (temp < 8)
                    {
                        idString = GetSeqNumHdr() + "Acknowledge Message: " + AckTypeStr[temp];
                    }
                    else
                    {
                        idString = GetSeqNumHdr() + "Acknowledge Message: ";
                    }
                }
                else if (Message == MessageType.EventReportMessage)                 // DONE Type 2 ==============================
                {
                    if (BinMsg.Length > 42)
                    {
                        idString = GetSeqNumHdr() + "Event Report Message";
                        //LoadMessageHeader(BinMsg, ref pos, Message, lmuDevice);

                        GetMessageHeader(BinMsg, ref pos, lmuDevice);
                        //Get the rest of the Event Report Message
                        paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Big, "Event Index", "", "255 = Real Time PEG Action Request");
                        tmpString = GetEventName(BinMsg[pos], lmuDevice);
                        idString = GetSeqNumHdr() + "Event Report Message: (" + BinMsg[pos].ToString() + ") " + tmpString;
                        paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Big, "Event Code", "", tmpString);
                        //Get the accumulators
                        GetAccumulators(BinMsg, ref pos, lmuDevice);
                    }
                }
                else if (Message == MessageType.IdReportMessage)                    // DONEish Type 3 ==============================
                {
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Big, "Script Version", "", "");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Big, "Config Version", "", "");
                    paramList.AddDecodedParam(BinMsg, ref pos, 3, ParamType.Alpha, Endian.Big, "App Version", "", "");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Big, "Vehicle Class", "", "");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Binary, Endian.Big, "Unit Status", "", new Comment(UnitStatusTypeStr, true));
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Big, "Modem Selection", "", new Comment(ModemTypeStr));
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Big, "Application ID", "", "");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Big, "Mobile ID Type", "", new Comment(MobileIdTypeStr, false));
                    paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "Query ID", "", "");
                    paramList.AddDecodedParam(BinMsg, ref pos, 8, ParamType.PackedBcd, Endian.Big, "ESN", "", "");
                    paramList.AddDecodedParam(BinMsg, ref pos, 8, ParamType.PackedBcd, Endian.Big, "IMEI", "", "");
                    paramList.AddDecodedParam(BinMsg, ref pos, 8, ParamType.PackedBcd, Endian.Big, "IMSI", "", "");
                    paramList.AddDecodedParam(BinMsg, ref pos, 8, ParamType.PackedBcd, Endian.Big, "MIN", "", "");
                    paramList.AddDecodedParam(BinMsg, ref pos, 10, ParamType.PackedBcd, Endian.Big, "ICC-ID", "", "");
                    do
                    {
                        temp = (int)paramList.AddNullTerminatedString(BinMsg, ref pos, "", "");                        
                    }
                    while(temp != 0);

                    idString = GetSeqNumHdr() + "ID Report Message";
                }
                else if (Message == MessageType.UserDataMessage)                    // DONE Type 4 ==============================
                {
                    idString = GetSeqNumHdr() + "User Data Message";

                    GetMessageHeader(BinMsg, ref pos, lmuDevice);
                    //Get the rest of the User Data Message
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Big, "User Msg Route", "", "");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Big, "User Msg ID", "", "");
                    msgLen = (int)paramList.AddDecodedParam(BinMsg, ref pos, 2, ParamType.Uint, Endian.Big, "User Msg Length", "", "");
                    paramList.AddDecodedParam(BinMsg, ref pos, msgLen, ParamType.Hex, Endian.Big, "User Msg", "", "");
                }
                else if (Message == MessageType.ApplicationDataMessage)             // DONE Type 5 ==============================
                {
                    GetMessageHeader(BinMsg, ref pos, lmuDevice);

                    //paramList.AddDecodedParam(BinMsg, ref pos, 2, ParamType.Hex, Endian.Big, "Spare", "", "");
                    //Get the accumulators
                    //GetAccumulators(BinMsg, ref pos, lmuDevice);

                    MsgType = (int)paramList.AddDecodedParam(BinMsg, ref pos, 2, ParamType.Uint, Endian.Big, "App Msg Type", "", "");
                    MsgLength = (int)paramList.AddDecodedParam(BinMsg, ref pos, 2, ParamType.Uint, Endian.Big, "App Msg Length", "", "");

                    appMessage = new AppMessage(BinMsg, pos, MsgType, MsgLength);
                    idString = GetSeqNumHdr() + "App Data Msg: " + appMessage.ToString();
                    
                }


                else if (Message == MessageType.ConfigurationParameterMessage)
                {
                    idString = GetSeqNumHdr() + "Configuration Parameter Message";
                }
                else if (Message == MessageType.UnitRequestMessage)
                {
                    idString = GetSeqNumHdr() + "Unit Request Message";
                }
                else if (Message == MessageType.LocateReportMessage)
                {
                    idString = GetSeqNumHdr() + "Locate Report Message";                    
                    GetMessageHeader(BinMsg, ref pos, lmuDevice);
                }
                else if (Message == MessageType.UserDataWithAccumulatorsMessage)
                {
                    idString = GetSeqNumHdr() + "User Data With Accumulators Message";                    

                    GetMessageHeader(BinMsg, ref pos, lmuDevice);
                    //Get the accumulators
                    GetAccumulators(BinMsg, ref pos, lmuDevice);
                }
                else if (Message == MessageType.MiniEventReportMessage)
                {
                    idString = GetSeqNumHdr() + "Mini Event Report Message";
                    GetMiniMessageHeader(BinMsg, ref pos, lmuDevice);

                    tmpString = GetEventName(BinMsg[pos], lmuDevice);
                    idString = GetSeqNumHdr() + "Event Report Message: (" + BinMsg[pos].ToString() + ") " + tmpString;
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Big, "Event Code", "", tmpString);

                    GetMiniAccumulators(BinMsg, ref pos, lmuDevice);
                }
                else if (Message == MessageType.MiniUserDataMessage)
                {
                    idString = GetSeqNumHdr() + "Mini User Data Message";
                }
                else if (Message == MessageType.MiniApplicationMessage)
                {
                    idString = GetSeqNumHdr() + "Mini Application Message";
                }
                else if (Message == MessageType.DeviceVersionMessageMdt)
                {
                    idString = GetSeqNumHdr() + "Device Version Message (MDT)";
                }
                else if (Message == MessageType.ApplicationMessageWithAccumulators) // DONE Type 14 =============================
                {
                    GetMessageHeader(BinMsg, ref pos, lmuDevice);
                    paramList.AddDecodedParam(BinMsg, ref pos, 2, ParamType.Hex, Endian.Big, "Spare", "", "");
                    //Get the accumulators
                    GetAccumulators(BinMsg, ref pos, lmuDevice);

                    //paramList.SetString("Application Message With Accumulators");

                    MsgType = (int)paramList.AddDecodedParam(BinMsg, ref pos, 2, ParamType.Uint, Endian.Big, "App Msg Type", "", "");
                    MsgLength = (int)paramList.AddDecodedParam(BinMsg, ref pos, 2, ParamType.Uint, Endian.Big, "App Msg Length", "", "");

                    appMessage = new AppMessage(BinMsg, pos, 0x10000 + MsgType, MsgLength);
                    idString = GetSeqNumHdr() + "App Data Msg w/Acc: " + appMessage.ToString();
                }
                else if (Message == MessageType.PulsFirmwareDownload) // IN PROGRESS Type 206 =============================
                {
                    GetMessageHeader(BinMsg, ref pos, lmuDevice);
                    //paramList.SetString("Application Message With Accumulators");

                    idString = GetSeqNumHdr() + "PULS Firmware Download";
                }
                else if (Message == MessageType.PulsFirmwareAcknowledgement) // IN PROGRESS Type 210 =============================
                {
                    GetMessageHeader(BinMsg, ref pos, lmuDevice);
                    //paramList.SetString("Application Message With Accumulators");

                    idString = GetSeqNumHdr() + "PULS Firmware Acknowledgement";
                }
                else
                {
                    idString = GetSeqNumHdr() + "Invalid";
                }
            }//if (BinLen > 0)
        }

        private void GetMessageHeader(byte[] BinMsg, ref int pos, LmuDevice lmuDevice)
        {
            paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.UnixTime, Endian.Big, "Update Time", "", "");
            paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.UnixTime, Endian.Big, "GPS Time of Fix", "", "");
            paramList.AddScaledParam(BinMsg, ref pos, 4, 0.0000001, 0, true, Endian.Big, "GPS Latitude", "°", "");
            paramList.AddScaledParam(BinMsg, ref pos, 4, 0.0000001, 0, true, Endian.Big, "GPS Longitude", "°", "");
            paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Sint, Endian.Big, "GPS Altitude", "cm", "");
            paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "GPS Speed", "cm/s", "");
            paramList.AddDecodedParam(BinMsg, ref pos, 2, ParamType.Uint, Endian.Big, "GPS Heading", "°", "");
            paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Big, "GPS Satellites", "", "");
            paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Binary, Endian.Big, "GPS Fix Status", "", new Comment(GpsFixStatusTypeStr, true));
            paramList.AddDecodedParam(BinMsg, ref pos, 2, ParamType.Uint, Endian.Big, "Carrier", "", "");
            paramList.AddDecodedParam(BinMsg, ref pos, 2, ParamType.Sint, Endian.Big, "RSSI", "dBm", "");
            paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Binary, Endian.Big, "Comm State", "", new Comment(CommStateTypeStr, true));
            paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Big, "HDOP", "", "");
            paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Binary, Endian.Big, "Inputs", "", new Comment(InputsTypeStr, true));
            paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Binary, Endian.Big, "Unit Status", "", new Comment(UnitStatusTypeStr, true));
        }

        private void GetMiniMessageHeader(byte[] BinMsg, ref int pos, LmuDevice lmuDevice)
        {
            paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.UnixTime, Endian.Big, "Update Time", "", "");
            //paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.UnixTime, Endian.Big, "GPS Time of Fix", "", "");
            paramList.AddScaledParam(BinMsg, ref pos, 4, 0.0000001, 0, true, Endian.Big, "GPS Latitude", "°", "");
            paramList.AddScaledParam(BinMsg, ref pos, 4, 0.0000001, 0, true, Endian.Big, "GPS Longitude", "°", "");
            paramList.AddDecodedParam(BinMsg, ref pos, 2, ParamType.Uint, Endian.Big, "GPS Heading", "°", "");
            paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Big, "GPS Speed", "cm/s", "");
            paramList.AddDecodedBitParam(BinMsg[pos], 0, 3, "Satellites", "", "", "");
            paramList.AddDecodedBitParam(BinMsg[pos], 4, 7, "GPS Fix Status", "", new Comment(GpsFixStatusTypeStr, true), "");
            pos++;
            paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Binary, Endian.Big, "Comm State", "", new Comment(CommStateTypeStr, true));
            paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Binary, Endian.Big, "Inputs", "", new Comment(InputsTypeStr, true));
        }

        private void GetAccumulators(byte[] BinMsg, ref int pos, LmuDevice lmuDevice)
        {
            int Accumulators;
            int AccumFormat;
            int AccumList = 0;
            int CurAcc = 0;

            Accumulators = (int)paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Big, "Accums", "", "");
            AccumFormat = (Accumulators >> 6);
            Accumulators &= 0x3F;
            paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Big, "Spare", "", "");
            switch (AccumFormat)
            {
                case 0: //Standard
                    break;

                case 1: //Enhanced
                    Accumulators -= 2;
                    paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "Thresholds", "", "");
                    AccumList = (int)paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "ReportedAcc", "", "");
                    break;
                default:
                    break;
            }

            if (AccumList != 0)
            {
                while (AccumList != 0)
                {
                    if ((AccumList & 1) == 1)
                    {
                        AddAccumulator(CurAcc, ref pos, lmuDevice);
                    }
                    AccumList >>= 1;
                    CurAcc++;
                }
            }
            else
            {
                for (CurAcc = 0; CurAcc < Accumulators; CurAcc++)
                {
                    AddAccumulator(CurAcc, ref pos, lmuDevice);
                }
            }
        }

        private void GetMiniAccumulators(byte[] BinMsg, ref int pos, LmuDevice lmuDevice)
        {
            int Accumulators;
            int AccumFormat;
            int AccumList = 0;
            int CurAcc = 0;

            Accumulators = (int)paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Big, "Accums", "", "");
            AccumFormat = (Accumulators >> 6);
            Accumulators &= 0x3F;
            switch (AccumFormat)
            {
                case 0: //Standard
                    break;

                case 1: //Enhanced
                    Accumulators -= 2;
                    paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "Thresholds", "", "");
                    AccumList = (int)paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "ReportedAcc", "", "");
                    break;
                default:
                    break;
            }

            if (AccumList != 0)
            {
                while (AccumList != 0)
                {
                    if ((AccumList & 1) == 1)
                    {
                        AddAccumulator(CurAcc, ref pos, lmuDevice);
                    }
                    AccumList >>= 1;
                    CurAcc++;
                }
            }
            else
            {
                for (CurAcc = 0; CurAcc < Accumulators; CurAcc++)
                {
                    AddAccumulator(CurAcc, ref pos, lmuDevice);
                }
            }
        }

        private void AddAccumulator(int Acc, ref int pos, LmuDevice lmuDevice)
        {
            LmuDeviceAccumulator Accumulator = lmuDevice.GetAccumulatorInfo(Acc);

            if (Accumulator.number == Acc)
            {
                switch (Accumulator.type)
                {
                    case AccumType.Alpha:
                        paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Alpha, Endian.Big, "Acc" + Acc.ToString(), Accumulator.units, Accumulator.desc);
                        break;
                    case AccumType.Binary:
                        paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Binary, Endian.Big, "Acc" + Acc.ToString(), Accumulator.units, Accumulator.desc);
                        break;
                    case AccumType.Hex:
                        paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Hex, Endian.Big, "Acc" + Acc.ToString(), Accumulator.units, Accumulator.desc);
                        break;
                    case AccumType.Sint:
                        paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Sint, Endian.Big, "Acc" + Acc.ToString(), Accumulator.units, Accumulator.desc);
                        break;
                    case AccumType.Uint:
                        paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "Acc" + Acc.ToString(), Accumulator.units, Accumulator.desc);
                        break;
                    case AccumType.Scaled:
                        paramList.AddScaledParam(BinMsg, ref pos, 4, Accumulator.scale, Accumulator.adder, false, Endian.Big, "Acc" + Acc.ToString(), Accumulator.units, Accumulator.desc);
                        break;
                }
            }
            else
            {
                paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "Acc" + Acc.ToString(), "", "");
            }
        }

        private string GetEventName(int EventCode, LmuDevice lmuDevice)
        {
            LmuDeviceEvent lmuEvent = lmuDevice.GetEventInfo(EventCode);

            return lmuEvent.desc;
        }


        public void FillDataGrid(DataGridView dgv, DataGridView dgvExtended)
        {
            paramList.FillDataGrid(dgv);

            dgvExtended.RowCount = 0;
            dgvExtended.ColumnCount = 0;

            if ((Message == MessageType.ApplicationDataMessage) ||
                (Message == MessageType.ApplicationMessageWithAccumulators))
            {
                appMessage.FillDataGrid(dgvExtended);
            }
        }

    }
}
