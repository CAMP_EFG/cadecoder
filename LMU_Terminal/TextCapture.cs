﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace LMU_Terminal
{
    class TextCapture
    {
        private String fileName;
        private int maxLines;
        private bool valid;
        private bool fileOpen;
        private List<String> listCapture;
        public string liveLogFilename = "";
        public string liveLogOrigFilename = "";
        

        public TextCapture()
        {
            maxLines = 0;
            fileOpen = false;
            valid = false;
            listCapture = new List<String>();
        }

        public int TextLineCount
        {
            get
            {
                int retVal = -1;

                if ((listCapture != null) && (listCapture.Count >= 0))
                    retVal = listCapture.Count;

                return retVal;
            }
        }

        public int TextLineMaxCount
        {
            get
            {
                return maxLines;
            }
        }

        public bool FileOpen
        {
            get
            {
                return fileOpen;
            }
        }

        public String ReadTextLine(int lineNum)
        {
            String retVal = null;
            String test = "";
            if ((listCapture != null) && (listCapture.Count > lineNum))
            {
                if (listCapture[lineNum] != null)
                {
                    retVal = listCapture[lineNum].Substring(0);
                }
                listCapture.RemoveAt(lineNum);
            }

            return retVal;
        }

        public Boolean IsValid()
        {
            return valid;
        }

        public String GetFileName()
        {
            return fileName;
        }

        public void LoadCaptureFile(String Name)
        {
            LmuDeviceSearchString ldss;
            String			str;
	        int		        pos;	

            valid = false;
            fileName = Name;
            fileOpen = true;
            maxLines = 0;

	        try 
	        {
                listCapture = new List<String>();

		        //Console::WriteLine("trying to open file {0}...", fileName);
		        StreamReader fileIn = File.OpenText(fileName);

		        while ((str = fileIn.ReadLine()) != null) //read one line at a time
		        {
                    if (str != null)
                    {
                        this.listCapture.Add(str.Substring(0));
                        maxLines++;
                        valid = true;
                    }
		        }
                fileIn.Close();
	        }
	        catch (Exception e)
	        {
                Console.WriteLine("LoadCaptureFile " + e.Message, fileName);
	        }

            fileOpen = false;
        }

        public void OpenCaptureFile()
        {
            //String defaultPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            //String defaultPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase);
            String defaultPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            //String defaultPath = AppDomain.CurrentDomain.BaseDirectory;

            this.OpenCaptureFile(defaultPath);
        }

        public Boolean OpenCaptureFile(String DefaultPath)
        {
            Boolean success = false;
            
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
  
	        openFileDialog1.InitialDirectory = DefaultPath;
	        openFileDialog1.Filter = "All files (*.*)|*.*";
	        openFileDialog1.FilterIndex = 1;
	        openFileDialog1.RestoreDirectory = true;
  
	        if (openFileDialog1.ShowDialog() == DialogResult.OK)
	        {
                LoadCaptureFile(openFileDialog1.FileName);
                liveLogFilename = AppDomain.CurrentDomain.BaseDirectory + "log.txt";
                File.Copy(openFileDialog1.FileName, liveLogFilename,true);
                liveLogOrigFilename = openFileDialog1.FileName;
                success = true;
            }
            return success;
        }

        public void ReOpenCaptureFile()
        {

            if (!string.IsNullOrEmpty(liveLogFilename))
            {
                File.Copy(liveLogOrigFilename, liveLogFilename, true);
                LoadCaptureFile(liveLogFilename);
                
            }
        }

        public void WriteAllParamCsvFile(String fileName)//, FilterArray ListFilter)
        {
            /*
	        Int32							LastPGN = -1;
	        Int32							LastSPN = -1;
//	        Int32							LastPID = -1;
//	        Int32							Index;
	        Int32						    Count = 0;
	        bool							FilterFound;
	        bool							Valid;
	        bool							HadSPN = false;
//	        String							List;

	        //ParamString						Local;
	        //Param1708						Local1708;
	        //Param1939						Local1939;

            try
            {
                if (File.Exists(fileName)) File.Delete(fileName);

                StreamWriter fileOut = new StreamWriter(fileName);

		        fileOut.WriteLine("==== ALL PARAMETERS ====");
		        fileOut.WriteLine("");

		        if (ListMID.Count() != 0)	
		        {
			        fileOut.WriteLine("==== J1708/J1587 Message Identification ====");
			        fileOut.WriteLine(",,MID:,MID Description:");
                    foreach (ParamString MID in ListMID)
			        {
				        fileOut.Write(",,");
				        fileOut.Write(MID.param.ToString());
				        fileOut.Write(",");
				        fileOut.WriteLine(MID.desc);
			        }
			        fileOut.WriteLine("");
		        }

		        if (List1708.Count() != 0)	
		        {
			        fileOut.WriteLine("==== J1708/J1587 Parameter Identification ====");
			        fileOut.WriteLine("PID:,,MID Reporting:,PID Description:");
			
                    foreach (ParamString PID in ListPID)
			        {
				        fileOut.Write(PID.param.ToString());
				        fileOut.Write(",");

				        FilterFound = false;
                        foreach (Filter Filt in ListFilter.List)
					        if ((Filt.type == Filter.FilterLineType.ofltPID) && (Filt.PID == PID.param))
					        {
						        FilterFound = true;
						        break;
					        }

				        if (FilterFound) fileOut.Write("*");

				        fileOut.Write(",");
				        Count = 0;
                        foreach (Param1708 P1708 in List1708)
					        if (PID.param == P1708.PID)
					        {
						        Count++;
					        }
				        fileOut.Write(Count.ToString());
				        fileOut.Write(",PID: ");
				        fileOut.WriteLine(PID.desc);

			        }
			        fileOut.WriteLine("");
		        }


		        if (ListSA.Count() != 0)
		        {
			        fileOut.WriteLine("==== J1939 Source Addresses ====");
			        fileOut.WriteLine(",,SA:,SA Description:");
                    foreach (ParamString SA in ListSA)
			        {
				        fileOut.Write(",,");
				        fileOut.Write(SA.param.ToString());
				        fileOut.Write(",");
				        fileOut.WriteLine(SA.desc);	
			        }
			        fileOut.WriteLine("");
		        }

		        if (List1939.Count() != 0)
		        {
			        fileOut.WriteLine("==== J1939 Data ====");
			        fileOut.WriteLine("PGN/SPN:,,SA Reporting:,PGN or SPN Description:");
			        Valid = false;
			        //List = "";
			        Count = 0;
                    foreach (Param1939 P1939 in List1939)
			        {
				        if ((P1939.PGN == 61444) && (P1939.SPN == 190))
				        {
					        //Valid = Valid;
				        }

				        if (((!HadSPN) && (P1939.PGN != LastPGN)) || ((HadSPN) && (P1939.SPN != LastSPN)))
				        {
					        if (Valid)
					        {
						        FilterFound = false;
                                foreach (Filter Filt in ListFilter.List)
						        {
							        if (Filt.PGN == LastPGN)
							        {
								        if (HadSPN) 
								        {
									        if ((Filt.type == Filter.FilterLineType.ofltSPN) && (Filt.SPN == LastSPN)) FilterFound = true;
								        }
								        else
								        {
									        if (Filt.type == Filter.FilterLineType.ofltPGN) FilterFound = true;
								        }
							        }

							        if (FilterFound) break;
						        }

						        fileOut.Write(LastPGN.ToString());
						
						        if (HadSPN)
						        {
							        fileOut.Write("/");
							        fileOut.Write(LastSPN.ToString());
						        }
						        fileOut.Write(",");	

						        if (FilterFound) fileOut.Write("*");
						
						        fileOut.Write(",");
						        fileOut.Write(Count.ToString());
						        fileOut.Write(",");
						        if (HadSPN)
						        {
                                    foreach (ParamString SPN in ListSPN)
                                        if (SPN.param == LastSPN)
                                        {
                                            fileOut.Write("SPN: ");
                                            fileOut.WriteLine(SPN.desc);
                                            break;
                                        }
						        }
						        else
						        {
                                    foreach (ParamString PGN in ListPGN)
                                        if (PGN.param == LastPGN)
                                        {
							                fileOut.Write("PGN: ");
							                fileOut.WriteLine(PGN.desc);
                                            break;
                                        }
						        }
					        }
					        HadSPN = P1939.HasSPN;	
					        LastPGN = (Int32) P1939.PGN;
					        if (P1939.HasSPN)
					        {
						        LastSPN = (Int32) P1939.SPN;
					        }
					        else
					        {
						        LastSPN = -1; 
					        }
					
					        Valid = false;
					        Count = 0;
					        //List = "";
				        }



				        if (P1939.HasValidData)
				        {
					        Count++;
					        Valid = true;
				        }
			        }
			        fileOut.WriteLine("");
		        }

                fileOut.Close();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message, fileName);
            }
            */
        }

        public void SaveLmuDeviceFile()//FilterArray ListFilter)
        {
            /*
	        String path = "";//"C:\\Users\\Mike\\Documents\\Visual Studio 2010\\Projects\\Smith Chart\\Data";

	        SaveFileDialog saveFileDialog1 = new SaveFileDialog();
  
	        saveFileDialog1.InitialDirectory = path;
	        saveFileDialog1.Filter = "csv files (*.csv)|*.csv|txt files (*.txt)|*.txt|All files (*.*)|*.*";
	        saveFileDialog1.FilterIndex = 1;
	        saveFileDialog1.RestoreDirectory = true;
  
	        if (saveFileDialog1.ShowDialog() == DialogResult.OK )
	        {
		        WriteAllParamCsvFile(saveFileDialog1.FileName, ListFilter);
		        //WriteParamCsvFile(saveFileDialog1.FileName);
	        }
             * */
        }

        public void WriteFilteredParamCsvFile(String fileName)//, FilterArray ListFilter)
        {
            /*
//	        Int32						    Index;
	        bool							HasSPN = false;
//	        bool							FoundSPN = false;
	        bool							Found = false;
            String						    Desc = "";

            try
            {
                if (File.Exists(fileName)) File.Delete(fileName);

                StreamWriter fileOut = new StreamWriter(fileName);

		        fileOut.WriteLine("==== FILTERED ====");

		        if (ListMID.Count != 0)	
		        {
			        fileOut.WriteLine("==== J1708/J1587 Message Identification ====");
			        fileOut.WriteLine("MID:,Description:");
			        foreach (ParamString MID in ListMID)
			        {
				        fileOut.Write(MID.param.ToString());
				        fileOut.Write(" (" + Utility.Dec2Hex(MID.param, 2, true) + ")");
				        fileOut.Write(",");
				        fileOut.WriteLine(MID.desc);
			        }
			        fileOut.WriteLine("");
		        }

		        if (ListSA.Count != 0)
		        {
			        fileOut.WriteLine("==== J1939 Source Addresses ====");
			        fileOut.WriteLine("SA:,Description:");
			        foreach (ParamString SA in ListSA)
			        {
				        fileOut.Write(SA.param.ToString());
				        fileOut.Write(" (" + Utility.Dec2Hex(SA.param, 2, true) + ")");
				        fileOut.Write(",");
				        fileOut.WriteLine(SA.desc);	
			        }
			        fileOut.WriteLine("");
		        }

                foreach (Filter Filt in ListFilter.List)
		        {
                    switch (Filt.type)
			        {
                        case Filter.FilterLineType.ofltTitle:	//Title line
					        fileOut.WriteLine("");
                            fileOut.WriteLine(Filt.title);
					        break;

                        case Filter.FilterLineType.ofltPID:	//J1708/J1587 Parameter Identification	
                            foreach (ParamString PID in ListPID)
					        {
                                if (PID.param == Filt.PID)
						        {
							        fileOut.Write("PID: ");
                                    fileOut.Write(Filt.PID.ToString());
							        //fileOut.Write(" (" + Dec2Hex(Local.PID, 2, true) + ")");
							        fileOut.Write(",");
                                    fileOut.WriteLine(PID.desc);

                                    foreach (Param1708 P1708 in List1708)
                                        if (P1708.PID == Filt.PID)
								        {
									        fileOut.Write(",,");
                                            fileOut.Write(P1708.MID.ToString());
                                            fileOut.Write(" (" + Utility.Dec2Hex(P1708.MID, 2, true) + ")");
									        fileOut.Write(",: ");
                                            fileOut.WriteLine(P1708.param);
								        }
							        break;
						        }
					        }
					        break;

                        case Filter.FilterLineType.ofltPGN:
                        case Filter.FilterLineType.ofltSPN:	//J1939 Parameter Group Number (No SPN)
                            foreach (ParamString PGN in ListPGN)
					        {
                                HasSPN = (Filt.type == Filter.FilterLineType.ofltSPN);

                                if (PGN.param == Filt.PGN)
						        {
							        Found = !HasSPN;
							        if (HasSPN)
							        {
                                        foreach (ParamString SPN in ListSPN)
                                            if (SPN.param == Filt.SPN)
									        {
                                                Desc = SPN.desc;
                                                Found = true;
										        break;
									        }
							        }							
							
							        if (Found)
							        {
								        if (HasSPN)			
									        fileOut.Write("PGN/SPN: ");	
								        else
									        fileOut.Write("PGN: ");

                                        fileOut.Write(Filt.PGN.ToString());
								        //fileOut.Write(" (" + Dec2Hex(Local.PGN, 4, true) + ")");
								
								        if (HasSPN)
								        {
									        fileOut.Write("/");
                                            fileOut.Write(Filt.SPN.ToString());
									        //fileOut.Write(" (" + Dec2Hex(Local.SPN, 4, true) + ")");
									        fileOut.Write(",");
                                            fileOut.WriteLine(Desc);									
								        }
								        else
								        {
									        fileOut.Write(",");		
									        fileOut.WriteLine(PGN.desc);
								        }

                                        foreach (Param1939 P1939 in List1939)
								        //for ( Iter1939 = list1939.begin( ) ; Iter1939 != list1939.end( ) ; Iter1939++ )
								        {
									        //Local1939 = *Iter1939;

                                            if ((!HasSPN && (P1939.PGN == Filt.PGN)) || (HasSPN && (P1939.PGN == Filt.PGN) && (P1939.SPN == Filt.SPN))) 
									        {
										        fileOut.Write(",,");
                                                fileOut.Write(P1939.SA.ToString());
                                                fileOut.Write(" (" + Utility.Dec2Hex(P1939.SA, 2, true) + ")");
										        fileOut.Write(",: ");
                                                fileOut.Write(P1939.param);
										        fileOut.Write(",");
										        //Valid =	IsValid1939Param(Local1939.param);
                                                fileOut.Write(P1939.HasValidData.ToString());
										        fileOut.Write(",");
                                                fileOut.WriteLine(Utility.GetCharFromHexSubstring(P1939.param));
									        }
								        }
								        break;
							        }

						        }
					        }
					        break;



				        default:

					        break;
			        }
		        }

                fileOut.Close();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message, fileName);
            }
            */
        }

        public void SaveFilteredParamCsvFile()//FilterArray ListFilter)
        {
            /*
	        String path = "";//"C:\\Users\\Mike\\Documents\\Visual Studio 2010\\Projects\\Smith Chart\\Data";

	        SaveFileDialog saveFileDialog1 = new SaveFileDialog();
  
	        saveFileDialog1.InitialDirectory = path;
	        saveFileDialog1.Filter = "csv files (*.csv)|*.csv|txt files (*.txt)|*.txt|All files (*.*)|*.*";
	        saveFileDialog1.FilterIndex = 1;
	        saveFileDialog1.RestoreDirectory = true;
  
	        if (saveFileDialog1.ShowDialog() == DialogResult.OK )
	        {
		        WriteFilteredParamCsvFile(saveFileDialog1.FileName, ListFilter);
	        }
             */
        }





    }
}
