﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace LMU_Terminal
{
    class Property
    {
        // LmuDevice Path
        public String lmuDevicePath;

        // Terminal Parameters
        private int defRowLimit; 
        public Boolean autoScroll;

        // Text Colors
        public Color defaultColor;
        public Color nmeaColor;
        public Color cellColor;
        public Color triggerColor;
        public Color eventsColor;
        public Color vbusColor;
        public Color hexColor;
        public Color jpod2Color;
        public Color haColor;
        public Color everythingElseColor;

        // Check Box States
        public Boolean nmeaChecked;
        public Boolean cellChecked;
        public Boolean triggerChecked;
        public Boolean eventsChecked;
        public Boolean vbusChecked;
        public Boolean hexChecked;
        public Boolean jpodChecked;
        public Boolean haChecked;
        public Boolean everythingElseChecked;

        // Serial Port Options
        //public ComSettings comSettings; 

        public Property()  // constructor
        {
            //-------------------------------------
            //Load Default Properties from Storage
            //-------------------------------------

            // LmuDevice Path
            lmuDevicePath = Properties.Settings.Default.LmuDevicePath;
            if (lmuDevicePath == "") // Let's choose a better staring point
            {
                //lmuDevicePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                //lmuDevicePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase);
                lmuDevicePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                //lmuDevicePath = AppDomain.CurrentDomain.BaseDirectory;
            }

            // Terminal Parameters
            defRowLimit = Properties.Settings.Default.RowLimit;
            autoScroll = Properties.Settings.Default.AutoScroll;

            // Text Colors
            defaultColor = Properties.Settings.Default.DefaultColor;
            nmeaColor = Properties.Settings.Default.NmeaColor;
            cellColor = Properties.Settings.Default.CellColor;
            triggerColor = Properties.Settings.Default.TriggerColor;
            eventsColor = Properties.Settings.Default.EventColor;
            vbusColor = Properties.Settings.Default.VbusColor;
            hexColor = Properties.Settings.Default.HexColor;
            jpod2Color = Properties.Settings.Default.Jpod2Color;
            haColor = Properties.Settings.Default.HAColor;
            everythingElseColor = Properties.Settings.Default.EverythingElseColor;

            // Check Box States
            nmeaChecked = Properties.Settings.Default.NmeaChecked;
            cellChecked = Properties.Settings.Default.CellChecked;
            triggerChecked = Properties.Settings.Default.TriggerChecked;
            eventsChecked = Properties.Settings.Default.EventChecked;
            vbusChecked = Properties.Settings.Default.VbusChecked;
            hexChecked = Properties.Settings.Default.HexChecked;
            jpodChecked = Properties.Settings.Default.JpodChecked;
            haChecked = Properties.Settings.Default.HAChecked;
            everythingElseChecked = Properties.Settings.Default.EverythingElseChecked;


            // Serial Port Options
            //comSettings = new ComSettings();
            //comSettings.PortNumber = Properties.Settings.Default.ComPort;
            //comSettings.BaudRate = (ComPortBaudRate)Properties.Settings.Default.BaudRate;
            //comSettings.DataBits = (ComPortDataBits)Properties.Settings.Default.DataBits;
            //comSettings.Parity = (ComPortParity)Properties.Settings.Default.Parity;
            //comSettings.StopBits = (ComPortStopBits)Properties.Settings.Default.StopBits;
        }

        ~Property()  // destructor
        {
            //Save Properties to Storage

            // LmuDevice Path
            Properties.Settings.Default.LmuDevicePath = lmuDevicePath;

            // Terminal Parameters
            Properties.Settings.Default.AutoScroll = autoScroll;

            // Text Colors
            Properties.Settings.Default.DefaultColor = defaultColor;
            Properties.Settings.Default.NmeaColor = nmeaColor;
            Properties.Settings.Default.CellColor = cellColor;
            Properties.Settings.Default.TriggerColor = triggerColor;
            Properties.Settings.Default.EventColor = eventsColor;
            Properties.Settings.Default.VbusColor = vbusColor;
            Properties.Settings.Default.HexColor = hexColor;
            Properties.Settings.Default.Jpod2Color = jpod2Color;
            Properties.Settings.Default.HAColor = haColor;
            Properties.Settings.Default.EverythingElseColor = everythingElseColor;

            // Check Box States
            Properties.Settings.Default.NmeaChecked = nmeaChecked;
            Properties.Settings.Default.CellChecked = cellChecked;
            Properties.Settings.Default.TriggerChecked = triggerChecked;
            Properties.Settings.Default.EventChecked = eventsChecked;
            Properties.Settings.Default.VbusChecked = vbusChecked;
            Properties.Settings.Default.HexChecked = hexChecked;
            Properties.Settings.Default.JpodChecked = jpodChecked;
            Properties.Settings.Default.HAChecked = haChecked;
            Properties.Settings.Default.EverythingElseChecked = everythingElseChecked;


            // Serial Port Options
            //Properties.Settings.Default.ComPort = (byte)comSettings.PortNumber;
            //Properties.Settings.Default.BaudRate = (int)comSettings.BaudRate;
            //Properties.Settings.Default.DataBits = (byte)comSettings.DataBits;
            //Properties.Settings.Default.Parity = (byte)comSettings.Parity;
            //Properties.Settings.Default.StopBits = (byte)comSettings.StopBits;
            //Properties.Settings.Default.DsrDtrHs = false;
            //Properties.Settings.Default.RtsCtsHs = false;
            //Properties.Settings.Default.XonXoffHs = false;
            //Properties.Settings.Default.Rs485Hs = false;
            //Properties.Settings.Default.EnableDtr = true;
            //Properties.Settings.Default.EnableRts = true;

            Properties.Settings.Default.Save();            
        }

        public int rowLimit
        {
            get { return defRowLimit; }
        }

        public void SetChecked(String textParam, CheckState checkState)
        {
            if (textParam == "NMEA")
                nmeaChecked = ControlExtensions.CheckStateToBoolean(checkState);
            else if (textParam == "Cell")
                cellChecked = ControlExtensions.CheckStateToBoolean(checkState);
            else if (textParam == "Triggers")
                triggerChecked = ControlExtensions.CheckStateToBoolean(checkState);
            else if (textParam == "Events")
                eventsChecked = ControlExtensions.CheckStateToBoolean(checkState);
            else if (textParam == "VBUS")
                vbusChecked = ControlExtensions.CheckStateToBoolean(checkState);
            else if (textParam == "HEX")
                hexChecked = ControlExtensions.CheckStateToBoolean(checkState);
            else if (textParam == "JPod2")
                jpodChecked = ControlExtensions.CheckStateToBoolean(checkState);
            else if (textParam == "HA")
                haChecked = ControlExtensions.CheckStateToBoolean(checkState);
            else if (textParam == "Everything Else")
                everythingElseChecked = ControlExtensions.CheckStateToBoolean(checkState);
        }

        public CheckState GetChecked(String textParam)
        {
            Boolean isChecked = false;

            if (textParam == "NMEA")
                isChecked = nmeaChecked;
            else if (textParam == "Cell")
                isChecked = cellChecked;
            else if (textParam == "Triggers")
                isChecked = triggerChecked;
            else if (textParam == "Events")
                isChecked = eventsChecked;
            else if (textParam == "VBUS")
                isChecked = vbusChecked;
            else if (textParam == "HEX")
                isChecked = hexChecked;
            else if (textParam == "JPod2")
                isChecked = jpodChecked;
            else if (textParam == "HA")
                isChecked = haChecked;
            else if (textParam == "Everything Else")
                isChecked = everythingElseChecked;

            return ControlExtensions.BooleanToCheckState(isChecked);
        }

        public void SetColor(String textParam, Color color)
        {
            if (textParam == "Default")
                defaultColor = color;
            else if (textParam == "NMEA")
                nmeaColor = color;
            else if (textParam == "Cell")
                cellColor = color;
            else if (textParam == "Triggers")
                triggerColor = color;
            else if (textParam == "Events")
                eventsColor = color;
            else if (textParam == "VBUS")
                vbusColor = color;
            else if (textParam == "HEX")
                hexColor = color;
            else if (textParam == "JPod2")
                jpod2Color = color;
            else if (textParam == "HA")
                haColor = color;
            else if (textParam == "Everything Else")
                everythingElseColor = color;
        }

        public Color GetColor(String textParam)
        {
            Color color = defaultColor;

            if (textParam == "Default")
                color = defaultColor;
            else if (textParam == "NMEA")
                color = nmeaColor;
            else if (textParam == "Cell")
                color = cellColor;
            else if (textParam == "Triggers")
                color = triggerColor;
            else if (textParam == "Events")
                color = eventsColor;
            else if (textParam == "VBUS")
                color = vbusColor;
            else if (textParam == "HEX")
                color = hexColor;
            else if (textParam == "JPod2")
                color = jpod2Color;
            else if (textParam == "HA")
                color = haColor;
            else if (textParam == "Everything Else")
                color = everythingElseColor;
            return color;
        }
    }
}
