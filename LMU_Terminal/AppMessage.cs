﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LMU_Terminal
{
    class AppMessage
    {
        private static readonly CommentEntryType[] VbusDataRptStr = { new CommentEntryType(44, "JBUS Data: Basic Parms Report"),
                                                                      new CommentEntryType(45, "JBUS Data: Fuel/Idle Report"),
                                                                      new CommentEntryType(46, "JBUS Data: Time/Temp Report"),
                                                                      new CommentEntryType(48, "JBUS Data: Old J1708 VIN Report"),
                                                                      new CommentEntryType(49, "JBUS Data: VIN Report"),
                                                                      new CommentEntryType(145, "JBUS Data: Fleet Daily Report"),
                                                                      new CommentEntryType(146, "JBUS Data: Fleet Hourly Report"),
                                                                      new CommentEntryType(147, "JBUS Data: Construction Daily Report"),
                                                                      new CommentEntryType(148, "JBUS Data: Construction Daily Usage Report"),
                                                                      new CommentEntryType(149, "Construction Hourly Report"),
                                                                      new CommentEntryType(151, "JBUS Data: J1939 DTC Codes"),
                                                                      new CommentEntryType(152, "JBUS Data: J1708 DTC Codes"),
                                                                      new CommentEntryType(170, "JBUS Data: Bobcat Hydraulic Report"),
                                                                      new CommentEntryType(171, "JBUS Data: Bobcat Fault Report"),
                                                                      new CommentEntryType(172, "JBUS Data: Bobcat Instant Report"),
                                                                      new CommentEntryType(173, "JBUS Data: Bobcat Run Report"),
                                                                      new CommentEntryType(174, "JBUS Data: Bobcat Run Usage Report")
                                                                    };

        private static readonly CommentEntryType[] JbusMachineStateStr = { new CommentEntryType(0, "EngineOn"),
                                                                           new CommentEntryType(1, "PtoOn"),
                                                                           new CommentEntryType(2, "Moving"),
                                                                           new CommentEntryType(6, "RcvdJ1708"),
                                                                           new CommentEntryType(7, "RcvdJ1939")
                                                                         };

        private static readonly string[] JbusVinMsgStr = { "No VIN seen",
                                                           "1708 VIN",
                                                           "J1939 VIN",
                                                           "Both VIN"
                                                         };

        private bool Add0x = false;
        private int ReportMsgType;
        private ParamList paramList;
        private string idString;

        public override string ToString()
        {
            return idString;
        }

        public AppMessage(byte[] BinMsg, int pos, int msgType, int msgLength)
        {
            ReportMsgType = -1;
            paramList = new ParamList("Parameter", "Value", "Units", "Hex", "Comment", "Map Revision");
            idString = "Invalid";
            int NumDTCs;

            if ((pos + msgLength) >= BinMsg.Length)
            {
                switch (msgType)
                {
                    case 10: //IP Request  
                        idString = "IP Request";
                        break;
                    case 11: //IP Report  
                        idString = "IP Report";
                        break;
                    case 50: //Time Sync  
                        idString = "Time Sync";
                        break;
                    case 100: //Download ID Report  
                        idString = "Download ID Report";
                        break;
                    case 101: //Download Authorization  
                        idString = "Download Authorization  ";
                        break;
                    case 102: //Download Request  
                        idString = "Download Request  ";
                        break;
                    case 103: //Download Update 
                        idString = "Download Update ";
                        break;
                    case 104: //Download Complete
                        idString = "Download Complete";
                        break;
                    case 105: //Download, HTTP, LMU FW  
                        idString = "Download, HTTP, LMU FW ";
                        break;
                    case 106: //Download, HTTP, File  
                        idString = "Download, HTTP, File  ";
                        break;
                    case 107: //OTA Download  
                        idString = "OTA Download  ";
                        break;
                    case 110: //AT Command  
                        idString = "AT Command  ";
                        break;
                    case 111: //Version Report  
                        idString = "Version Report  ";
                        break;
                    case 112: //GPS Status Report  
                        idString = "GPS Status Report";
                        break;
                    case 113: //Message Statisics Report  
                        idString = "Message Statisics Report";
                        break;
                    case 115: //State Report  
                        idString = "State Report";
                        break;
                    case 116: //Geo-Zone Action Message  
                        idString = "Geo-Zone Action Message";
                        break;
                    case 117: //Geo-Zone Update Message  
                        idString = "Geo-Zone Update Message";
                        break;
                    case 118: //Probe ID Report  
                        idString = "Probe ID Report";
                        break;
                    case 120: //Capture Report  
                        idString = "Capture Report";
                        break;
                    case 122: //Motion Log Report  
                        idString = "Motion Log Report";
                        break;
                    case 123: //Compressed Motion Log Report  
                        idString = "Compressed Motion Log Report";
                        break;
                 
                    case 131: //Vehicle ID Report  
                        idString = "Vehicle ID Report";
                        break;
                    case 132: //VBus DTC Report  
                        idString = "VBus DTC Report";
                        break;
                    case 133: //VBus VIN Decode Lookup  
                        idString = "VBus VIN Decode Lookup";
                        break;
                    case 134: //Squarell Command Message  
                        idString = "Squarell Command Message";
                        break;
                    case 135: //Squarell Status Message  
                        idString = "Squarell Status Message";
                        break;
                    case 136: //VBus Register Device Message  
                        idString = "VBus Register Device Message";
                        break;
                    case 137: //VBus Freeze Frame  
                        idString = "VBus Freeze Frame";
                        break;
                    case 138: //VBus Diagnostics Report  
                        //idString = "VBus Diagnostics Report";
                        idString = "ID 138 DTC Report";

                        NumDTCs = (int)paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Dtc138, Endian.Little, "ID138 DTCs", "-");
                        idString += NumDTCs.ToString() + " DTC(s)";
                        break;
                    //case (130 + 0x10000): //VBus Data Report (Type 14)
                    case (130): //VBus Data Report (Type 14)
                        DecodeType14VbusDataReport(BinMsg, pos, msgType, msgLength);
                        break;
                    case 211: // VBus Data Report (Type 5)
                        DecodeType5VbusDataReport(BinMsg, pos, msgType, msgLength);
                        break;
                 
                    default:
                        idString = "(" + msgType.ToString() + ") Unknown";
                        break;
                } //switch (msgType)
            } //if (pos + msgLength >= BinMsg.Length)

        }

        private void DecodeJbusDataHeader(byte[] BinMsg, ref int pos)
        {
            paramList.AddDecodedParam(BinMsg, ref pos, 2, ParamType.Uint, Endian.Little, "Machine State (MS)", "", new Comment(JbusMachineStateStr, true), "0");
            paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Map Revision", "", "", "0");
        }

        private void DecodeJbusDTC(byte[] BinMsg, ref int pos)
        {



        }

        private void DecodeType5VbusDataReport(byte[] BinMsg, int pos, int msgType, int msgLength)// type 5
        {
            //int NumDTCs;
            ReportMsgType = (int)paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Vbus Data Report Type", "", new Comment(VbusDataRptStr, false), "");
            //            pos += 1;
            //            ReportMsgType = 148;
            string valueundermaprevision = "0";
            int J186lengthbit = 4;
            double faultcount = 0;

            

            //switch ()
            //NumDTCs = (int)paramList.AddJbusParam(BinMsg, ref pos, JbusParam.DTC138, Endian.Little, "ID138 DTCs", "-");
            //idString += NumDTCs.ToString() + " DTC";
            //break;

            switch (ReportMsgType)
            {



                //case 151:
                //    idString = "JBUS Data: J1939 DM1 - ";
                //    NumDTCs = (int)paramList.AddJbusParam(BinMsg, ref pos, JbusParam.DtcJ1939, Endian.Little, "J1939 DTCs", "-");
                //    idString += NumDTCs.ToString() + " DTC(s)";
                //    break;
                //case 138: idString = "JBUS Data: ID 138 DM1 - ";
                //    NumDTCs = (int)paramList.AddJbusParam(BinMsg, ref pos, JbusParam.DTC138, Endian.Little, "ID138 DTCs", "-");
                //    idString += NumDTCs.ToString() + " DTC";
                //    break;
                case 171:
                    idString = "JBUS Data: Bobcat Fault Report";
                    
                    paramList.AddScaledParam(BinMsg, ref pos, 1, 1.0, 0, false, Endian.Little, "Map Revision", "", "", " ");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Source Address", "", "Bobcat Custom CAN04", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Device", "", "Bobcat Custom CAN04", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Function", "", "Bobcat Custom CAN04", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Failure", "", "Bobcat Custom CAN04", valueundermaprevision);


                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Devicebatteryvoltage, Endian.Big, "IoT Battery Level", valueundermaprevision); // update 09/26
                    paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "Geozone state before", "", "Geozone state before", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "Geozone state after", "", "Geozone state after", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Enginehours, Endian.Big, "Engine Hours", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Enginespeed, Endian.Big, "Engine Speed(RPM)", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Fuellevel, Endian.Big, "Fuel Level", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Deflevel, Endian.Big, "DEF Level", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Serviceclockhours, Endian.Big, "Service Clock Hours", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "Attachment ID", "", "Attachment ID", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "User ID", "", "User ID", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Cumulativefuelusage, Endian.Big, "Cumulative Fuel Usage", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Devicebatteryvoltage, Endian.Big, "Internal Battery Level", valueundermaprevision); // update 1/17/19 
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Dtccount, Endian.Big, "DTC Count", valueundermaprevision);  // added 8/19/2019
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Bobcatfaultcount, Endian.Big, "Bobcat Fault Count", valueundermaprevision);  // added 8/19/2019
                    break;

                case 172:
                    idString = "JBUS Data: Bobcat Instant Report";
                    //paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Map Revision", "", "", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 1, 1.0, 0, false, Endian.Little, "Map Revision", "", "", " ");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn91, Endian.Little, "Throttle Position", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn513, Endian.Little, "Load (Actual Engine Percent Torque)", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn3515, Endian.Little, "DEF Temperature", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn3516, Endian.Little, "DEF Concentration", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn157, Endian.Little, "Fuel Rail Pressure", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn110, Endian.Little, "Engine Coolant Temperature", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn174, Endian.Little, "Engine Fuel Temperature", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn175, Endian.Little, "Engine Oil Temperature", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn100, Endian.Little, "Engine Oil Pressure", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Brake Switch", "", "SPN 70 Format", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn183, Endian.Little, "Fuel Rate", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Seatbar position", "", "Bobcat Custom BICS", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Door position", "", "Bobcat Custom BICS", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Workgroup State", "", "Bobcat Custom BICS", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Drive State", "", "Bobcat Custom BICS", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Drive Response", "", "Bobcat Custom BICS", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Steering Drift Forward", "", "Bobcat Custom BICS", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Steering Drift Reverse", "", "Bobcat Custom BICS", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Front Base Solenoid", "", "Bobcat Custom CAN02", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Front Rod Solenoid", "", "Bobcat Custom CAN02", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Rear Base Solenoid", "", "Bobcat Custom CAN02", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Rear Rod Solenoid", "", "Bobcat Custom CAN02", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "High Flow Solenoid", "", "Bobcat Custom CAN02", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Engine State (Cranking)", "", "Bobcat Custom CAN03", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Auxiliary Enable", "", "Bobcat Custom CAN20", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "High Flow", "", "Bobcat Custom CAN20", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_HydraulicChargePressure, Endian.Little, "Hydraulic Charge Pressure", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_HydraulicOilTemperature, Endian.Little, "Hydraulic Oil Temperature", valueundermaprevision);


                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Devicebatteryvoltage, Endian.Big, "IoT Battery Level", valueundermaprevision); // update 09/26
                    paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "Geozone state before", "", "Geozone state before", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "Geozone state after", "", "Geozone state after", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Enginehours, Endian.Big, "Engine Hours", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Enginespeed, Endian.Big, "Engine Speed(RPM)", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Fuellevel, Endian.Big, "Fuel Level", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Deflevel, Endian.Big, "DEF Level", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Serviceclockhours, Endian.Big, "Service Clock Hours", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "Attachment ID", "", "Attachment ID", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "User ID", "", "User ID", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Cumulativefuelusage, Endian.Big, "Cumulative Fuel Usage", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Devicebatteryvoltage, Endian.Big, "Internal Battery Level", valueundermaprevision); // update 1/17/19
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Dtccount, Endian.Big, "DTC Count", valueundermaprevision);  // added 8/19/2019
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Bobcatfaultcount, Endian.Big, "Bobcat Fault Count", valueundermaprevision);  // added 8/19/2019
                    break;

                case 173:
                    idString = "JBUS Data: Bobcat Run Report";
                    paramList.AddScaledParam(BinMsg, ref pos, 1, 1.0, 0, false, Endian.Little, "Map Revision", "", "", " ");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn91, Endian.Little, "throttle_position_min, Min Throttle Position", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn91, Endian.Little, "throttle_position_max, Max Throttle Position", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn91, Endian.Little, "throttle_position_average, Avg Throttle Position", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn3516, Endian.Little, "def_concentration_min, Min DEF Concentration", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn3516, Endian.Little, "def_concentration_max, Max DEF Concentration", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn3516, Endian.Little, "def_concentration_average, Avg DEF Concentration", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn1761, Endian.Little, "def_level_min, Min DEF Level", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn1761, Endian.Little, "def_level_max, Max DEF Level", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn157, Endian.Little, "fuel_rail_pressure_min, Min Fuel Rail Pressure", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn157, Endian.Little, "fuel_rail_pressure_max, Max Fuel Rail Pressure", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn157, Endian.Little, "fuel_rail_pressure_average, Avg Fuel Rail Pressure", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn110, Endian.Little, "engine_coolant_temperature_min, Min Engine Coolant Temperature", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn110, Endian.Little, "engine_coolant_temperature_max, Max Engine Coolant Temperature", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn110, Endian.Little, "engine_coolant_temperature_average, Avg Engine Coolant Temperature", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn174, Endian.Little, "engine_fuel_temperature_min, Min Engine Fuel Temperature", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn174, Endian.Little, "engine_fuel_temperature_max, Max Engine Fuel Temperature", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn174, Endian.Little, "engine_fuel_temperature_average, Avg Engine Fuel Temperature", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn175, Endian.Little, "engine_oil_temperature_min, Min Engine Oil Temperature", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn175, Endian.Little, "engine_oil_temperature_max, Max Engine Oil Temperature", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn175, Endian.Little, "engine_oil_temperature_average, Avg Engine Oil Temperature", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn100, Endian.Little, "engine_oil_pressure_min, Min Engine Oil Pressure", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn100, Endian.Little, "engine_oil_pressure_max, Max Engine Oil Pressure", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn100, Endian.Little, "engine_oil_pressure_average, Avg Engine Oil Pressure", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn183, Endian.Little, "fuel_consumption_min, Min Fuel Rate", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn183, Endian.Little, "fuel_consumption_max, Max Fuel Rate", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn183, Endian.Little, "fuel_consumption_average, Avg Fuel Rate", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn172, Endian.Little, "engine_air_intake_temperature_min, Min Engine Air Intake Temperature", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn172, Endian.Little, "engine_air_intake_temperature_max, Max Engine Air Intake Temperature", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn172, Endian.Little, "engine_air_intake_temperature_average, Avg Engine Air Intake Temperature", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_HydraulicChargePressure, Endian.Little, "hydraulic_charge_pressure_min, Min Hydraulic Charge Pressure", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_HydraulicChargePressure, Endian.Little, "hydraulic_charge_pressure_max, Max Hydraulic Charge Pressure", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_HydraulicChargePressure, Endian.Little, "hydraulic_charge_pressure_average, Avg Hydraulic Charge Pressure", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_HydraulicOilTemperature, Endian.Little, "hydraulic_oil_temperature_min, Min Hydraulic Oil Temperature", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_HydraulicOilTemperature, Endian.Little, "hydraulic_oil_temperature_max, Max Hydraulic Oil Temperature", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_HydraulicOilTemperature, Endian.Little, "hydraulic_oil_temperature_average, Avg Hydraulic Oil Temperature", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Gateway (0xB0) Version", "", "Bobcat Custom CAN01 Version", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Gateway (0xB0) Major", "", "Bobcat Custom CAN01 Major", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Gateway (0xB0) Minor", "", "Bobcat Custom CAN01 Minor", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Auxiliary/MX Secondary (0xE4) Version", "", "Bobcat Custom CAN01 Version", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Auxiliary/MX Secondary (0xE4) Major", "", "Bobcat Custom CAN01 Major", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Auxiliary/MX Secondary (0xE4) Minor", "", "Bobcat Custom CAN01 Minor", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Standard Panel (0x17) Version", "", "Bobcat Custom CAN01 Version", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Standard Panel (0x17) Major", "", "Bobcat Custom CAN01 Major", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Standard Panel (0x17) Minor", "", "Bobcat Custom CAN01 Minor", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Drive (0x09 or 0x13) Version", "", "Bobcat Custom CAN01 Version", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Drive (0x09 or 0x13) Major", "", "Bobcat Custom CAN01 Major", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Drive (0x09 or 0x13) Minor", "", "Bobcat Custom CAN01 Minor", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Workgroup/Throttle (0xFC) Version", "", "Bobcat Custom CAN01 Version", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Workgroup/Throttle (0xFC) Major", "", "Bobcat Custom CAN01 Major", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Workgroup/Throttle (0xFC) Minor", "", "Bobcat Custom CAN01 Minor", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Deluxe Panel (0x54) Version", "", "Bobcat Custom CAN01 Version", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Deluxe Panel (0x54) Major", "", "Bobcat Custom CAN01 Major", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Deluxe Panel (0x54) Minor", "", "Bobcat Custom CAN01 Minor", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "ACD (0xB4) Version", "", "Bobcat Custom CAN01 Version", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "ACD (0xB4) Major", "", "Bobcat Custom CAN01 Major", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "ACD (0xB4) Minor", "", "Bobcat Custom CAN01 Minor", valueundermaprevision);


                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Devicebatteryvoltage, Endian.Big, "IoT Battery Level", valueundermaprevision); // update 09/26
                    paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "Geozone state before", "", "Geozone state before", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "Geozone state after", "", "Geozone state after", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Enginehours, Endian.Big, "Engine Hours", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Enginespeed, Endian.Big, "Engine Speed(RPM)", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Fuellevel, Endian.Big, "Fuel Level", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Deflevel, Endian.Big, "DEF Level", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Serviceclockhours, Endian.Big, "Service Clock Hours", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "Attachment ID", "", "Attachment ID", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "User ID", "", "User ID", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Cumulativefuelusage, Endian.Big, "Cumulative Fuel Usage", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Devicebatteryvoltage, Endian.Big, "Internal Battery Level", valueundermaprevision); // update 1/17/19
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Dtccount, Endian.Big, "DTC Count", valueundermaprevision);  // added 8/19/2019
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Bobcatfaultcount, Endian.Big, "Bobcat Fault Count", valueundermaprevision);  // added 8/19/2019
                    break;

                case 174:
                    idString = "JBUS Data: Bobcat Run Usage Report";
                    //paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Map Revision", "", "", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 1, 1.0, 0, false, Endian.Little, "Map Revision", "", "", " ");
                    paramList.AddDecodedParam(BinMsg, ref pos, 2, ParamType.Uint, Endian.Little, "seatbar_open_count, Seatbar Position (Times Cycled)", "", "Bobcat Custom", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 2, ParamType.Uint, Endian.Little, "door_open_count, Door Position (Times Opened)", "", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "workgroup_on_count, WorkGroup State (Time not OFF)", "", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "drive_state_on_count, Drive State (Time not OFF)", "", "Bobcat Custom", valueundermaprevision);

                    paramList.AddDecodedParam(BinMsg, ref pos, 2, ParamType.Uint, Endian.Little, "engine_state_cranking_count, Engine State (Times Cranked)", "", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "engine_state_cranking_duration, Engine State (Time During Last Crank)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "engine_state_cranking_max, Engine State (Max Single Cranking Time)", "s", "Bobcat Custom", valueundermaprevision);

                    paramList.AddScaledParam(BinMsg, ref pos, 2, 6.0, 0, false, Endian.Little, "engine_on_time, Engine Total On Time", "s", "Bobcat Custom", valueundermaprevision);

                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "throttle_position_binned1, Time at Throttle Position (0 to 25 %)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "throttle_position_binned2, Time at Throttle Position (26 to 50 %)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "throttle_position_binned3, Time at Throttle Position (51 to 75 %)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "throttle_position_binned4, Time at Throttle Position (76 to 100 %)", "s", "Bobcat Custom", valueundermaprevision);

                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "engine_speed_duration_binned1, Time at Engine Speed (RPM) (0 to 1099 RPM)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "engine_speed_duration_binned2, Time at Engine Speed (RPM) (1100 to 1199 RPM)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "engine_speed_duration_binned3, Time at Engine Speed (RPM) (1200 to 1299 RPM)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "engine_speed_duration_binned4, Time at Engine Speed (RPM) (1300 to 1399 RPM)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "engine_speed_duration_binned5, Time at Engine Speed (RPM) (1400 to 1499 RPM)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "engine_speed_duration_binned6, Time at Engine Speed (RPM) (1500 to 1599 RPM)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "engine_speed_duration_binned7, Time at Engine Speed (RPM) (1600 to 1699 RPM)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "engine_speed_duration_binned8, Time at Engine Speed (RPM) (1700 to 1799 RPM)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "engine_speed_duration_binned9, Time at Engine Speed (RPM) (1800 to 1899 RPM)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "engine_speed_duration_binned10, Time at Engine Speed (RPM) (1900 to 1999 RPM)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "engine_speed_duration_binned11, Time at Engine Speed (RPM) (2000 to 2099 RPM)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "engine_speed_duration_binned12, Time at Engine Speed (RPM) (2100 to 2199 RPM)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "engine_speed_duration_binned13, Time at Engine Speed (RPM) (2200 to 2299 RPM)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "engine_speed_duration_binned14, Time at Engine Speed (RPM) (2300 to 2399 RPM)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "engine_speed_duration_binned15, Time at Engine Speed (RPM) (2400 to 2499 RPM)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "engine_speed_duration_binned16, Time at Engine Speed (RPM) (2500 to 2599 RPM)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "engine_speed_duration_binned17, Time at Engine Speed (RPM) (2600 to 2699 RPM)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "engine_speed_duration_binned18, Time at Engine Speed (RPM) (2700 to 2799 RPM)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "engine_speed_duration_binned19, Time at Engine Speed (RPM) (2800+ RPM)", "s", "Bobcat Custom", valueundermaprevision);

                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "load_duration_binned1, Time at Load (Actual Engine - Percent Torque) (< 5 %)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "load_duration_binned2, Time at Load (Actual Engine - Percent Torque) (5 to 9 %)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "load_duration_binned3, Time at Load (Actual Engine - Percent Torque) (10 to 14 %)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "load_duration_binned4, Time at Load (Actual Engine - Percent Torque) (15 to 19 %)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "load_duration_binned5, Time at Load (Actual Engine - Percent Torque) (20 to 24 %)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "load_duration_binned6, Time at Load (Actual Engine - Percent Torque) (25 to 29 %)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "load_duration_binned7, Time at Load (Actual Engine - Percent Torque) (30 to 34 %)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "load_duration_binned8, Time at Load (Actual Engine - Percent Torque) (35 to 39 %)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "load_duration_binned9, Time at Load (Actual Engine - Percent Torque) (40 to 50 %)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "load_duration_binned10, Time at Load (Actual Engine - Percent Torque) (51 and up %)", "s", "Bobcat Custom", valueundermaprevision);

                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "def_temperature_binned1, Time at DEF Temperature (< -13 °C)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "def_temperature_binned2, Time at DEF Temperature (-13 to 15 °C)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "def_temperature_binned3, Time at DEF Temperature (16 to 43 °C)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "def_temperature_binned4, Time at DEF Temperature (44 to 54 °C)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "def_temperature_binned5, Time at DEF Temperature (55 to 65 °C)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "def_temperature_binned6, Time at DEF Temperature (66+ °C)", "s", "Bobcat Custom", valueundermaprevision);

                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "engine_coolant_temperature_binned1, Time at Engine Coolant Temperature (< 83 °C)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "engine_coolant_temperature_binned2, Time at Engine Coolant Temperature (83 to 88 °C)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "engine_coolant_temperature_binned3, Time at Engine Coolant Temperature (89 to 93 °C)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "engine_coolant_temperature_binned4, Time at Engine Coolant Temperature (94 to 99 °C)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "engine_coolant_temperature_binned5, Time at Engine Coolant Temperature (100 to 104 °C)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "engine_coolant_temperature_binned6, Time at Engine Coolant Temperature (105 to 110 °C)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "engine_coolant_temperature_binned7, Time at Engine Coolant Temperature (111 to 115 °C)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "engine_coolant_temperature_binned8, Time at Engine Coolant Temperature (116+ °C)", "s", "Bobcat Custom", valueundermaprevision);



                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "def_concentration_binned1, Time at DEF Concentration Temperature - percent (0 to 19.75 %)", "s", "Bobcat Custom", valueundermaprevision);// UPDATE 09/26
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "def_concentration_binned2, Time at DEF Concentration Temperature - percent (20 to 26.75 %)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "def_concentration_binned3, Time at DEF Concentration Temperature - percent (27 to 29.75 %)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "def_concentration_binned4, Time at DEF Concentration Temperature - percent (30 to 31.75 %)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "def_concentration_binned5, Time at DEF Concentration Temperature - percent (32 to 33.75 %)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "def_concentration_binned6, Time at DEF Concentration Temperature - percent (34 to 37.75 %)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "def_concentration_binned7, Time at DEF Concentration Temperature - percent (38 to 41.75 %)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "def_concentration_binned8, Time at DEF Concentration Temperature - percent (42 to 44.75 %)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "def_concentration_binned9, Time at DEF Concentration Temperature - percent (45 to 59.75 %)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "def_concentration_binned10, Time at DEF Concentration Temperature - percent (60 + %)", "s", "Bobcat Custom", valueundermaprevision);

                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "two_speed_on_binned, Two speed usage", "", "Bobcat Custom", valueundermaprevision);

                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "drive_response_binned1, Drive Response (State 1 Time)", "", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "drive_response_binned2, Drive Response (State 2 Time)", "", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "drive_response_binned3, Drive Response (State 3 Time)", "", "Bobcat Custom", valueundermaprevision);

                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "front_base_solenoid_binned, Front Base Solenoid (Time ON)", "", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "front_rod_solenoid_binned, Front Rod Solenoid (Time ON)", "", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "rear_rod_solenoid_binned, Rear Rod Solenoid (Time ON)", "", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "rear_base_solenoid_binned, Rear Base Solenoid (Time ON)", "", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "high_flow_solenoid_binned, High Flow Solenoid (Time ON)", "", "Bobcat Custom", valueundermaprevision);

                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "auxiliary_binned, Auxiliary Enable (Time ON)", "", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "high_flow_binned, High Flow (Time ON)", "", "Bobcat Custom", valueundermaprevision);

                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "hydraulic_oil_temperature_binned1, Time at Hydraulic Oil Temperature (< 38 °C)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "hydraulic_oil_temperature_binned2, Time at Hydraulic Oil Temperature (38 to 59 °C)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "hydraulic_oil_temperature_binned3, Time at Hydraulic Oil Temperature (60 to 70 °C)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "hydraulic_oil_temperature_binned4, Time at Hydraulic Oil Temperature (71 to 81 °C)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "hydraulic_oil_temperature_binned5, Time at Hydraulic Oil Temperature (82 to 87 °C)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "hydraulic_oil_temperature_binned6, Time at Hydraulic Oil Temperature (88 to 93 °C)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "hydraulic_oil_temperature_binned7, Time at Hydraulic Oil Temperature (94 to 98 °C)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "hydraulic_oil_temperature_binned8, Time at Hydraulic Oil Temperature (99 to 104 °C)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "hydraulic_oil_temperature_binned9, Time at Hydraulic Oil Temperature (105 to 110 °C)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "hydraulic_oil_temperature_binned10, Time at Hydraulic Oil Temperature (111+ °C)", "s", "Bobcat Custom", valueundermaprevision);

                    

                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Devicebatteryvoltage, Endian.Big, "IoT Battery Level", valueundermaprevision); // update 09/26
                    paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "Geozone state before", "", "Geozone state before", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "Geozone state after", "", "Geozone state after", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Enginehours, Endian.Big, "Engine Hours", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Enginespeed, Endian.Big, "Engine Speed(RPM)", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Fuellevel, Endian.Big, "Fuel Level", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Deflevel, Endian.Big, "DEF Level", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Serviceclockhours, Endian.Big, "Service Clock Hours", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "Attachment ID", "", "Attachment ID", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "User ID", "", "User ID", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Cumulativefuelusage, Endian.Big, "Cumulative Fuel Usage", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Devicebatteryvoltage, Endian.Big, "Internal Battery Level", valueundermaprevision); // update 1/17/19
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Dtccount, Endian.Big, "DTC Count", valueundermaprevision);  // added 8/19/2019
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Bobcatfaultcount, Endian.Big, "Bobcat Fault Count", valueundermaprevision);  // added 8/19/2019
                    break;


                case 175:
                    idString = "JBus Data: Soft Message";
                    if (BinMsg[pos] <= 2){
                        paramList.AddScaledParam(BinMsg, ref pos, 1, 1.0, 0, false, Endian.Big, "Map Revision", "", "", " ");
                        paramList.AddScaledParam(BinMsg, ref pos, 64, 1.0, 0, false, Endian.Big, "ECU (SA 0x00) SOFT Message", "ASCII", "ECU (SA 0x00) SOFT Message", " ");
                        paramList.AddScaledParam(BinMsg, ref pos, 64, 1.0, 0, false, Endian.Big, "DCU Message", "ASCII", "DCU Message", " ");

                        paramList.AddScaledParam(BinMsg, ref pos, 64, 1.0, 0, false, Endian.Big, "Gateway (SA 0xB0) SOFT Message", "ASCII", "Gateway (SA 0xB0) SOFT Message", " ");
                        paramList.AddScaledParam(BinMsg, ref pos, 64, 1.0, 0, false, Endian.Big, "Auxilliary (SA 0xE4) SOFT Message", "ASCII", "Auxilliary (SA 0xE4) SOFT Message", " ");
                        paramList.AddScaledParam(BinMsg, ref pos, 64, 1.0, 0, false, Endian.Big, "Drive (SA 0x09) SOFT Message", "ASCII", "Drive (SA 0x09) SOFT Message", " ");
                        paramList.AddScaledParam(BinMsg, ref pos, 64, 1.0, 0, false, Endian.Big, "Workgroup (SA 0xFC) SOFT Message", "ASCII", "Workgroup (SA 0xFC) SOFT Message", " ");


                        paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Devicebatteryvoltage, Endian.Big, "IoT Battery Level", valueundermaprevision); // update 09/26
                        paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "Geozone state before", "", "Geozone state before", valueundermaprevision);
                        paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "Geozone state after", "", "Geozone state after", valueundermaprevision);
                        paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Enginehours, Endian.Big, "Engine Hours", valueundermaprevision);
                        paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Enginespeed, Endian.Big, "Engine Speed(RPM)", valueundermaprevision);
                        paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Fuellevel, Endian.Big, "Fuel Level", valueundermaprevision);
                        paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Deflevel, Endian.Big, "DEF Level", valueundermaprevision);
                        paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Serviceclockhours, Endian.Big, "Service Clock Hours", valueundermaprevision);
                        paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "Attachment ID", "", "Attachment ID", valueundermaprevision);
                        paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "User ID", "", "User ID", valueundermaprevision);
                        paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Cumulativefuelusage, Endian.Big, "Cumulative Fuel Usage", valueundermaprevision);
                        paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Devicebatteryvoltage, Endian.Big, "Internal Battery Level", valueundermaprevision); // update 1/17/19
                        paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Dtccount, Endian.Big, "DTC Count", valueundermaprevision);  // added 8/19/2019
                        paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Bobcatfaultcount, Endian.Big, "Bobcat Fault Count", valueundermaprevision);  // added 8/19/2019
                    }
                    else if (BinMsg[pos] == 3){ 
                        
                            paramList.AddScaledParam(BinMsg, ref pos, 1, 1.0, 0, false, Endian.Big, "Map Revision", "", "", " ");
                            paramList.AddScaledParam(BinMsg, ref pos, 64, 1.0, 0, false, Endian.Big, "ECU (SA 0x00) SOFT Message", "ASCII", "ECU (SA 0x00) SOFT Message", " ");
                            paramList.AddScaledParam(BinMsg, ref pos, 64, 1.0, 0, false, Endian.Big, "DCU Message", "ASCII", "DCU Message", " ");

                            paramList.AddScaledParam(BinMsg, ref pos, 64, 1.0, 0, false, Endian.Big, "Gateway (SA 0xB0) SOFT Message", "ASCII", "Gateway (SA 0xB0) SOFT Message", " ");
                            paramList.AddScaledParam(BinMsg, ref pos, 64, 1.0, 0, false, Endian.Big, "Auxilliary (SA 0xE4) SOFT Message", "ASCII", "Auxilliary (SA 0xE4) SOFT Message", " ");
                            paramList.AddScaledParam(BinMsg, ref pos, 64, 1.0, 0, false, Endian.Big, "Drive (SA 0x09) SOFT Message", "ASCII", "Drive (SA 0x09) SOFT Message", " ");
                            paramList.AddScaledParam(BinMsg, ref pos, 64, 1.0, 0, false, Endian.Big, "Workgroup (SA 0xFC) SOFT Message", "ASCII", "Workgroup (SA 0xFC) SOFT Message", " ");


                            paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Devicebatteryvoltage, Endian.Big, "IoT Battery Level", valueundermaprevision); // update 09/26
                            paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "Geozone state before", "", "Geozone state before", valueundermaprevision);
                            paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "Geozone state after", "", "Geozone state after", valueundermaprevision);
                            paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Enginehours, Endian.Big, "Engine Hours", valueundermaprevision);
                            paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Enginespeed, Endian.Big, "Engine Speed(RPM)", valueundermaprevision);
                            paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Fuellevel, Endian.Big, "Fuel Level", valueundermaprevision);
                            paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Deflevel, Endian.Big, "DEF Level", valueundermaprevision);
                            paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Serviceclockhours, Endian.Big, "Service Clock Hours", valueundermaprevision);
                            paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "Attachment ID", "", "Attachment ID", valueundermaprevision);
                            paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "User ID", "", "User ID", valueundermaprevision);
                            paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Cumulativefuelusage, Endian.Big, "Cumulative Fuel Usage", valueundermaprevision);
                            paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Devicebatteryvoltage, Endian.Big, "Internal Battery Level", valueundermaprevision); // update 1/17/19
                            paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Dtccount, Endian.Big, "DTC Count", valueundermaprevision);  // added 8/19/2019
                            paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Bobcatfaultcount, Endian.Big, "Bobcat Fault Count", valueundermaprevision);  // added 8/19/2019
                        }
                    else if (BinMsg[pos] >= 4)
                    {

                        paramList.AddScaledParam(BinMsg, ref pos, 1, 1.0, 0, false, Endian.Big, "Map Revision", "", "", " ");
                        paramList.AddScaledParam(BinMsg, ref pos, 64, 1.0, 0, false, Endian.Big, "ECU (SA 0x00) SOFT Message", "ASCII", "ECU (SA 0x00) SOFT Message", " ");
                        paramList.AddScaledParam(BinMsg, ref pos, 64, 1.0, 0, false, Endian.Big, "DCU Message", "ASCII", "DCU Message", " ");

                        paramList.AddScaledParam(BinMsg, ref pos, 64, 1.0, 0, false, Endian.Big, "Gateway (SA 0xB0) SOFT Message", "ASCII", "Gateway (SA 0xB0) SOFT Message", " ");
                        paramList.AddScaledParam(BinMsg, ref pos, 64, 1.0, 0, false, Endian.Big, "Auxilliary (SA 0xE4) SOFT Message", "ASCII", "Auxilliary (SA 0xE4) SOFT Message", " ");
                        paramList.AddScaledParam(BinMsg, ref pos, 64, 1.0, 0, false, Endian.Big, "Drive (SA 0x09) SOFT Message", "ASCII", "Drive (SA 0x09) SOFT Message", " ");
                        paramList.AddScaledParam(BinMsg, ref pos, 64, 1.0, 0, false, Endian.Big, "Workgroup (SA 0xFC) SOFT Message", "ASCII", "Workgroup (SA 0xFC) SOFT Message", " ");
                        paramList.AddScaledParam(BinMsg, ref pos, 64, 1.0, 0, false, Endian.Big, "Display Pane SOFT Message", "ASCII", "Display Pane SOFT Message", " ");
                        paramList.AddScaledParam(BinMsg, ref pos, 32, 1.0, 0, false, Endian.Big, "Cab Controller SOFT Message", "ASCII", "Cab Controller SOFT Message", " ");
                        paramList.AddScaledParam(BinMsg, ref pos, 32, 1.0, 0, false, Endian.Big, "Mid Controller SOFT Message", "ASCII", "Mid Controller SOFT Message", " ");
                        paramList.AddScaledParam(BinMsg, ref pos, 32, 1.0, 0, false, Endian.Big, "Rear Aux Controller SOFT Message", "ASCII", "Rear Aux Controller SOFT Message", " ");
                        paramList.AddScaledParam(BinMsg, ref pos, 32, 1.0, 0, false, Endian.Big, "Road Light Controller SOFT Message", "ASCII", "Road Light Controller SOFT Message", " ");
                        paramList.AddScaledParam(BinMsg, ref pos, 32, 1.0, 0, false, Endian.Big, "AFT Controller SOFT Message", "ASCII", "AFT Controller SOFT Message", " ");

                        paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Devicebatteryvoltage, Endian.Big, "IoT Battery Level", valueundermaprevision); // update 09/26
                        paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "Geozone state before", "", "Geozone state before", valueundermaprevision);
                        paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "Geozone state after", "", "Geozone state after", valueundermaprevision);
                        paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Enginehours, Endian.Big, "Engine Hours", valueundermaprevision);
                        paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Enginespeed, Endian.Big, "Engine Speed(RPM)", valueundermaprevision);
                        paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Fuellevel, Endian.Big, "Fuel Level", valueundermaprevision);
                        paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Deflevel, Endian.Big, "DEF Level", valueundermaprevision);
                        paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Serviceclockhours, Endian.Big, "Service Clock Hours", valueundermaprevision);
                        paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "Attachment ID", "", "Attachment ID", valueundermaprevision);
                        paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "User ID", "", "User ID", valueundermaprevision);
                        paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Cumulativefuelusage, Endian.Big, "Cumulative Fuel Usage", valueundermaprevision);
                        paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Devicebatteryvoltage, Endian.Big, "Internal Battery Level", valueundermaprevision); // update 1/17/19
                        paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Dtccount, Endian.Big, "DTC Count", valueundermaprevision);  // added 8/19/2019
                        paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Bobcatfaultcount, Endian.Big, "Bobcat Fault Count", valueundermaprevision);  // added 8/19/2019
                    }
                    break;




                case 176:
                    idString = "JBUS Data: Bobcat HeatMap Report";
                    //paramList.AddScaledParam(BinMsg, ref pos, 1, 1.0, 0, false, Endian.Little, "Map ID", "", "", " ");
                    paramList.AddScaledParam(BinMsg, ref pos, 1, 1.0, 0, false, Endian.Little, "Map Revision", "", "", " ");
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (0 - 1099 RPM) vs Load (< 5%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (0 - 1099 RPM) vs Load (5 - 9%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (0 - 1099 RPM) vs Load (10 - 14%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (0 - 1099 RPM) vs Load (15 - 19%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (0 - 1099 RPM) vs Load (20 - 24%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (0 - 1099 RPM) vs Load (25 - 29%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (0 - 1099 RPM) vs Load (30 - 34%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (0 - 1099 RPM) vs Load (35 - 39%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (0 - 1099 RPM) vs Load (40 - 50%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (0 - 1099 RPM) vs Load (51% and up)", "s", "Bobcat Custom", valueundermaprevision);

                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1100 - 1199 RPM) vs Load (< 5%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1100 - 1199 RPM) vs Load (5 - 9%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1100 - 1199 RPM) vs Load (10 - 14%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1100 - 1199 RPM) vs Load (15 - 19%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1100 - 1199 RPM) vs Load (20 - 24%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1100 - 1199 RPM) vs Load (25 - 29%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1100 - 1199 RPM) vs Load (30 - 34%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1100 - 1199 RPM) vs Load (35 - 39%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1100 - 1199 RPM) vs Load (40 - 50%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1100 - 1199 RPM) vs Load (51% and up)", "s", "Bobcat Custom", valueundermaprevision);


                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1200 - 1299 RPM) vs Load (< 5%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1200 - 1299 RPM) vs Load (5 - 9%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1200 - 1299 RPM) vs Load (10 - 14%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1200 - 1299 RPM) vs Load (15 - 19%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1200 - 1299 RPM) vs Load (20 - 24%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1200 - 1299 RPM) vs Load (25 - 29%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1200 - 1299 RPM) vs Load (30 - 34%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1200 - 1299 RPM) vs Load (35 - 39%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1200 - 1299 RPM) vs Load (40 - 50%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1200 - 1299 RPM) vs Load (51% and up)", "s", "Bobcat Custom", valueundermaprevision);

                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1300 - 1399 RPM) vs Load (< 5%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1300 - 1399 RPM) vs Load (5 - 9%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1300 - 1399 RPM) vs Load (10 - 14%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1300 - 1399 RPM) vs Load (15 - 19%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1300 - 1399 RPM) vs Load (20 - 24%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1300 - 1399 RPM) vs Load (25 - 29%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1300 - 1399 RPM) vs Load (30 - 34%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1300 - 1399 RPM) vs Load (35 - 39%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1300 - 1399 RPM) vs Load (40 - 50%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1300 - 1399 RPM) vs Load (51% and up)", "s", "Bobcat Custom", valueundermaprevision);

                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1400 - 1499 RPM) vs Load (< 5%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1400 - 1499 RPM) vs Load (5 - 9%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1400 - 1499 RPM) vs Load (10 - 14%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1400 - 1499 RPM) vs Load (15 - 19%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1400 - 1499 RPM) vs Load (20 - 24%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1400 - 1499 RPM) vs Load (25 - 29%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1400 - 1499 RPM) vs Load (30 - 34%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1400 - 1499 RPM) vs Load (35 - 39%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1400 - 1499 RPM) vs Load (40 - 50%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1400 - 1499 RPM) vs Load (51% and up)", "s", "Bobcat Custom", valueundermaprevision);

                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1500 - 1599 RPM) vs Load (< 5%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1500 - 1599 RPM) vs Load (5 - 9%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1500 - 1599 RPM) vs Load (10 - 14%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1500 - 1599 RPM) vs Load (15 - 19%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1500 - 1599 RPM) vs Load (20 - 24%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1500 - 1599 RPM) vs Load (25 - 29%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1500 - 1599 RPM) vs Load (30 - 34%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1500 - 1599 RPM) vs Load (35 - 39%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1500 - 1599 RPM) vs Load (40 - 50%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1500 - 1599 RPM) vs Load (51% and up)", "s", "Bobcat Custom", valueundermaprevision);

                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1600 - 1699 RPM) vs Load (< 5%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1600 - 1699 RPM) vs Load (5 - 9%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1600 - 1699 RPM) vs Load (10 - 14%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1600 - 1699 RPM) vs Load (15 - 19%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1600 - 1699 RPM) vs Load (20 - 24%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1600 - 1699 RPM) vs Load (25 - 29%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1600 - 1699 RPM) vs Load (30 - 34%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1600 - 1699 RPM) vs Load (35 - 39%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1600 - 1699 RPM) vs Load (40 - 50%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1600 - 1699 RPM) vs Load (51% and up)", "s", "Bobcat Custom", valueundermaprevision);

                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1700 - 1799 RPM) vs Load (< 5%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1700 - 1799 RPM) vs Load (5 - 9%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1700 - 1799 RPM) vs Load (10 - 14%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1700 - 1799 RPM) vs Load (15 - 19%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1700 - 1799 RPM) vs Load (20 - 24%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1700 - 1799 RPM) vs Load (25 - 29%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1700 - 1799 RPM) vs Load (30 - 34%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1700 - 1799 RPM) vs Load (35 - 39%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1700 - 1799 RPM) vs Load (40 - 50%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1700 - 1799 RPM) vs Load (51% and up)", "s", "Bobcat Custom", valueundermaprevision);

                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1800 - 1899 RPM) vs Load (< 5%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1800 - 1899 RPM) vs Load (5 - 9%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1800 - 1899 RPM) vs Load (10 - 14%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1800 - 1899 RPM) vs Load (15 - 19%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1800 - 1899 RPM) vs Load (20 - 24%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1800 - 1899 RPM) vs Load (25 - 29%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1800 - 1899 RPM) vs Load (30 - 34%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1800 - 1899 RPM) vs Load (35 - 39%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1800 - 1899 RPM) vs Load (40 - 50%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1800 - 1899 RPM) vs Load (51% and up)", "s", "Bobcat Custom", valueundermaprevision);

                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1900 - 1999 RPM) vs Load (< 5%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1900 - 1999 RPM) vs Load (5 - 9%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1900 - 1999 RPM) vs Load (10 - 14%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1900 - 1999 RPM) vs Load (15 - 19%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1900 - 1999 RPM) vs Load (20 - 24%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1900 - 1999 RPM) vs Load (25 - 29%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1900 - 1999 RPM) vs Load (30 - 34%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1900 - 1999 RPM) vs Load (35 - 39%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1900 - 1999 RPM) vs Load (40 - 50%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (1900 - 1999 RPM) vs Load (51% and up)", "s", "Bobcat Custom", valueundermaprevision);


                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2000 - 2099 RPM) vs Load (< 5%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2000 - 2099 RPM) vs Load (5 - 9%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2000 - 2099 RPM) vs Load (10 - 14%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2000 - 2099 RPM) vs Load (15 - 19%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2000 - 2099 RPM) vs Load (20 - 24%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2000 - 2099 RPM) vs Load (25 - 29%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2000 - 2099 RPM) vs Load (30 - 34%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2000 - 2099 RPM) vs Load (35 - 39%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2000 - 2099 RPM) vs Load (40 - 50%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2000 - 2099 RPM) vs Load (51% and up)", "s", "Bobcat Custom", valueundermaprevision);

                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2100 - 2199 RPM) vs Load (< 5%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2100 - 2199 RPM) vs Load (5 - 9%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2100 - 2199 RPM) vs Load (10 - 14%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2100 - 2199 RPM) vs Load (15 - 19%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2100 - 2199 RPM) vs Load (20 - 24%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2100 - 2199 RPM) vs Load (25 - 29%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2100 - 2199 RPM) vs Load (30 - 34%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2100 - 2199 RPM) vs Load (35 - 39%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2100 - 2199 RPM) vs Load (40 - 50%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2100 - 2199 RPM) vs Load (51% and up)", "s", "Bobcat Custom", valueundermaprevision);


                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2200 - 2299 RPM) vs Load (< 5%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2200 - 2299 RPM) vs Load (5 - 9%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2200 - 2299 RPM) vs Load (10 - 14%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2200 - 2299 RPM) vs Load (15 - 19%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2200 - 2299 RPM) vs Load (20 - 24%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2200 - 2299 RPM) vs Load (25 - 29%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2200 - 2299 RPM) vs Load (30 - 34%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2200 - 2299 RPM) vs Load (35 - 39%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2200 - 2299 RPM) vs Load (40 - 50%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2200 - 2299 RPM) vs Load (51% and up)", "s", "Bobcat Custom", valueundermaprevision);


                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2300 - 2399 RPM) vs Load (< 5%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2300 - 2399 RPM) vs Load (5 - 9%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2300 - 2399 RPM) vs Load (10 - 14%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2300 - 2399 RPM) vs Load (15 - 19%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2300 - 2399 RPM) vs Load (20 - 24%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2300 - 2399 RPM) vs Load (25 - 29%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2300 - 2399 RPM) vs Load (30 - 34%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2300 - 2399 RPM) vs Load (35 - 39%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2300 - 2399 RPM) vs Load (40 - 50%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2300 - 2399 RPM) vs Load (51% and up)", "s", "Bobcat Custom", valueundermaprevision);

                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2400 - 2499 RPM) vs Load (< 5%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2400 - 2499 RPM) vs Load (5 - 9%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2400 - 2499 RPM) vs Load (10 - 14%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2400 - 2499 RPM) vs Load (15 - 19%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2400 - 2499 RPM) vs Load (20 - 24%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2400 - 2499 RPM) vs Load (25 - 29%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2400 - 2499 RPM) vs Load (30 - 34%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2400 - 2499 RPM) vs Load (35 - 39%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2400 - 2499 RPM) vs Load (40 - 50%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2400 - 2499 RPM) vs Load (51% and up)", "s", "Bobcat Custom", valueundermaprevision);

                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2500 - 2599 RPM) vs Load (< 5%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2500 - 2599 RPM) vs Load (5 - 9%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2500 - 2599 RPM) vs Load (10 - 14%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2500 - 2599 RPM) vs Load (15 - 19%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2500 - 2599 RPM) vs Load (20 - 24%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2500 - 2599 RPM) vs Load (25 - 29%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2500 - 2599 RPM) vs Load (30 - 34%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2500 - 2599 RPM) vs Load (35 - 39%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2500 - 2599 RPM) vs Load (40 - 50%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2500 - 2599 RPM) vs Load (51% and up)", "s", "Bobcat Custom", valueundermaprevision);

                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2600 - 2699 RPM) vs Load (< 5%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2600 - 2699 RPM) vs Load (5 - 9%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2600 - 2699 RPM) vs Load (10 - 14%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2600 - 2699 RPM) vs Load (15 - 19%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2600 - 2699 RPM) vs Load (20 - 24%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2600 - 2699 RPM) vs Load (25 - 29%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2600 - 2699 RPM) vs Load (30 - 34%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2600 - 2699 RPM) vs Load (35 - 39%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2600 - 2699 RPM) vs Load (40 - 50%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2600 - 2699 RPM) vs Load (51% and up)", "s", "Bobcat Custom", valueundermaprevision);

                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2700 - 2799 RPM) vs Load (< 5%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2700 - 2799 RPM) vs Load (5 - 9%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2700 - 2799 RPM) vs Load (10 - 14%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2700 - 2799 RPM) vs Load (15 - 19%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2700 - 2799 RPM) vs Load (20 - 24%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2700 - 2799 RPM) vs Load (25 - 29%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2700 - 2799 RPM) vs Load (30 - 34%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2700 - 2799 RPM) vs Load (35 - 39%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2700 - 2799 RPM) vs Load (40 - 50%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2700 - 2799 RPM) vs Load (51% and up)", "s", "Bobcat Custom", valueundermaprevision);


                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2800 + RPM) vs Load (< 5%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2800 + RPM) vs Load (5 - 9%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2800 + RPM) vs Load (10 - 14%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2800 + RPM) vs Load (15 - 19%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2800 + RPM) vs Load (20 - 24%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2800 + RPM) vs Load (25 - 29%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2800 + RPM) vs Load (30 - 34%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2800 + RPM) vs Load (35 - 39%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2800 + RPM) vs Load (40 - 50%)", "s", "Bobcat Custom", valueundermaprevision);
                    paramList.AddScaledParam(BinMsg, ref pos, J186lengthbit, 1.0, 0, false, Endian.Little, "Engine Speed (2800 + RPM) vs Load (51% and up)", "s", "Bobcat Custom", valueundermaprevision);


                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Devicebatteryvoltage, Endian.Big, "IoT Battery Level", valueundermaprevision); // update 09/26
                    paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "Geozone state before", "", "Geozone state before", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "Geozone state after", "", "Geozone state after", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Enginehours, Endian.Big, "Engine Hours", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Enginespeed, Endian.Big, "Engine Speed(RPM)", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Fuellevel, Endian.Big, "Fuel Level", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Deflevel, Endian.Big, "DEF Level", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Serviceclockhours, Endian.Big, "Service Clock Hours", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "Attachment ID", "", "Attachment ID", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "User ID", "", "User ID", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Cumulativefuelusage, Endian.Big, "Cumulative Fuel Usage", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Devicebatteryvoltage, Endian.Big, "Internal Battery Level", valueundermaprevision); // update 1/17/19
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Dtccount, Endian.Big, "DTC Count", valueundermaprevision);  // added 8/19/2019
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Bobcatfaultcount, Endian.Big, "Bobcat Fault Count", valueundermaprevision);  // added 8/19/2019
                    break;




                case 177:
                    idString = "JBUS Data: Multiple Bobcat Fault Report";  //message added 8/19/2019
                    paramList.AddScaledParam(BinMsg, ref pos, 1, 1.0, 0, false, Endian.Little, "Map Revision", "", "", " ");
                    faultcount = paramList.AddScaledParam(BinMsg, ref pos, 1, 1.0, 0, false, Endian.Little, "Fault Count", "", "", " ");
                    

                   // NumDTCs = (int)AddDecodedParam(data, ref startbyte, 1, ParamType.Uint, endian, "Number of DTCs", "", "", revision);
                   // retVal = NumDTCs;
                   // AddDecodedParam(data, ref startbyte, 1, ParamType.Uint, endian, "Modified Count", "", "", revision);
                   //
                    while (faultcount-- > 0)
                    {
                        
                        paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Source Address", "", "Bobcat Custom CAN04", valueundermaprevision);
                        paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Device", "", "Bobcat Custom CAN04", valueundermaprevision);
                        paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Function", "", "Bobcat Custom CAN04", valueundermaprevision);
                        paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Failure", "", "Bobcat Custom CAN04", valueundermaprevision);
                        paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Format", "", "Bobcat Custom CAN04", valueundermaprevision);
                    }

                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Devicebatteryvoltage, Endian.Big, "IoT Battery Level", valueundermaprevision); 
                    paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "Geozone state before", "", "Geozone state before", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "Geozone state after", "", "Geozone state after", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Enginehours, Endian.Big, "Engine Hours", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Enginespeed, Endian.Big, "Engine Speed(RPM)", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Fuellevel, Endian.Big, "Fuel Level", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Deflevel, Endian.Big, "DEF Level", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Serviceclockhours, Endian.Big, "Service Clock Hours", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "Attachment ID", "", "Attachment ID", valueundermaprevision);
                    paramList.AddDecodedParam(BinMsg, ref pos, 4, ParamType.Uint, Endian.Big, "User ID", "", "User ID", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Cumulativefuelusage, Endian.Big, "Cumulative Fuel Usage", valueundermaprevision);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Devicebatteryvoltage, Endian.Big, "Internal Battery Level", valueundermaprevision); // update 1/17/19 
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Dtccount, Endian.Big, "DTC Count", valueundermaprevision);  // added 8/19/2019
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_Bobcatfaultcount, Endian.Big, "Bobcat Fault Count", valueundermaprevision);  // added 8/19/2019
                    break;

                default:
                    idString = "VBUS/JBUS Data: Unknown Report Type";
                    break;
            }
        }


        private void DecodeVbusDataReport(byte[] BinMsg, int pos, int msgType, int msgLength)
        {
            int i;
            int NumDTCs;
            ReportMsgType = (int)paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Vbus Data Report Type", "", new Comment(VbusDataRptStr, false), "");
            //            pos += 1;
            //            ReportMsgType = 148;

            switch (ReportMsgType)
            {
                //Gen 1 Messages
                
                case 44: idString = "JBUS Data: Basic Parms Report";
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Pid245, Endian.Little, "J1708 Odometer", "-");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn245, Endian.Little, "J1939 Odometer", "-");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn917, Endian.Little, "J1939 Hi-Rez Odometer", "-");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Pid168, Endian.Little, "J1708 Battery Voltage", "-");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn168, Endian.Little, "J1939 Battery Voltage", "-");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Pid158, Endian.Little, "J1708 Switched Battery Voltage", "-");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn158, Endian.Little, "J1939 Switched Battery Voltage", "-");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Pid190, Endian.Little, "J1708 Engine Speed", "-");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn190, Endian.Little, "J1939 Engine Speed", "-");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Pid84, Endian.Little, "J1708 Road Speed", "-");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn84, Endian.Little, "J1939 Road Speed", "-");
                    break;

                case 45: idString = "JBUS Data: Fuel/Idle Report";
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Pid250, Endian.Little, "J1708 Total Fuel", "-");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn250, Endian.Little, "J1939 Total Fuel", "-");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Pid236, Endian.Little, "J1708 Total Idle Fuel", "-");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn236, Endian.Little, "J1939 Total Idle Fuel", "-");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Pid235, Endian.Little, "J1708 Total Idle Hours", "-");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn235, Endian.Little, "J1939 Total Idle Hours", "-");
                    break;

                case 46: idString = "JBUS Data: Time/Temp Report";
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Pid247, Endian.Little, "J1708 Total Engine Hours", "-");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn247, Endian.Little, "J1939 Total Engine Hours", "-");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Pid110, Endian.Little, "J1708 Engine Coolant Temperature", "-");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn110, Endian.Little, "J1939 Engine Coolant Temperature", "-");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Pid175, Endian.Little, "J1708 Engine Oil Temperature", "-");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn175, Endian.Little, "J1939 Engine Oil Temperature", "-");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn1856, Endian.Little, "SPN 1856 Seatbelt", "-");
                    break;

                case 48: idString = "JBUS Data: Old J1708 VIN Report";
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Pid237, Endian.Little, "Vehicle Identification Number (VIN)", "-");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "VIN Indicator", "", new Comment(JbusVinMsgStr, false), "-");
                    break;

                //VIN Message (Gen 1 style)
                case 49: idString = "JBUS Data: VIN Report";
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Pid237, Endian.Little, "Vehicle Identification Number (VIN)", "-");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "VIN Indicator", "", new Comment(JbusVinMsgStr, false), "-");
                    break;

                //Fleet Mesages
                case 145: idString = "JBUS Data: Fleet Daily Report";
                    DecodeJbusDataHeader(BinMsg, ref pos);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn247, Endian.Little, "Total Engine Hours", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn235, Endian.Little, "Total Idle Hours", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn236, Endian.Little, "Total Idle Fuel", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn98, Endian.Little, "Engine Oil Level", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn111, Endian.Little, "Engine Coolant Level", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn1761, Endian.Little, "DEF Level/NOx Tank Level", "0");
                    break;

                case 146: idString = "JBUS Data: Fleet Hourly Report";
                    DecodeJbusDataHeader(BinMsg, ref pos);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Binary, Endian.Little, "Overflow Flags", "", "", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn110, Endian.Little, "Engine Coolant Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn175, Endian.Little, "Engine Oil Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn100, Endian.Little, "Engine Oil Pressure", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn101, Endian.Little, "Engine Crankcase Pressure", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn109, Endian.Little, "Engine Coolant Pressure", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn168, Endian.Little, "Battery Voltage", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn96, Endian.Little, "Fuel Tank Level 1", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn38, Endian.Little, "Fuel Tank Level 2", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn177, Endian.Little, "Transmission Oil Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn185, Endian.Little, "Avg Fuel Economy", "0");
                    break;

                //Construction Messages
                case 147: idString = "JBUS Data: Construction Daily Report";
                    DecodeJbusDataHeader(BinMsg, ref pos);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn250, Endian.Little, "Engine Total Fuel Used", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn183, Endian.Little, "Avg Engine Fuel Rate", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn513, Endian.Little, "Avg Actual Engine - Percent Torque", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn190, Endian.Little, "Min Engine Speed", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn190, Endian.Little, "Max Engine Speed", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn190, Endian.Little, "Avg Engine Speed", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn3516, Endian.Little, "Min DEF Concentration", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn3516, Endian.Little, "Max DEF Concentration", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn3516, Endian.Little, "Avg DEF Concentration", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn3515, Endian.Little, "Min DEF Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn3515, Endian.Little, "Max DEF Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn3515, Endian.Little, "Avg DEF Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn100, Endian.Little, "Min Oil Pressure", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn100, Endian.Little, "Max Oil Pressure", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn100, Endian.Little, "Avg Oil Pressure", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn175, Endian.Little, "Min Oil Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn175, Endian.Little, "Max Oil Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn175, Endian.Little, "Avg Oil Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn110, Endian.Little, "Min Coolant Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn110, Endian.Little, "Max Coolant Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn110, Endian.Little, "Avg Coolant Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn174, Endian.Little, "Min Fuel Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn174, Endian.Little, "Max Fuel Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn174, Endian.Little, "Avg Fuel Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn171, Endian.Little, "Min Ambient Air Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn171, Endian.Little, "Max Ambient Air Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn171, Endian.Little, "Avg Ambient Air Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn441, Endian.Little, "Min Axiliary Temperature 1", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn441, Endian.Little, "Max Axiliary Temperature 1", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn441, Endian.Little, "Avg Axiliary Temperature 1", "0");
                    break;

                case 148: idString = "JBUS Data: Construction Daily Usage Report";
                    DecodeJbusDataHeader(BinMsg, ref pos);
                    for (i = 0; i < 90; i += 10)
                        paramList.AddScaledParam(BinMsg, ref pos, 2, 0.1, 0, false, Endian.Little, "Engine Torque " + i.ToString() + " to " + (i + 10).ToString() + "% Usage", "minutes", "Time spent in range", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 0.1, 0, false, Endian.Little, "Engine Torque Over 90% Usage", "minutes", "Time spent in range", "0");

                    for (i = 0; i < 90; i += 10)
                        paramList.AddScaledParam(BinMsg, ref pos, 2, 0.1, 0, false, Endian.Little, "Throttle Position " + i.ToString() + " to " + (i + 10).ToString() + "% Usage", "minutes", "Time spent in range", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 0.1, 0, false, Endian.Little, "Throttle Position Over 90% Usage", "minutes", "Time spent in range", "0");
                    break;

                case 149: idString = "Construction Hourly Report";
                    DecodeJbusDataHeader(BinMsg, ref pos);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn247, Endian.Little, "Total Engine Hours", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn1761, Endian.Little, "DEF Level/NOx Tank Level", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn96, Endian.Little, "Fuel Tank Level 1", "0");
                    break;

                //DTC Messages
                case 151: idString = "JBUS Data: J1939 DM1 - ";
                    NumDTCs = (int)paramList.AddJbusParam(BinMsg, ref pos, JbusParam.DtcJ1939, Endian.Little, "J1939 DTCs", "-");
                    idString += NumDTCs.ToString() + " DTC(s)";
                    break;

                case 152: idString = "JBUS Data: J1708 DM1 Codes";
                    NumDTCs = (int)paramList.AddJbusParam(BinMsg, ref pos, JbusParam.DtcJ1708, Endian.Little, "J1708 DTCs", "-");
                    idString += NumDTCs.ToString() + " DTC";
                    break;

                //Bobcat Custom Messages
                case 170: idString = "JBUS Data: Bobcat Hydraulic Report";
                    DecodeJbusDataHeader(BinMsg, ref pos);
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_HydraulicChargePressure, Endian.Little, "Min Hydraulic Charge Pressure", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_HydraulicChargePressure, Endian.Little, "Max Hydraulic Charge Pressure", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_HydraulicChargePressure, Endian.Little, "Avg Hydraulic Charge Pressure", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_HydraulicOilTemperature, Endian.Little, "Min Hydraulic Oil Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_HydraulicOilTemperature, Endian.Little, "Max Hydraulic Oil Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_HydraulicOilTemperature, Endian.Little, "Avg Hydraulic Oil Temperature", "0");
                    break;

                case 171: idString = "JBUS Data: Bobcat Fault Report";
                    DecodeJbusDataHeader(BinMsg, ref pos);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Source Address", "", "Bobcat Custom", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Device", "", "Bobcat Custom", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Function", "", "Bobcat Custom", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Failure", "", "Bobcat Custom", "0");
                    break;

                default: idString = "VBUS/JBUS Data: Unknown Report Type";
                    break;
            }
        }

        private void DecodeType14VbusDataReport(byte[] BinMsg, int pos, int msgType, int msgLength)
        {
            int NumDTCs;
            ReportMsgType = (int)paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Vbus Data Report Type", "", new Comment(VbusDataRptStr, false), "");
            //            pos += 1;
            //            ReportMsgType = 148;

            switch (ReportMsgType)
            {
                //DTC Messages
                case 151:
                    idString = "JBUS Data: J1939 DM1 - ";
                    NumDTCs = (int)paramList.AddJbusParam(BinMsg, ref pos, JbusParam.DtcJ1939, Endian.Little, "J1939 DTCs", "-");
                    idString += NumDTCs.ToString() + " DTC";
                    break;

                case 171:
                    idString = "JBUS Data: Bobcat Fault Report";
                    //DecodeJbusDataHeader(BinMsg, ref pos);
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Source Address", "", "Bobcat Custom CAN04", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Device", "", "Bobcat Custom CAN04", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Function", "", "Bobcat Custom CAN04", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Failure", "", "Bobcat Custom CAN04", "0");
                    break;

                case 172:
                    idString = "JBUS Data: Instant Report";
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Map Revision", "", "", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn91, Endian.Little, "Throttle Position", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn513, Endian.Little, "Load (Actual Engine Percent Torque)", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn3515, Endian.Little, "DEF Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn3516, Endian.Little, "DEF Concentration", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn157, Endian.Little, "Fuel Rail Pressure", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn110, Endian.Little, "Engine Coolant Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn174, Endian.Little, "Engine Fuel Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn175, Endian.Little, "Engine Oil Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn100, Endian.Little, "Engine Oil Pressure", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Brake Switch", "", "SPN 70 Format", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn183, Endian.Little, "Fuel Rate", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Seatbar position", "", "Bobcat Custom BICS", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Door position", "", "Bobcat Custom BICS", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Workgroup State", "", "Bobcat Custom BICS", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Drive State", "", "Bobcat Custom BICS", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Drive Response", "", "Bobcat Custom BICS", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Steering Drift Forward", "", "Bobcat Custom BICS", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Steering Drift Reverse", "", "Bobcat Custom BICS", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Front Base Solenoid", "", "Bobcat Custom CAN02", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Front Rod Solenoid", "", "Bobcat Custom CAN02", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Rear Base Solenoid", "", "Bobcat Custom CAN02", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Rear Rod Solenoid", "", "Bobcat Custom CAN02", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "High Flow Solenoid", "", "Bobcat Custom CAN02", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Engine State (Cranking)", "", "Bobcat Custom CAN03", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Auxiliary Enable", "", "Bobcat Custom CAN20", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "High Flow", "", "Bobcat Custom CAN20", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_HydraulicChargePressure, Endian.Little, "Hydraulic Charge Pressure", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_HydraulicOilTemperature, Endian.Little, "Hydraulic Oil Temperature", "0");
                    break;

                case 173:
                    idString = "JBUS Data: Bobcat Run Report";
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Map Revision", "", "", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn91, Endian.Little, "Min Throttle Position", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn91, Endian.Little, "Max Throttle Position", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn91, Endian.Little, "Avg Throttle Position", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn3516, Endian.Little, "Min DEF Concentration", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn3516, Endian.Little, "Max DEF Concentration", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn3516, Endian.Little, "Avg DEF Concentration", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn1761, Endian.Little, "Min DEF Level", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn1761, Endian.Little, "Max DEF Level", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn157, Endian.Little, "Min Fuel Rail Pressure", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn157, Endian.Little, "Max Fuel Rail Pressure", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn157, Endian.Little, "Avg Fuel Rail Pressure", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn110, Endian.Little, "Min Engine Coolant Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn110, Endian.Little, "Max Engine Coolant Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn110, Endian.Little, "Avg Engine Coolant Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn174, Endian.Little, "Min Engine Fuel Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn174, Endian.Little, "Max Engine Fuel Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn174, Endian.Little, "Avg Engine Fuel Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn175, Endian.Little, "Min Engine Oil Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn175, Endian.Little, "Max Engine Oil Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn175, Endian.Little, "Avg Engine Oil Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn100, Endian.Little, "Min Engine Oil Pressure", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn100, Endian.Little, "Max Engine Oil Pressure", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn100, Endian.Little, "Avg Engine Oil Pressure", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn183, Endian.Little, "Min Fuel Rate", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn183, Endian.Little, "Max Fuel Rate", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn183, Endian.Little, "Avg Fuel Rate", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn172, Endian.Little, "Min Engine Air Intake Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn172, Endian.Little, "Max Engine Air Intake Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Spn172, Endian.Little, "Avg Engine Air Intake Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_HydraulicChargePressure, Endian.Little, "Min Hydraulic Charge Pressure", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_HydraulicChargePressure, Endian.Little, "Max Hydraulic Charge Pressure", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_HydraulicChargePressure, Endian.Little, "Avg Hydraulic Charge Pressure", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_HydraulicOilTemperature, Endian.Little, "Min Hydraulic Oil Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_HydraulicOilTemperature, Endian.Little, "Max Hydraulic Oil Temperature", "0");
                    paramList.AddJbusParam(BinMsg, ref pos, JbusParam.Bobcat_CAN21_HydraulicOilTemperature, Endian.Little, "Avg Hydraulic Oil Temperature", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Gateway (0xB0) Version", "", "Bobcat Custom CAN01 Version", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Gateway (0xB0) Major", "", "Bobcat Custom CAN01 Major", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Gateway (0xB0) Minor", "", "Bobcat Custom CAN01 Minor", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Auxiliary/MX Secondary (0xE4) Version", "", "Bobcat Custom CAN01 Version", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Auxiliary/MX Secondary (0xE4) Major", "", "Bobcat Custom CAN01 Major", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Auxiliary/MX Secondary (0xE4) Minor", "", "Bobcat Custom CAN01 Minor", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Standard Panel (0x17) Version", "", "Bobcat Custom CAN01 Version", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Standard Panel (0x17) Major", "", "Bobcat Custom CAN01 Major", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Standard Panel (0x17) Minor", "", "Bobcat Custom CAN01 Minor", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Drive (0x09 or 0x13) Version", "", "Bobcat Custom CAN01 Version", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Drive (0x09 or 0x13) Major", "", "Bobcat Custom CAN01 Major", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Drive (0x09 or 0x13) Minor", "", "Bobcat Custom CAN01 Minor", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Workgroup/Throttle (0xFC) Version", "", "Bobcat Custom CAN01 Version", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Workgroup/Throttle (0xFC) Major", "", "Bobcat Custom CAN01 Major", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Workgroup/Throttle (0xFC) Minor", "", "Bobcat Custom CAN01 Minor", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Deluxe Panel (0x54) Version", "", "Bobcat Custom CAN01 Version", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Deluxe Panel (0x54) Major", "", "Bobcat Custom CAN01 Major", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Deluxe Panel (0x54) Minor", "", "Bobcat Custom CAN01 Minor", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "ACD (0xB4) Version", "", "Bobcat Custom CAN01 Version", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "ACD (0xB4) Major", "", "Bobcat Custom CAN01 Major", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "ACD (0xB4) Minor", "", "Bobcat Custom CAN01 Minor", "0");
                    break;

                case 174:
                    idString = "JBUS Data: Bobcat Run Usage Report";
                    paramList.AddDecodedParam(BinMsg, ref pos, 1, ParamType.Uint, Endian.Little, "Map Revision", "", "", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 2, ParamType.Uint, Endian.Little, "Seatbar Position (Times Cycled)", "", "Bobcat Custom", "0");
                    paramList.AddDecodedParam(BinMsg, ref pos, 2, ParamType.Uint, Endian.Little, "Door Position (Times Opened)", "", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "WorkGroup State (Time not OFF)", "", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Drive State (Time not OFF)", "", "Bobcat Custom", "0");

                    paramList.AddDecodedParam(BinMsg, ref pos, 2, ParamType.Uint, Endian.Little, "Engine State (Times Cranked)", "", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Engine State (Time During Last Crank)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Engine State (Max Single Cranking Time)", "s", "Bobcat Custom", "0");

                    paramList.AddScaledParam(BinMsg, ref pos, 2, 6.0, 0, false, Endian.Little, "Engine Total On Time", "s", "Bobcat Custom", "0");

                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Throttle Position (< 26 %)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Throttle Position (26 to 50 %)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Throttle Position (51 to 75 %)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Throttle Position (76+ %)", "s", "Bobcat Custom", "0");

                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Speed (RPM) (< 1100 RPM)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Speed (RPM) (1100 to 1199 RPM)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Speed (RPM) (1200 to 1299 RPM)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Speed (RPM) (1300 to 1399 RPM)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Speed (RPM) (1400 to 1499 RPM)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Speed (RPM) (1500 to 1599 RPM)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Speed (RPM) (1600 to 1699 RPM)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Speed (RPM) (1700 to 1799 RPM)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Speed (RPM) (1800 to 1899 RPM)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Speed (RPM) (1900 to 1999 RPM)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Speed (RPM) (2000 to 2099 RPM)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Speed (RPM) (2100 to 2199 RPM)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Speed (RPM) (2200 to 2299 RPM)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Speed (RPM) (2300 to 2399 RPM)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Speed (RPM) (2400 to 2499 RPM)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Speed (RPM) (2500 to 2599 RPM)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Speed (RPM) (2600 to 2699 RPM)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Speed (RPM) (2700 to 2799 RPM)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Speed (RPM) (2800+ RPM)", "s", "Bobcat Custom", "0");

                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Load (Actual Engine - Percent Torque) (< 10 %)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Load (Actual Engine - Percent Torque) (10 to 19 %)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Load (Actual Engine - Percent Torque) (20 to 29 %)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Load (Actual Engine - Percent Torque) (30 to 39 %)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Load (Actual Engine - Percent Torque) (40 to 49 %)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Load (Actual Engine - Percent Torque) (50 to 59 %)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Load (Actual Engine - Percent Torque) (60 to 69 %)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Load (Actual Engine - Percent Torque) (70 to 79 %)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Load (Actual Engine - Percent Torque) (80 to 89 %)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Load (Actual Engine - Percent Torque) (90 to 99 %)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Load (Actual Engine - Percent Torque) (100+ %)", "s", "Bobcat Custom", "0");

                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at DEF Temperature (< -13 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at DEF Temperature (-13 to 15 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at DEF Temperature (16 to 43 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at DEF Temperature (44 to 54 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at DEF Temperature (55 to 65 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at DEF Temperature (66+ °C)", "s", "Bobcat Custom", "0");

                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Coolant Temperature (< 83 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Coolant Temperature (83 to 88 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Coolant Temperature (89 to 93 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Coolant Temperature (94 to 99 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Coolant Temperature (100 to 104 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Coolant Temperature (105 to 110 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Coolant Temperature (111 to 115 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Coolant Temperature (116+ °C)", "s", "Bobcat Custom", "0");

                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Fuel Temperature (< -16 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Fuel Temperature (-16 to 0 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Fuel Temperature (1 to 15 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Fuel Temperature (16 to 32 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Fuel Temperature (33 to 49 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Fuel Temperature (50 to 54 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Fuel Temperature (55 to 60 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Fuel Temperature (61 to 66 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Fuel Temperature (67 to 71 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Fuel Temperature (72 to 77 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Fuel Temperature (78 to 82 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Fuel Temperature (83 to 88 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Fuel Temperature (89+ °C)", "s", "Bobcat Custom", "0");

                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Oil Temperature (< 83 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Oil Temperature (83 to 99 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Oil Temperature (100 to 115 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Oil Temperature (116 to 132 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Engine Oil Temperature (132+ °C)", "s", "Bobcat Custom", "0");

                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Drive Response (State 1 Time)", "", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Drive Response (State 2 Time)", "", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Drive Response (State 3 Time)", "", "Bobcat Custom", "0");

                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Front Base Solenoid (Time ON)", "", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Front Rod Solenoid (Time ON)", "", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Rear Base Solenoid (Time ON)", "", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Rear Rod Solenoid (Time ON)", "", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "High Flow Solenoid (Time ON)", "", "Bobcat Custom", "0");

                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Auxiliary Enable (Time ON)", "", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "High Flow (Time ON)", "", "Bobcat Custom", "0");

                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Hydraulic Oil Temperature (< 38 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Hydraulic Oil Temperature (38 to 49 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Hydraulic Oil Temperature (50 to 60 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Hydraulic Oil Temperature (61 to 66 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Hydraulic Oil Temperature (67 to 71 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Hydraulic Oil Temperature (72 to 77 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Hydraulic Oil Temperature (78 to 82 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Hydraulic Oil Temperature (83 to 88 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Hydraulic Oil Temperature (89 to 93 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Hydraulic Oil Temperature (94 to 99 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Hydraulic Oil Temperature (100 to 104 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Hydraulic Oil Temperature (105 to 110 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Hydraulic Oil Temperature (111 to 115 °C)", "s", "Bobcat Custom", "0");
                    paramList.AddScaledParam(BinMsg, ref pos, 2, 1.0, 0, false, Endian.Little, "Time at Hydraulic Oil Temperature (116+ °C)", "s", "Bobcat Custom", "0");
                    break;

                default:
                    idString = "VBUS/JBUS Data: Unknown Report Type";
                    break;
            }
        }

        public void FillDataGrid(DataGridView dgv)
        {
            paramList.FillDataGrid(dgv);
        }

    }

}
