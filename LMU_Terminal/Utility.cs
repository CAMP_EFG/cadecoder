﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMU_Terminal
{
    public class Utility
    {
        //
        //  Will read an unsigned integer no matter what non-digit character is the delimiter
        //
        public static Double StrToDouble(String str)
        {
            int pos = 0;
            Double value = 0;
            Double place = 1;
            bool neg = false;

            while ((pos < str.Length) && (str[pos] == ' ')) pos++;
            if ((pos < str.Length) && (str[pos] == '-'))
            {
                neg = true;
                pos++;
            }
            //while ((pos < str.Length) && (str[pos] == ' ')) pos++;

            while (pos < str.Length)
            {
                if ((str[pos] >= '0') && (str[pos] <= '9'))
                {
                    value *= 10;
                    value += (UInt32)(str[pos] - '0');
                    pos++;
                }
                else
                {
                    break;
                }
            }

            if ((pos < str.Length) && (str[pos] == '.'))
            {
                pos++;
                while (pos < str.Length)
                {
                    if ((str[pos] >= '0') && (str[pos] <= '9'))
                    {
                        place /= 10;
                        value += place * (UInt32)(str[pos] - '0');
                        pos++;
                    }
                    else
                    {
                        break;
                    }
                }
            }

            if (neg) value *= -1;

            return value;
        }

        //
        //  Will read an unsigned integer no matter what non-digit character is the delimiter
        //
        public static UInt32 StrToUInt32(String str)
        {
            int pos = 0;
            UInt32 value = 0;

            while ((pos < str.Length) && (str[pos] == ' ')) pos++;

            while (pos < str.Length)
            {
                if ((str[pos] >= '0') && (str[pos] <= '9'))
                {
                    value *= 10;
                    value += (UInt32)(str[pos] - '0');
                    pos++;
                }
                else
                {
                    pos = str.Length;
                }
            }

            return value;
        }

        public static String Dec2Bin(byte dec, bool add0b)
        {
            byte i;
            String retVal = "";

            for (i = 0; i < 8; i++)
            {
                if ((dec & 0x80) == 0)
                {
                    retVal += "0";
                }
                else
                {
                    retVal += "1";
                }
                dec <<= 1;
            }

            if (add0b)
            {
                retVal = "0b" + retVal;
            }

            return retVal;
        }

        public static String Dec2Hex(UInt64 dec, UInt16 places, bool add0x)
        {
            //	        UInt16		chars = 0;	
            String conv = "0123456789ABCDEF";
            String retVal = "";

            if (places > 16) places = 16;

            while (places > 0)
            {
                retVal = conv[(int)(dec & 0xF)] + retVal;
                dec >>= 4;
                places--;
            }

            if (add0x)
            {
                retVal = "0x" + retVal;
            }

            return retVal;
        }

        public static UInt64 Hex2Dec(String str)
        {
            int pos = 0;
            UInt64 value = 0;
            int cur;

            while (str[pos] == ' ') pos++;

            while (pos < str.Length)
            {
                if ((str[pos] >= '0') && (str[pos] <= '9'))
                {
                    cur = str[pos] - '0';
                }
                else if ((str[pos] >= 'a') && (str[pos] <= 'f'))
                {
                    cur = 10 + str[pos] - 'a';
                }
                else if ((str[pos] >= 'A') && (str[pos] <= 'F'))
                {
                    cur = 10 + str[pos] - 'A';
                }
                else if (str[pos] == 'x')
                {
                    cur = 0;
                    value = 0;
                }
                else
                {
                    cur = -1;
                }

                if (cur < 0)
                {
                    pos = str.Length;
                }
                else
                {
                    value <<= 4;
                    value += (UInt64)cur;
                    pos++;
                }
            }

            return value;
        }

        public static bool IsHexChar(Char HexVal)
        {
            return (((HexVal >= 'A') && (HexVal <= 'F')) ||
                    ((HexVal >= 'a') && (HexVal <= 'f')) ||
                    ((HexVal >= '0') && (HexVal <= '9')));
        }

        public static bool IsValidHexLine(String text)
        {
            int i;
            int HexCount = 0;
            int SpaceCount = 0;
            bool AnyValidCharFound = false;                             //None so far
            bool FormatValid = ((text != null) && (text.Length > 0));   //Haven't seen anything invalid yet

            for (i = 0; FormatValid && (i < text.Length) ; i++)
            {
                if (IsHexChar(text[i]))
                {
                    AnyValidCharFound = true;
                    SpaceCount = 0;
                    if (++HexCount > 2) FormatValid = false;
                }
                else if (text[i] == ' ')
                {
                    HexCount = 0;
                    if (++SpaceCount > 1) FormatValid = false;                    
                }
                else
                {
                    FormatValid = false;
                }
            }

            return (FormatValid & AnyValidCharFound);
        }

        public static bool IsHexLine(String text)
        {
            int i;
            bool retVal = true;
            bool found = false;

            if ((text != null) && (text.Length > 0))
            {
                if (text.Contains("  ")) retVal = false;    //No double spaces
                if (text.Contains('\t')) retVal = false;    //no tabs

                for (i = 0; (i < text.Length) && retVal; i++)
                {
                    if (((text[i] >= '0') && (text[i] <= '9')) ||
                        ((text[i] >= 'a') && (text[i] <= 'f')) ||
                        ((text[i] >= 'A') && (text[i] <= 'F')) ||
                        ((text[i] == ' ') || (text[i] == '\n') || (text[i] == '\r')))
                    {
                        found = true;
                    }
                    else
                    {
                        retVal = false;
                    }
                }
            }
            else
                retVal = false;

            return (retVal & found);
        }


        //PrevData is a string a previous data to concatenate onto
        public static String AssembleHexBytesToString(String HexStr, String PrevData)
        {
            UInt16 i;
            UInt16 Count = 0;

            HexStr += " ";
            for (i = 0; i < HexStr.Length; i++)
            {
                if (IsHexChar(HexStr[i]))
                {
                    if (++Count >= 2)
                    {
                        PrevData += (Char)Hex2Dec(HexStr.Substring(i - Count + 1, Count));
                        Count = 0;
                    }
                }
                else
                {
                    if (Count > 0)
                    {
                        PrevData += (Char)Hex2Dec(HexStr.Substring(i - Count, Count));
                        Count = 0;
                    }
                }
            }

            return PrevData;
        }

        public static bool IsCharVal(char Val)
        {
            String valid = "ABCDEFGHIJKLMNOPQRSTUVWXYZ 0123456789!.:;/()abcdefghijklmnopqrstuvwxyz";
            bool retVal = false;
            int pos = 0;

            for (pos = 0; pos < valid.Length; pos++)
            {
                if (valid[pos] == Val)
                {
                    retVal = true;
                    break;
                }
            }

            return retVal;
        }

        public static String GetCharFromHexSubstring(String HexStr)
        {
            String retStr = "";
            int i = 0;
            char val;

            for (i = 0; i < HexStr.Length - 1; i++)
            {
                if (HexStr[i] != ' ')
                {
                    val = (char)Hex2Dec(HexStr.Substring(i, 2));
                    if (IsCharVal(val)) retStr += val.ToString();
                    else retStr += "_";
                    i++;
                }
            }

            return retStr;
        }

        public static String GetHexSubstring(String MyStr, UInt16 HexValue)
        {
            String HexStr = Dec2Hex(HexValue, 2, false);
            int pos = 0;

            while ((MyStr[pos] != HexStr[0]) || (MyStr[pos + 1] != HexStr[1])) pos++;

            return MyStr.Substring(pos);
        }

        public static int InStr(String Within, String Search)
        {
            int retVal = -1;
            int i = 0;
            int j;

            if (Search.Length > 0)
            {
                while ((retVal < 0) && (i < Within.Length))
                {
                    j = 0;
                    while ((i < Within.Length) && (j < Search.Length) && (Within[i] == Search[j]))
                    {
                        i++;
                        j++;
                    }

                    if (j == Search.Length)
                        retVal = i - Search.Length;
                    else if (j > 0)
                        i--;

                    i++;
                }
            }
            return retVal;
        }

        public static Double ReadTimeStr(String TimeStr)
        {
            Double retVal = -1;
            Double Cur = 0;
            Double Divisor = 1;
            bool Fractional = false;
            int i;

            if (TimeStr.Length > 0)
            {
                retVal = 0;
                for (i = 0; i < TimeStr.Length; i++)
                {
                    if ((TimeStr[i] >= '0') && (TimeStr[i] <= '9'))
                    {
                        if (Fractional)
                        {
                            Divisor /= 10;
                            retVal += Divisor * (TimeStr[i] - '0');
                        }
                        else
                        {
                            Cur *= 10;
                            Cur += (TimeStr[i] - '0');
                        }
                    }
                    else if (TimeStr[i] == ':')
                    {
                        retVal += Cur;
                        retVal *= 60;
                        Cur = 0;
                    }
                    else if (TimeStr[i] == '.')
                    {
                        Fractional = true;
                        retVal += Cur;
                        Cur = 0;
                    }
                }
            }
            return retVal;
        }
    }
}
