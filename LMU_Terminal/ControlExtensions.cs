﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace LMU_Terminal
{
    class ControlExtensions
    {
        //=================================================================================================
        // TOOL STRIP PROGRESS BAR
        //=================================================================================================


        // Adds a Percentage Display and/or other text to a ToolStripProgressBar
        // It uses the current value, minimum and maximum parameters, and should be called each time the control is updated
        // pb - the control to modify
        // displayPercentage -  if true, the percentage will be calculated and displayed
        //                      if false, the text only will be displayed
        // textToDisplay - text that is displayed when percentage is not displayed
        // Only valid when displayPercentage is true:
        // displayTextWhenMin - if true, text will be displayed instead of percentage when value == minimum
        // displayTextWhenMax - if true, text will be displayed instead of percentage when value == maximum
        //
        // In addition, double buffering should be enabled by adding this line to the Form's constructor:
        //  this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.DoubleBuffer, true);  
        //
        // Example:
        //
        // public FormMyForm()
        // {
        //     //Activate Double Buffering for all kind of drawing within your form
        //     this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.DoubleBuffer, true);            
        //     
        //     InitializeComponent();
        // }

        public static void ProgBarDisplay(ToolStripProgressBar pb, 
                                          Boolean displayPercentage, 
                                          Boolean displayTextWhenMin, 
                                          Boolean displayTextWhenMax, 
                                          String textToDisplay)
        {
            int percent;
            String displayText = "";

            if (displayPercentage)
            {
                percent = (int)(((double)(pb.Value - pb.Minimum) / (double)(pb.Maximum - pb.Minimum)) * 100);
                displayText = percent.ToString() + "%";

                if ((displayTextWhenMin) && (pb.Value == pb.Minimum) && (textToDisplay != null))
                {
                    displayText = textToDisplay;
                }

                if ((displayTextWhenMax) && (pb.Value == pb.Maximum) && (textToDisplay != null))
                {
                    displayText = textToDisplay;
                }
            }
            else
            {
                if (textToDisplay != null)
                {
                    displayText = textToDisplay;
                }
            }

            using (Graphics gr = pb.ProgressBar.CreateGraphics())
            {
                //Switch to Antialiased drawing for better (smoother) graphic results
                gr.SmoothingMode = SmoothingMode.AntiAlias;
                gr.DrawString(displayText,
                    SystemFonts.DefaultFont,
                    Brushes.Black,
                    new PointF(pb.Width / 2 - (gr.MeasureString(displayText,
                        SystemFonts.DefaultFont).Width / 2.0F),
                    pb.Height / 2 - (gr.MeasureString(displayText,
                        SystemFonts.DefaultFont).Height / 2.0F)));
            }
        }

        //=================================================================================================
        // DATA GRID VIEW
        //=================================================================================================

        public static void ChangeColumnHeadersFontStyle(DataGridView dgv, FontStyle fontStyle)
        {
            int i;
            Font font;

            for (i = 0; i < dgv.ColumnCount; i++)
            {
                font = new Font(dgv.DefaultCellStyle.Font, fontStyle);
                dgv.Columns[i].HeaderCell.Style.Font = font;
            }
        }

        //=================================================================================================
        // CHECK BOX / CHECKED LIST BOX
        //=================================================================================================

        public static Boolean CheckStateToBoolean(CheckState checkState)
        {
            return (checkState == CheckState.Checked);
        }

        public static CheckState BooleanToCheckState(Boolean isChecked)
        {
            CheckState checkState;

            if (isChecked)
                checkState = CheckState.Checked;
            else
                checkState = CheckState.Unchecked;

            return checkState;
        }

        //=================================================================================================
        // CHECKED LIST BOX
        //=================================================================================================

        // Sets the CheckState of a CheckedListBox based off of the List text.
        // returns - false if the text was not found
        // clb - the control to modify
        // Text - the List text
        // checkState - the new check state if the text is found

        public static Boolean SetCheckBoxState(CheckedListBox clb, String Text, CheckState checkState)
        {
            int i;
            Boolean Found = false;

            for (i = 0; (i < clb.Items.Count) && !Found; i++)
            {
                if (clb.Items[i].ToString() == Text)
                {
                    Found = true;
                    clb.SetItemCheckState(i, checkState);
                }
            }

            return Found;
        }

        // Sets the CheckState of a CheckedListBox based off of the List text.
        // returns - false if the text was not found
        // clb - the control to modify
        // Text - the List text
        // isChecked - boolean value for whether the control should be checked

        public static Boolean SetCheckBoxState(CheckedListBox clb, String Text, Boolean isChecked)
        {
            return SetCheckBoxState(clb, Text, BooleanToCheckState(isChecked));
        }

        // Gets the CheckState of a CheckedListBox based off of the List text.
        // clb - the control to search
        // Text - the List text

        public static CheckState GetCheckBoxState(CheckedListBox clb, String Text)
        {
            int i;
            Boolean Found = false;
            CheckState RetVal = CheckState.Unchecked;

            for (i = 0; (i < clb.Items.Count) && !Found; i++)
            {
                if (clb.Items[i].ToString() == Text)
                {
                    Found = true;
                    RetVal = clb.GetItemCheckState(i);
                }
            }
            return RetVal;
        }


    }
}
