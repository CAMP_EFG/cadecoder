﻿namespace LMU_Terminal
{
    partial class form_Decode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(form_Decode));
            this.tab_Grid = new System.Windows.Forms.TabControl();
            this.tabUDP = new System.Windows.Forms.TabPage();
            this.dataGridViewUdp = new System.Windows.Forms.DataGridView();
            this.tabVbus = new System.Windows.Forms.TabPage();
            this.dataGridViewVbus = new System.Windows.Forms.DataGridView();
            this.button_Clear = new System.Windows.Forms.Button();
            this.button_Decode = new System.Windows.Forms.Button();
            this.label_HexString = new System.Windows.Forms.Label();
            this.textBox_Hex = new System.Windows.Forms.TextBox();
            this.timerUpdate = new System.Windows.Forms.Timer(this.components);
            this.tab_Grid.SuspendLayout();
            this.tabUDP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUdp)).BeginInit();
            this.tabVbus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewVbus)).BeginInit();
            this.SuspendLayout();
            // 
            // tab_Grid
            // 
            this.tab_Grid.Controls.Add(this.tabUDP);
            this.tab_Grid.Controls.Add(this.tabVbus);
            this.tab_Grid.Location = new System.Drawing.Point(12, 38);
            this.tab_Grid.Name = "tab_Grid";
            this.tab_Grid.SelectedIndex = 0;
            this.tab_Grid.Size = new System.Drawing.Size(1019, 483);
            this.tab_Grid.TabIndex = 6;
            // 
            // tabUDP
            // 
            this.tabUDP.Controls.Add(this.dataGridViewUdp);
            this.tabUDP.Location = new System.Drawing.Point(4, 22);
            this.tabUDP.Name = "tabUDP";
            this.tabUDP.Padding = new System.Windows.Forms.Padding(3);
            this.tabUDP.Size = new System.Drawing.Size(1011, 457);
            this.tabUDP.TabIndex = 0;
            this.tabUDP.Text = "UDP Header";
            this.tabUDP.UseVisualStyleBackColor = true;
            // 
            // dataGridViewUdp
            // 
            this.dataGridViewUdp.AllowUserToAddRows = false;
            this.dataGridViewUdp.AllowUserToDeleteRows = false;
            this.dataGridViewUdp.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewUdp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewUdp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewUdp.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewUdp.Name = "dataGridViewUdp";
            this.dataGridViewUdp.ReadOnly = true;
            this.dataGridViewUdp.Size = new System.Drawing.Size(1005, 451);
            this.dataGridViewUdp.TabIndex = 5;
            // 
            // tabVbus
            // 
            this.tabVbus.Controls.Add(this.dataGridViewVbus);
            this.tabVbus.Location = new System.Drawing.Point(4, 22);
            this.tabVbus.Name = "tabVbus";
            this.tabVbus.Padding = new System.Windows.Forms.Padding(3);
            this.tabVbus.Size = new System.Drawing.Size(1011, 457);
            this.tabVbus.TabIndex = 1;
            this.tabVbus.Text = "Vbus Data";
            this.tabVbus.UseVisualStyleBackColor = true;
            // 
            // dataGridViewVbus
            // 
            this.dataGridViewVbus.AllowUserToAddRows = false;
            this.dataGridViewVbus.AllowUserToDeleteRows = false;
            this.dataGridViewVbus.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewVbus.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewVbus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewVbus.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewVbus.Name = "dataGridViewVbus";
            this.dataGridViewVbus.ReadOnly = true;
            this.dataGridViewVbus.Size = new System.Drawing.Size(1005, 451);
            this.dataGridViewVbus.TabIndex = 6;
            // 
            // button_Clear
            // 
            this.button_Clear.Location = new System.Drawing.Point(680, 12);
            this.button_Clear.Name = "button_Clear";
            this.button_Clear.Size = new System.Drawing.Size(60, 20);
            this.button_Clear.TabIndex = 9;
            this.button_Clear.Text = "Clear";
            this.button_Clear.UseVisualStyleBackColor = true;
            this.button_Clear.Click += new System.EventHandler(this.button_Clear_Click);
            // 
            // button_Decode
            // 
            this.button_Decode.Location = new System.Drawing.Point(614, 12);
            this.button_Decode.Name = "button_Decode";
            this.button_Decode.Size = new System.Drawing.Size(60, 20);
            this.button_Decode.TabIndex = 10;
            this.button_Decode.Text = "Decode";
            this.button_Decode.UseVisualStyleBackColor = true;
            this.button_Decode.Click += new System.EventHandler(this.button_Decode_Click);
            // 
            // label_HexString
            // 
            this.label_HexString.AutoSize = true;
            this.label_HexString.Location = new System.Drawing.Point(12, 15);
            this.label_HexString.Name = "label_HexString";
            this.label_HexString.Size = new System.Drawing.Size(59, 13);
            this.label_HexString.TabIndex = 8;
            this.label_HexString.Text = "Hex String:";
            // 
            // textBox_Hex
            // 
            this.textBox_Hex.Location = new System.Drawing.Point(77, 12);
            this.textBox_Hex.Multiline = true;
            this.textBox_Hex.Name = "textBox_Hex";
            this.textBox_Hex.Size = new System.Drawing.Size(522, 20);
            this.textBox_Hex.TabIndex = 7;
            this.textBox_Hex.Text = resources.GetString("textBox_Hex.Text");
            // 
            // timerUpdate
            // 
            this.timerUpdate.Enabled = true;
            this.timerUpdate.Interval = 250;
            this.timerUpdate.Tick += new System.EventHandler(this.timerUpdate_Tick);
            // 
            // form_Decode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1043, 533);
            this.Controls.Add(this.button_Clear);
            this.Controls.Add(this.button_Decode);
            this.Controls.Add(this.label_HexString);
            this.Controls.Add(this.textBox_Hex);
            this.Controls.Add(this.tab_Grid);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "form_Decode";
            this.Text = "Hex Message Decoder";
            this.Load += new System.EventHandler(this.form_Decode_Load);
            this.Resize += new System.EventHandler(this.form_Decode_Resize);
            this.tab_Grid.ResumeLayout(false);
            this.tabUDP.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUdp)).EndInit();
            this.tabVbus.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewVbus)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tab_Grid;
        private System.Windows.Forms.TabPage tabUDP;
        private System.Windows.Forms.DataGridView dataGridViewUdp;
        private System.Windows.Forms.TabPage tabVbus;
        private System.Windows.Forms.DataGridView dataGridViewVbus;
        private System.Windows.Forms.Button button_Clear;
        private System.Windows.Forms.Button button_Decode;
        private System.Windows.Forms.Label label_HexString;
        private System.Windows.Forms.TextBox textBox_Hex;
        private System.Windows.Forms.Timer timerUpdate;

    }
}