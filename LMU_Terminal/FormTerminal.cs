﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.IO;

namespace LMU_Terminal
{
    public partial class FormTerminal : Form
    {
        //private ComPort comPort;
        private Property property;
        private String HexData;
        private LmuDevice lmuDevice;
        private TextCapture textCapture;

        //Com Port Status
        private Boolean DataSetReady;
        private Boolean ClearToSend;
        private Boolean RingIndicator;
        private Boolean BreakReceived;

        //Live Log
        private int startingLine = 0;

        private List<PictureBox> pictureBoxColor;

        private String lastTimeStamp = "";

        public FormTerminal()
        {
            //Activate Double Buffering for all kind of drawing within your form
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.DoubleBuffer, true);            
            
            InitializeComponent();
        }

        private void FormTerminal_Load(object sender, EventArgs e)
        {
            property = new Property();
            lmuDevice = new LmuDevice();
            textCapture = new TextCapture();
            //comPort = new ComPort();

            //comPort.SetSettings(property.comSettings);
            //comPort.ReceiveTextLines = true;
            //comPort.TextLineReceived += new EventHandler(comPort_TextLineReceived);
            //comPort.Opened += new EventHandler(comPort_Opened);
            //comPort.Closed += new EventHandler(comPort_Closed);
            //comPort.DsrChanged += new EventHandler(comPort_DsrChanged);
            //comPort.CtsChanged += new EventHandler(comPort_CtsChanged);
            //comPort.RingDetected += new EventHandler(comPort_RingDetected);
            //comPort.BreakReceived += new EventHandler(comPort_BreakReceived);

            DisplayCheckColorBoxes();
            checkBoxAutoScroll.CheckState = ControlExtensions.BooleanToCheckState(property.autoScroll);

            DataSetReady = false;
            ClearToSend = false;
            RingIndicator = false;
            BreakReceived = false;

            HexData = "";
            toolStripProgressBarTerminalLength.Maximum = property.rowLimit;

            dgvTerminal.ColumnCount = 2;
            //ControlExtensions.ChangeColumnHeadersFontStyle(dgvTerminal, FontStyle.Bold);
            dgvTerminal.Columns[0].HeaderText = "LMU Serial Data";
            dgvTerminal.Columns[1].HeaderText = "Comment";
            dgvTerminal.Columns[0].Width = 500;
            dgvTerminal.Columns[0].MinimumWidth = 200;
            dgvTerminal.Columns[1].Width = 300;
            dgvTerminal.Columns[1].MinimumWidth = 200;

            if (property.lmuDevicePath != "")
            {
                lmuDevice.LoadLmuDeviceFile(property.lmuDevicePath);
                UpdateTitle();
            }

            //OpenComPort();
        }

        private void FormTerminal_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }

        private void FormTerminal_Resize(object sender, EventArgs e)
        {
            Rectangle displayRecatangle = this.DisplayRectangle;
            int border = 5;

            displayRecatangle.Width -= border;
            displayRecatangle.Height -= statusStrip.Height + border;

            dgvTerminal.Width = displayRecatangle.Width - dgvTerminal.Left;
            dgvTerminal.Height = displayRecatangle.Height - dgvTerminal.Top;
            textBoxTxData.Width = displayRecatangle.Width - textBoxTxData.Left;
        }

        private int GetLimIntVal(int intVal, int minVal, int maxVal, int defVal)
        {
            int retVal = intVal;

            if ((intVal < minVal) || (intVal > maxVal)) retVal = defVal;

            return retVal;
        }

        private void ShowComPortOpenState ()
        {
            
        }

        private void OpenComPort()
        {
            
                
        }

        private void CloseComPort()
        {
           
        }

        private void toolStripButtonOpen_Click(object sender, EventArgs e)
        {
           
        }

        private void toolStripButtonConfigure_Click(object sender, EventArgs e)
        {
            
        }

        private void toolStripButtonClose_Click(object sender, EventArgs e)
        {
           
        }

        private void toolStripLoadCaptureFile_Click(object sender, EventArgs e)
        {
            if (textCapture.OpenCaptureFile(property.lmuDevicePath))
            {
                startingLine = 0;
            }
            
        }

        private void toolStripButtonCopyGrid_Click(object sender, EventArgs e)
        {
            dgvTerminal.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dgvTerminal.SelectAll();
            DataObject dataObject = dgvTerminal.GetClipboardContent();
            dgvTerminal.ClearSelection();
            if (dataObject != null)
                Clipboard.SetDataObject(dataObject);
        }

        private void toolStripButtonClear_Click(object sender, EventArgs e)
        {
            dgvTerminal.RowCount = 0;
        }

        private void UpdateTitle()
        {
            string Title = "LMU Terminal";
            string fileName = lmuDevice.GetFileName();

            if ((fileName == null) || (fileName == ""))
            {
                this.Text = Title;
            }
            else
            {
                this.Text = Title + " - " + fileName;
            }
        }

        private void toolStripLoadLmuDeviceFile_Click(object sender, EventArgs e)
        {
            lmuDevice.OpenLmuDeviceFile(property.lmuDevicePath);
            if (lmuDevice.IsValid())
            {
                UpdateTitle();
                property.lmuDevicePath = lmuDevice.GetFileName();
            }
        }

        private void comPort_TextLineReceived(object sender, EventArgs e)
        {
            /*
            String textLine;

            while (comPort.TextLineCount > 0)
            {
                textLine = comPort.ReadTextLine();
                if ((textLine != null) && (textLine != ""))
                    ParseTextLine(textLine);
            }
            */
        }

        private void comPort_Opened(object sender, EventArgs e)
        {
            
        }

        private void comPort_Closed(object sender, EventArgs e)
        {
            
        }

        private void comPort_DsrChanged(object sender, EventArgs e)
        {
            
        }

        private void comPort_CtsChanged(object sender, EventArgs e)
        {
            
        }

        private void comPort_RingDetected(object sender, EventArgs e)
        {
            
        }

        private void comPort_BreakReceived(object sender, EventArgs e)
        {
            
        }

        private void SendTextData(String Data)
        {
            
        }

        private void buttonSendData_Click(object sender, EventArgs e)
        {
           
        }

        private void textBoxTxData_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                if (textBoxTxData.Text != "")
                    SendTextData(textBoxTxData.Text);
            }
        }

        private void WriteText(String TextLine, String CommentText, Color color, Boolean AutoScroll)
        {
            dgvTerminal.RowCount += 1;
            dgvTerminal.Rows[dgvTerminal.RowCount - 1].DefaultCellStyle.ForeColor = color;
            dgvTerminal[(int)0, (dgvTerminal.RowCount - 1)].Value = TextLine;
            dgvTerminal[(int)1, (dgvTerminal.RowCount - 1)].Value = CommentText;

            if (dgvTerminal.RowCount > property.rowLimit) dgvTerminal.Rows.RemoveAt(0);

            if (AutoScroll)
            {
                dgvTerminal.CurrentCell = dgvTerminal[0, dgvTerminal.RowCount - 1];
                dgvTerminal.CurrentCell = null;
            }
        }

        private void ParseTextLine(String TextLine)
        {
            int i;
            int pos;
            String delimiterList = " ,;:";
            String CommentText = "";
            OtaMessage hexMessage;
            



            //if (TextLine.Contains("64"))
            if (TextLine.Contains("17:56:10"))
              { i = 0;
            }


            if (TextLine != null)
            {
                if (Utility.IsValidHexLine(TextLine))
                //if (Utility.IsHexLine(TextLine))
                {
                    if (property.hexChecked)
                    {
                        if ((HexData != "") && (!HexData.EndsWith(" "))) HexData += " ";
                        HexData += TextLine;
                    }
                }
                else
                {
                    if (HexData != "")
                    {
                        

                        //if (HexData.Contains("45 00") || HexData.Contains("45 58 00") || HexData.Contains("83 05 21") || HexData.Contains("02 01 20"))
                        if (HexData.StartsWith("45 00") || HexData.StartsWith("45 58 00") || HexData.StartsWith("83 05 21") || HexData.StartsWith("02 01 20"))
                        {
                            hexMessage = new OtaMessage(HexData, lmuDevice);
                            //WriteText(lastTimeStamp, CommentText, property.hexColor, property.autoScroll);
                            WriteText(HexData, lastTimeStamp + hexMessage.ToString(), property.hexColor, property.autoScroll);
                     
                        }
                        else if (property.jpodChecked)
                        {
                            hexMessage = new OtaMessage(HexData, lmuDevice);
                            WriteText(HexData, "", property.jpod2Color, property.autoScroll);
                        }
                        HexData = "";
                    }

                    if (TextLine.Length > 12)
                    {
                        lastTimeStamp = "";
                        lastTimeStamp += TextLine[4];
                        lastTimeStamp += TextLine[5];
                        lastTimeStamp += TextLine[6];
                        lastTimeStamp += TextLine[7];
                        lastTimeStamp += TextLine[8];
                        lastTimeStamp += TextLine[9];
                        lastTimeStamp += TextLine[10];
                        lastTimeStamp += TextLine[11];
                        lastTimeStamp += "; ";
                    }
                    

                    if (lmuDevice.IsValid())
                    {
                        for (i = 0; i < lmuDevice.listLmuSearch.Count; i++)
                        {
                            pos = TextLine.IndexOf(lmuDevice.listLmuSearch[i].searchText);

                            if (pos >= 0)
                            {
                                pos += lmuDevice.listLmuSearch[i].searchText.Length;
                                if ((pos == (TextLine.Length - 1)) ||                                       //end if line
                                    ((pos < TextLine.Length) && delimiterList.Contains(TextLine[pos])))     //has a delimiter
                                {
                                    if (CommentText != "")
                                        CommentText += "; ";
                                    CommentText += lmuDevice.listLmuSearch[i].displayText;
                                }
                            }
                        }
                    }

                    if (TextLine.Contains("$GP") || TextLine.Contains("$PUB"))
                    {
                        if (property.nmeaChecked)
                            WriteText(TextLine, CommentText, property.nmeaColor, property.autoScroll);
                    }
                    else if (TextLine.Contains("MDM"))
                    {
                        if (property.cellChecked)
                            WriteText(TextLine, CommentText, property.cellColor, property.autoScroll);
                    }
                    else if (TextLine.Contains("VBUS"))
                    {
                        if (property.vbusChecked)
                            WriteText(TextLine, CommentText, property.vbusColor, property.autoScroll);
                    }
                    else if (TextLine.Contains("JD:") || TextLine.Contains("VBus Data Group") || TextLine.Contains("ms ***"))
                    {
                        if (property.jpodChecked)
                            WriteText(TextLine, CommentText, property.jpod2Color, property.autoScroll);
                    }
                    else if (TextLine.Contains("Trigger"))
                    {
                        if (property.triggerChecked)
                            WriteText(TextLine, CommentText, property.triggerColor, property.autoScroll);
                    }
                    else if (TextLine.Contains("Event"))
                    {
                        if (property.eventsChecked)
                            WriteText(TextLine, CommentText, property.eventsColor, property.autoScroll);
                    }
                    else if (TextLine.Contains("HA: "))
                    {
                        //TPY Testing
                        if (property.haChecked)
                            WriteText(TextLine, CommentText, property.haColor, property.autoScroll);
                    }
                    else if (TextLine != "")
                    {
                        // TPY
                        if (property.everythingElseChecked)
                            WriteText(TextLine, CommentText, property.everythingElseColor, property.autoScroll);
                    }
                } // if (IsHexLine(TextLine))
            } // if (TextLine != null)
        }

        private void timerUpdate_Tick(object sender, EventArgs e)
        {
            String textLine;
            int maxCount = 1000;
            int count = 0;
            

            //Text = comPort.TextLineCount.ToString();
            if (checkReadLiveLog.Checked)
            {
                textCapture.ReOpenCaptureFile();
                if (textCapture.TextLineCount == startingLine)
                    timerUpdate.Interval = 3000;
            }
            else
            {
                //startingLine = 0;
                timerUpdate.Interval = 1;
            }

            if ((!textCapture.FileOpen) && (textCapture.TextLineCount > startingLine))
            {
                //property.autoScroll = false;
                //dgvTerminal.Visible = false;                
                //Load data from the text capture
                //while (textCapture.TextLineCount > 0)
                count = startingLine;
                maxCount = 1000 + startingLine;
                while ((textCapture.TextLineCount > startingLine) && (count < maxCount))
                {

                    count++;
                    textLine = textCapture.ReadTextLine(startingLine);
                    if (textLine != null)
                    {
                         ParseTextLine(textLine);
                    }
                       
                    
                    toolStripStatusLabelLoadCapture.Text = (100 * (textCapture.TextLineMaxCount - textCapture.TextLineCount) / textCapture.TextLineMaxCount).ToString() + "%";
                }
                //property.autoScroll = ControlExtensions.CheckStateToBoolean(((CheckBox)sender).CheckState);
                if (checkReadLiveLog.Checked)
                {
                    startingLine = count;
                }
                    else 
                {
                    startingLine = 0;
                }
            }
            else
            {
//                dgvTerminal.Visible = true;
                toolStripStatusLabelLoadCapture.Text = "";
                
            }

            //if (checkReadLiveLog.Checked == false)
            //{
            //    startingLine = 0;
            //}
            toolStripProgressBarTerminalLength.Value = dgvTerminal.RowCount;
            ControlExtensions.ProgBarDisplay(toolStripProgressBarTerminalLength, true, false, true, "Scroll");
        }

        private void DisplayCheckColorBoxes()
        {
            CheckState checkState;
            String textKey;
            int chkCount = checkedListBoxDisplay.Items.Count;
            int chkSize = 13;
            int chkOffset = 17 - chkSize;
            int i;
            PictureBox pictureBox;
            pictureBoxColor = new List<PictureBox>();

            for (i = 0; i < chkCount; i++)
            {
                textKey = checkedListBoxDisplay.Items[i].ToString();
                pictureBox = new PictureBox();
                pictureBox.Parent = this;
                pictureBox.Location = new Point(checkedListBoxDisplay.Left - chkSize - 2, checkedListBoxDisplay.Top + i * (chkSize + chkOffset) + 1);
                pictureBox.Name = "pictureBoxColor" + i.ToString();
                pictureBox.Size = new Size(chkSize, chkSize);
                pictureBox.Tag = textKey;
                pictureBox.BackColor = property.GetColor((string)pictureBox.Tag);
                pictureBox.BorderStyle = BorderStyle.FixedSingle;
                pictureBox.Visible = true;
                pictureBox.Click += new System.EventHandler(this.pictureBoxColor_Click);

                pictureBoxColor.Add(pictureBox);
               
                checkState = property.GetChecked(textKey);
                ControlExtensions.SetCheckBoxState(checkedListBoxDisplay, textKey, checkState);
            }
        }

        private void pictureBoxColor_Click(object sender, EventArgs e)
        {
            Color color = ((PictureBox)sender).BackColor;
            ColorDialog colorDialog1 = new ColorDialog();

            colorDialog1.Color = color;
            colorDialog1.AllowFullOpen = true;

            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                ((PictureBox)sender).BackColor = colorDialog1.Color;
                property.SetColor((string)((PictureBox)sender).Tag, ((PictureBox)sender).BackColor);
            }
        }

        private void checkedListBoxDisplay_SelectedValueChanged(object sender, EventArgs e)
        {
            String textParam = ((CheckedListBox)sender).SelectedItem.ToString();
            CheckState checkState = ControlExtensions.GetCheckBoxState(checkedListBoxDisplay, textParam);

            property.SetChecked(textParam, checkState);  //Keep "checked" booleans up to date
        }

        private void checkBoxAutoScroll_CheckStateChanged(object sender, EventArgs e)
        {
            property.autoScroll = ControlExtensions.CheckStateToBoolean(((CheckBox)sender).CheckState);
        }

        private void buttonDecode_Click(object sender, EventArgs e)
        {
            form_Decode DecodeForm = new form_Decode("", lmuDevice);
        }

        private void dgvTerminal_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int Col = 0; //e.ColumnIndex;
            int Row = e.RowIndex;
            form_Decode FormA;

            if ((Col < dgvTerminal.ColumnCount) && (Row < dgvTerminal.RowCount))
            {
                if (Utility.IsHexLine(dgvTerminal[Col, Row].Value.ToString()))
                {
                    FormA = new form_Decode(dgvTerminal[Col, Row].Value.ToString(), lmuDevice);
                }
            }
        }

        private void dgvTerminal_MouseClick(object sender, MouseEventArgs e)
        {
            checkBoxAutoScroll.CheckState = CheckState.Unchecked;
        }

        private void checkReadLiveLog_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBoxAutoScroll_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkedListBoxDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
